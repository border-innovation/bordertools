import { Action } from "@ngrx/store";
import { APIModel } from "./models/apis.model";
import { HttpErrorResponse } from "@angular/common/http";


export enum APIsActionTypes {
  GET_ALL = "APIS_GET_ALL",
  GET_ALL_SUCCESS = "APIS_GET_ALL_SUCCESS",
  GET_ALL_FAILED = "APIS_GET_ALL_FAILED",
  ADD_NEW = "APIS_ADD_NEW",
  ADD_NEW_SUCCESS = "APIS_ADD_NEW_SUCCESS",
  ADD_NEW_FAILED = "APIS_ADD_NEW_FAILED",
  UPDATE_ONE = "APIS_UPDATE_ONE",
  UPDATE_ONE_SUCCESS = "APIS_UPDATE_ONE_SUCCESS",
  UPDATE_ONE_FAILED = "APIS_UPDATE_ONE_FAILED",
  REMOVE_ID = "APIS_REMOVE_ID",
  REMOVE_ID_SUCCESS = "APIS_REMOVE_ID_SUCCESS",
  REMOVE_ID_FAILED = "APIS_REMOVE_ID_FAILED",
}

// TODO: When possible, create updated models to APi responses,
// and apply on services and effects
export class ListAPIsRequest implements Action {
  readonly type = APIsActionTypes.GET_ALL;
  constructor() {}
}
export class ListAPIsSuccess implements Action {
  readonly type = APIsActionTypes.GET_ALL_SUCCESS;
  constructor(public apis: APIModel[]) {
    console.log("Get apis success:", apis)
  }
}
export class ListAPIsFailed implements Action {
  readonly type = APIsActionTypes.GET_ALL_FAILED;
  constructor(public error: HttpErrorResponse) {
    console.log("Get apis failed:", error)
  }
}

export class AddAPIRequest implements Action {
  readonly type = APIsActionTypes.ADD_NEW;
  constructor(
    public api: APIModel
  ) {}
}
export class AddAPISuccess implements Action {
  readonly type = APIsActionTypes.ADD_NEW_SUCCESS;
  constructor(public api: APIModel) {}
}
export class AddAPIFailed implements Action {
  readonly type = APIsActionTypes.ADD_NEW_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export class UpdateAPIRequest implements Action {
  readonly type = APIsActionTypes.UPDATE_ONE;
  constructor(
    public api: APIModel
  ) {}
}
export class UpdateAPISuccess implements Action {
  readonly type = APIsActionTypes.UPDATE_ONE_SUCCESS;
  constructor(public api: APIModel) {}
}
export class UpdateAPIFailed implements Action {
  readonly type = APIsActionTypes.UPDATE_ONE_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export class RemoveAPIByIdRequest implements Action {
  readonly type = APIsActionTypes.REMOVE_ID;
  constructor(
    public id: string
  ) {
    console.log("Remove API by ID: ", id)
  }
}
export class RemoveAPIByIdSuccess implements Action {
  readonly type = APIsActionTypes.REMOVE_ID_SUCCESS;
  constructor(public api: APIModel) {}
}
export class RemoveAPIByIdFailed implements Action {
  readonly type = APIsActionTypes.REMOVE_ID_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export type APIActions =
  | ListAPIsRequest
  | ListAPIsSuccess
  | ListAPIsFailed
  | AddAPIRequest
  | AddAPISuccess
  | AddAPIFailed
  | UpdateAPIRequest
  | UpdateAPISuccess
  | UpdateAPIFailed
  | RemoveAPIByIdRequest
  | RemoveAPIByIdSuccess
  | RemoveAPIByIdFailed;
