import { Component, OnDestroy, OnInit } from "@angular/core";
import { AuthService } from "../auth/auth.service";
import { Subscription } from "rxjs";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrl: './header.component.scss'
})
export class HeaderComponent implements OnInit, OnDestroy{
    isAuthenticated = false;
    private userSub: Subscription;

    constructor(
        private authService: AuthService
    ){}

    onLogOut(){
        this.authService.logOut()
    }

    ngOnInit(): void {
        this.userSub = this.authService.user.subscribe(user=>{
            this.isAuthenticated = !!user;
        });
    }
    ngOnDestroy(): void {
        this.userSub.unsubscribe;
    }
}