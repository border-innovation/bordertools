import { createSelector } from "@ngrx/store";
import { GlobalState } from "../../app.store";
import { APIsState } from "./reducer";

export const selectAPIsState = (state: GlobalState) => state.apis;

export const selectAPIs = createSelector(
  selectAPIsState,
  (state: APIsState) => {
    return ({
      apis: state?.apis ?? [], 
      isLoading: state?.isLoading ?? false
    })}
);
