import { QueryRunner } from "typeorm";
import { Procedure } from "../interface/procedure.interface";
import { BadRequestException } from "@nestjs/common";
import { ClientEntity } from "src/client/client.entity";
import { ResultSet } from 'oracledb';

export interface ProcedureParameter {
    dir: number; // BIND_IN or BIND_OUT
    name: string;
    type: number; // NUMBER, STRING, CURSOR
    val?: any;
}

export abstract class ExecuteGetProcedure implements Procedure {
    name: string;
    queryRunner: QueryRunner;
    parameters: ProcedureParameter[] = [];
    callTimeout = 60000;
    batchSize?: number;

    constructor(queryRunner: QueryRunner) {
        if (!queryRunner) {
            throw new BadRequestException(`${this.constructor.name} is missing a valid query runner`);
        }
        this.queryRunner = queryRunner;
    }

    async execute() {
        const strParamVars = this.parameters.map(p => `:${p.name}`).join(',');
        const query = `CALL ${this.name}(${strParamVars})`;
        //console.log(query);
        try {
            const allRows = await this.processQuery(this.queryRunner, query, this.parameters);
            return allRows;
        } catch(e) {
            throw new BadRequestException(`${this.constructor.name} - ${e.message}`);
        }
    }

    // async processQuery(queryRunner: QueryRunner, query: string, parameters: ProcedureParameter[], batchSize?: number): Promise<RawResult[]> {
    async processQuery(queryRunner: QueryRunner, query: string, parameters: ProcedureParameter[]): Promise<ClientEntity[]> {
        let offset = 0;
        let allRows = [];
        let totalRows = 0;
        const maxRows = 20000;
    
        const result: [ResultSet] = await queryRunner.query(query, parameters);
        const [nodeDetailsCursor] = result;

        //console.log('parameters:', parameters);

        while (true) {
            const rows = await nodeDetailsCursor.getRows(this.batchSize);

            totalRows += rows.length;
            allRows = allRows.concat(rows);
    
            // If the number of retrieved rows is less than the batch size, we've reached the end of the cursor
            if (rows.length < this.batchSize || totalRows > maxRows) 
                break;
            
            offset += this.batchSize;
        }
        await nodeDetailsCursor.close();
    
        console.log('Total rows retrieved:', totalRows);
        return allRows;
    }
}