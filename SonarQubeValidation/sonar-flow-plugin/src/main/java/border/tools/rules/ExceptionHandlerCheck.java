package border.tools.rules;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.FlowGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;

@Rule(key = "S00024", name = "Wrappers must invoke exceptionHandler at least once", priority = Priority.MAJOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class ExceptionHandlerCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(WrapperInvokedServicesCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(FlowGrammar.FLOW);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String serviceName = FlowUtils.getQualifiedName(this.getContext().getFile());
		if (serviceName.endsWith("_wrp")) {
			List<AstNode> serviceList = getContent(astNode).getChildren(FlowGrammar.INVOKE);
			for (AstNode invoke : serviceList) {
				AstNode service = invoke.getFirstChild(FlowGrammar.ATTRIBUTES).getFirstChild(FlowAttTypes.SERVICE);
				if (service != null) {
					String name = service.getTokenValue();
					if (name.equalsIgnoreCase("wmextension.exceptionHandling.service:exceptionHandler")) {
						return;
					}
				}
			}
			getContext().createLineViolation(this,
					"Wrappers must invoke wmextension.exceptionHandling.service:exceptionHandler at least once",
					astNode);
		}
	}

	private AstNode getContent(AstNode sequenceNode) {
		if (sequenceNode != null) {
			return sequenceNode.getFirstChild(FlowGrammar.CONTENT);
		}
		return null;
	}

	@Override
	public boolean isFlowCheck() {
		return true;
	}

	@Override
	public boolean isNodeCheck() {
		return false;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}

}
