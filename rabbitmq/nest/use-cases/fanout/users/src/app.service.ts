import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { PostDto } from './post.dto';

@Injectable()
export class AppService {
 
  @RabbitSubscribe({
    queue: `subscribe-queue-${Math.random().toString(36).substring(7)}`,
    exchange: 'notification-exchange',
    routingKey: 'subscribe-route'
  })
  async receiveNotifications(post: PostDto){
    console.log('Notification: ', JSON.stringify(post));
  }
  
  @RabbitSubscribe({
    queue: `subscribe-queue-${Math.random().toString(36).substring(7)}`,
    exchange: 'notification-exchange',
    routingKey: 'subscribe-route'
  })
  async receiveNotification(post: PostDto){
    console.log('Notification: ', JSON.stringify(post));
  }
}

