import { Injectable } from '@nestjs/common';
import { MailDto } from './mail.dto';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class MailService {

    constructor(private readonly amqpConnection: AmqpConnection) { }

    postmail(mail: MailDto) {
        this.amqpConnection.publish<MailDto>('topic_exchange', mail.topic, mail);
    }

}
