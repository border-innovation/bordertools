import { Entity, Column, Unique, PrimaryColumn } from 'typeorm';

@Entity('RESOURCESYSTEM')
@Unique('RESOURCESYSTEM_UK', ['systemName'])
export class ResourceSystemEntity {
  
  @PrimaryColumn({
    name: 'SYSTEMID',
    type: 'number',
  })
  systemId: number;

  @Column({
    name: 'SYSTEMNAME',
    type: 'varchar2',
    length: 100,
    nullable: false,
  })
  systemName: string;
}