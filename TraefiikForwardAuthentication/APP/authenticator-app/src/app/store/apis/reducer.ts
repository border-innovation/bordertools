import { APIModel } from "../apis/models/apis.model";
import { APIActions, APIsActionTypes } from "./actions";

export interface APIsState {
  apis: APIModel[];
  isLoading: boolean;
}

export const initialState: APIsState = {
  apis: [],
  isLoading: false,
};

export function apisReducer(
  state = initialState,
  action: APIActions
): APIsState {
  switch (action.type) {
    case APIsActionTypes.GET_ALL:
      return {
        ...state,
        isLoading: true,
      };
    case APIsActionTypes.GET_ALL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        apis: action.apis,
      };
    case APIsActionTypes.GET_ALL_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case APIsActionTypes.ADD_NEW:
      return {
        ...state,
        isLoading: true,
      };
    case APIsActionTypes.ADD_NEW_SUCCESS:
      return {
        ...state,
        isLoading: false,
        apis: [...state.apis, action.api],
      };
    case APIsActionTypes.ADD_NEW_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case APIsActionTypes.UPDATE_ONE:
      return {
        ...state,
        isLoading: true,
      };
    case APIsActionTypes.UPDATE_ONE_SUCCESS:
      const updatedApisList = state.apis.map((r) => {
        if (r.id !== action.api.id) return r;
        return { ...r, ...action.api };
      });
      return {
        ...state,
        isLoading: false,
        apis: [...updatedApisList],
      };
    case APIsActionTypes.UPDATE_ONE_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case APIsActionTypes.REMOVE_ID:
      return {
        ...state,
        isLoading: true,
      };
    case APIsActionTypes.REMOVE_ID_SUCCESS:
      return {
        ...state,
        isLoading: false,
        apis: state.apis.filter((v) => v.id !== action.api.id),
      };
    case APIsActionTypes.REMOVE_ID_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
