/* Modules */
const express = require('express');
const expressJWT = require('express-jwt');
const router = express.Router();
const fs = require('fs');
const http = require('http');

/* env values */
const config = require('config');
const server = config.get('server');
const api = config.get('api');
const cookieParser = require('cookie-parser');


/* */
const controller = require('@oss-ready/nodejs-baseline').controller;

///* GENERAL PURPOSE functions here *///
router.use(controller.body_parser);
router.use(controller.routes_log);
/* router.use(
	expressJWT({
		secret: new Buffer.from(
			fs.readFileSync(api.token_secret, 'utf8'),
			'base64'
		),
		algorithms: ['HS384'],
		audience: api.token_audience
	})
); */
router.use(cookieParser());
router.use(
  controller.checkTokenMiddleware(api.token_audience, api.token_secret)
);
router.use(controller.error_log);

///* API routes here *///

/**
 * Healthcheck to start the application
 * 		
 */

router.get('/', (req, res) => {
	const options = {
		hostname: 'localhost',
		port: server.port,
		path: '/healthcheck',
		method: 'GET'
	};

	const request = http.request(options, response => {
   
		response.setEncoding('utf8');
		response.on('data', chunk => {
			console.debug(`Server up`);
		});
		response.on('end', () => {
	//   res.sendStatus(response.statusCode);
		res.status(response.statusCode).json('Server Up');
    //   res.send('Server up'); 
		});
	});

	request.on('error', e => {
		console.log(`Server is not running`);
	});

  request.end();
});

module.exports = router;
