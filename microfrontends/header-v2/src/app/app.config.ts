import { ApplicationConfig, NgZone } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { createApplication } from '@angular/platform-browser';
import { createCustomElement } from '@angular/elements';
import { AppComponent } from './app.component';

/* export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes)]
}; */

/*  (async () => {
  const app = await createApplication({
    providers: [ provideRouter(routes)],
  });

  const headerRoot = createCustomElement(AppComponent, {
    injector: app.injector,
  });

  console.log(headerRoot);

  customElements.define('header-root', headerRoot);
})();
 */

