export interface APIsViewModel {
  id?: string;
  endpoint: string;
  role: string;
  verb: string
}
