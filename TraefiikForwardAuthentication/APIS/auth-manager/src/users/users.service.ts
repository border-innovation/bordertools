import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User) private repo: Repository<User>
    ){}

    find(id: number){
        if(!id) throw new BadRequestException('Missing id.');
        return this.repo.findOneBy({id});
    }

    async findAll(){
        const obj = await this.repo.findAndCount()
        return obj[0];
    }

    findBy(email?: string, username?: string){
        if(email) 
            return this.repo.findBy({email});
        if(username) 
            return this.repo.findBy({username});
        throw new NotFoundException('Email and username not provided.');
    }
    
    async remove(id: number){
        if(!id) throw new NotFoundException('Id not provided.');
        const user = await this.find(id);
        if(!user) throw new NotFoundException('User not found.');
        return this.repo.remove(user);
        //return this.repo.delete(id);
    }

    async update(userId: number, attrs: Partial<User>){
        console.log('Updating: ',userId,attrs)
        const user = await this.find(userId);
        if(!user) 
            throw new BadRequestException('User Not found')
        Object.assign(user,attrs);
        return this.repo.save(user)
    }

}
