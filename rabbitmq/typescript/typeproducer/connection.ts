import client, { Connection, Channel, ConsumeMessage } from 'amqplib';

class RabbitMQConnection {

    connection: Connection | undefined;
    channel: Channel | undefined
    private connected: Boolean | undefined


    constructor() { }

    async connect() {
        if (this.connected && this.channel) return;
        else this.connected = true;

        try {
            console.log(`⌛️ Connecting to Rabbit-MQ Server`);

            this.connection = await client.connect(`amqp://username:password@localhost:5672`);

            console.log(`✅ Rabbit MQ Connection is ready`);

            this.channel = await this.connection.createChannel();

            console.log(`🛸 Created RabbitMQ Channel successfully`);

        } catch (error) {
            throw new Error(error + ' Not connected to MQ Server');
        }
    }

    async sendToQueue(queue: string, message: any) {
        try {
            if (!this.channel) await this.connect();
            else{
                this.channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)));
            }
        } catch (error) {
            console.error(error);
        }
    }
}

const mqConnection = new RabbitMQConnection();

export default mqConnection;