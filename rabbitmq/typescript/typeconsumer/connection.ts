import client, { Connection, Channel, ConsumeMessage } from 'amqplib';

type HandlerCB = (msg: string) => any;

class RabbitMQConnection {

    connection: Connection | undefined;
    channel: Channel | undefined
    private connected: Boolean | undefined


    constructor() { }

    async connect() {
        if (this.connected && this.channel) return;
        else this.connected = true;

        try {
            console.log(`⌛️ Connecting to Rabbit-MQ Server`);

            this.connection = await client.connect(`amqp://username:password@localhost:5672`);

            console.log(`✅ Rabbit MQ Connection is ready`);

            this.channel = await this.connection.createChannel();

            console.log(`🛸 Created RabbitMQ Channel successfully`);

        } catch (error) {
            throw new Error(error + ' Not connected to MQ Server');
        }
    }

    async consume(queue: string, handlerIncomingNotification: HandlerCB) {

        if (!this.channel) await this.connect();
        else {
            await this.channel.assertQueue(queue, {
                durable: true
            });

            this.channel.consume(queue, (msg) => {
                if (!msg) throw new Error('Invalid incoming message');
                handlerIncomingNotification(msg?.content?.toString());
                if (!this.channel) return;
                else this.channel.ack(msg);
            }, {
                noAck: false
            });
        }
    }
}

const mqConnection = new RabbitMQConnection();

export default mqConnection;