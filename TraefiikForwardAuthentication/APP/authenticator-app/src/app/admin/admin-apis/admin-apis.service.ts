import { Injectable, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { environment } from "../../../environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { APIModel } from "../../store/apis/models/apis.model";

@Injectable({
    providedIn: 'root'
  })
  
  export class AdminAPIsService {
    constructor(
        private httpClient: HttpClient
    ){

    }

    public getAllApis(){
      let url = `${environment.API_BASE_URL}${environment.APIS_API}`;
      return this.httpClient.get<APIModel[]>(url);
    }

    public addAPI(api: APIModel){
      let url = `${environment.API_BASE_URL}${environment.APIS_API}`;
      console.log('Adding API: ', api, url )
      return this.httpClient.post<APIModel[]>(url,api);
    }

    public updateAPIS(api: any){
      let url = `${environment.API_BASE_URL}${environment.APIS_API}/${api.id}`;
      console.log("Updating api service: ", url, api);
      return this.httpClient.patch<APIModel[]>(url,api);
    }

    public removeAPI(id: string){
      let url = `${environment.API_BASE_URL}${environment.APIS_API}/${id}`;
      console.log("Deleting api service: ", url);
      return this.httpClient.delete<APIModel[]>(url);
    }
   
  }