import { NgModule } from "@angular/core";
import { AdminComponent } from "./admin/admin.component";
import { RouterModule, Routes } from "@angular/router";
import { AuthComponent } from "./auth/auth.component";
import { RouterComponent } from "./router/router.component";
import { AuthGuard } from "./auth/auth.guard";


// const appRoutes: Routes = [
//   { path: '', redirectTo: '/authenticator/login', pathMatch: 'full' },
//   { path: 'authenticator' , redirectTo: '/authenticator/login', pathMatch: 'full' },
//   { path: 'authenticator/login', component: AuthComponent },
//   { path: 'authenticator/router', component: RouterComponent /*, canActivate: [AuthGuard] */ },
//   { path: 'authenticator/admin', component: AdminComponent /*, canActivate: [AuthGuard] */ }
// ];


const appRoutes: Routes = [
    { path: '', redirectTo: '/login',  pathMatch: 'full'},
    { path: 'login', component: AuthComponent},
    { path: 'router', component: RouterComponent, canActivate: [AuthGuard] },
    { path: 'admin', component: AdminComponent, /* canActivate: [AuthGuard] */ },
    { path: '**', redirectTo: ''}
  ]
  
@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}