import { QueryRunner } from "typeorm";
import { BIND_OUT, BIND_IN, STRING, NUMBER, CURSOR } from 'oracledb'
import { ExecuteGetProcedure } from "./executeQuery.procedure";

export class getClientById extends ExecuteGetProcedure {
    name = 'TEST.GETCLIENTBYID';
    queryRunner: QueryRunner;
    callTimeout = 60000;
    batchSize = 10;
    parameters = [
        { dir: BIND_IN, name: 'i_id', type: NUMBER, val: null},
        { dir: BIND_OUT, name: 'o_response', type: CURSOR},
    ];

    constructor(queryRunner: QueryRunner, id: number) {
        super(queryRunner);
        this.queryRunner = queryRunner;
        this.parameters[0].val = Number(id);
    }
}