import { IsString } from "@nestjs/class-validator";

export class ArticleDto {
    @IsString()
    title: string;

    @IsString()
    article: string;

    @IsString()
    category: string;

    @IsString()
    sub_category: string;
}