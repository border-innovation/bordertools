import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { UserDto } from './user/user.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }


}
