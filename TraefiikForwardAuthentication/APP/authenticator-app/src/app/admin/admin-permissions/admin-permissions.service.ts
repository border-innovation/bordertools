import { Injectable, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { environment } from "../../../environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { APIModel } from "../../store/apis/models/apis.model";
import { PermissionModel } from "../../store/permissions/models/permissions.model";

@Injectable({
    providedIn: 'root'
  })
  
  export class AdminPermissionsService {
    constructor(
        private httpClient: HttpClient
    ){

    }

    public getAllPermissions(){
      let url = `${environment.API_BASE_URL}${environment.AUTH_API_PERMISSIONS}`;
      return this.httpClient.get<PermissionModel[]>(url);
    }

    public addPermission(permission: PermissionModel){
      let url = `${environment.API_BASE_URL}${environment.AUTH_API_PERMISSIONS}`;
      console.log('Adding Permission: ', permission, url )
      return this.httpClient.post<PermissionModel[]>(url,permission);
    }

    public updatePermission(permission: any){
      let url = `${environment.API_BASE_URL}${environment.AUTH_API_PERMISSIONS}/${permission.id}`;
      console.log("Updating Permission service: ", url, permission);
      return this.httpClient.patch<PermissionModel[]>(url,permission);
    }

    public removePermission(id: string){
      let url = `${environment.API_BASE_URL}${environment.AUTH_API_PERMISSIONS}/${id}`;
      console.log("Deleting Permission service: ", url);
      return this.httpClient.delete<PermissionModel[]>(url);
    }   
  }