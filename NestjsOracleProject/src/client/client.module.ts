import { Module } from '@nestjs/common';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';
import { ClientEntity } from './client.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StoredProcedureService } from 'src/stored-procedure/stored-procedure.service';

@Module({
  imports: [TypeOrmModule.forFeature([ClientEntity])],
  controllers: [ClientController],
  providers: [ClientService, StoredProcedureService]
})
export class ClientModule {}
