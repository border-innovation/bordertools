import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Entry } from '../shared/entry.model';
import { ReferenceData } from '../shared/reference-data.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiCallService {
  private API_URL = environment.api_url;
  private SYSTEMS_ENDPOINT = 'systems';
  private FIND_ENDPOINT = 'reference_data/find';
  private REFERENCE_DATA_ENDPOINT = 'reference_data';
  private SEARCH_PARAMS = ['system', 'resource', 'value'];
  private FIND_PARAMS = ['system', 'resource'];

  constructor(private http: HttpClient) {}

  getSystems(): Observable<string[]> {
    return this.http
      .get<{ systems: string[] }>(this.API_URL + this.SYSTEMS_ENDPOINT)
      .pipe(
        map(data => data.systems),
        map(data =>
          data.sort((a: string, b: string) => {
            return a.localeCompare(b);
          })
        ),
        catchError(this.handleError<string[]>('getSystems'))
      );
  }

  searchReferenceData(rf: ReferenceData): Observable<ReferenceData[]> {
    if (!rf.system.trim() && !rf.resource.trim() && !rf.value.trim()) {
      return of([]);
    }
    return this.http
      .get<{ results: Entry[] }>(
        this._createURLWithParams(
          this.REFERENCE_DATA_ENDPOINT,
          this.SEARCH_PARAMS,
          rf
        )
      )
      .pipe(
        map(data => data.results),
        map(data =>
          data.map(
            entry =>
              new ReferenceData(
                entry.systemName,
                entry.resourceName,
                entry.resourceValue
              )
          )
        ),
        catchError(this.handleError<ReferenceData[]>(`searchReferenceData`))
      );
  }

  findReferenceData(rf: ReferenceData): Observable<ReferenceData[]> {
    if (!rf.system.trim() && !rf.resource.trim() && !rf.value.trim()) {
      return of([]);
    }
    return this.http
      .get<{ results: ReferenceData[] }>(
        this._createURLWithParams(this.FIND_ENDPOINT, this.FIND_PARAMS, rf)
      )
      .pipe(
        map(data => data.results),
        catchError(this.handleError<ReferenceData[]>(`searchReferenceData`))
      );
  }

  private _createURLWithParams(
    endpoint: string,
    params: string[],
    paramValues: object
  ) {
    let paramURL = '?' + params[0] + '=' + ""/*paramValues[params[0]]*/;
    for (let i = 1; i < params.length; i++) {
      paramURL += '&' + params[i] + '=' + ""/*paramValues[params[i]]*/;
    }
    return this.API_URL + endpoint + paramURL;
  }

  mergeReferenceData(rf: ReferenceData): Observable<any> {
    return this.http
      .put<any>(this.API_URL + this.REFERENCE_DATA_ENDPOINT, {
        systemName: rf.system,
        resourceName: rf.resource,
        resourceValue: rf.value
      })
      .pipe(
        catchError(this.handleError<any>('addReferenceData', { error: true }))
      );
  }

  deleteReferenceData(rfs: ReferenceData[]): Observable<any> {
    return this.http
      .post<any>(this.API_URL + this.REFERENCE_DATA_ENDPOINT, {
        referenceData: rfs
      })
      .pipe(
        catchError(this.handleError<any>('addReferenceData', { error: true }))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
}
