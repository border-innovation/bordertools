var amqp = require('amqplib/callback_api');

//Same as the sender to start the connection and declare the queue to consume

amqp.connect('amqp://localhost', (error0, connection) => {
    if (error0) throw error0;

    connection.createChannel((error1, channel) => {
        if (error1) throw error1;

        var queue = 'hello';

        channel.assertQueue(queue, {
            durable: false
        });

        console.log(`[*] Waiting for messages in ${queue}. To exit press CTRL+C`);

        channel.consume(queue, function (msg) {
            console.log(" [x] Received", msg.content.toString());
        }, {
            noAck: true
        });

        /*       channel.consume(queue, (msg) => {
                  console.log(`[x] Received ${msg.content.toString}`);
              }, {
                  noAck: true
              }); */
    });
});