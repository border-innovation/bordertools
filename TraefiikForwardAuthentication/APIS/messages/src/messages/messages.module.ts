import { Module } from '@nestjs/common';
import { MessagesController } from './messages.controller';
import { MessagesService } from './messages.service';
import { MessageRepository } from './messages.repository';
import { APP_GUARD } from '@nestjs/core';
import { AuthLibModule,JwtAuthGuard } from '@VascoRevesBorder/auth-lib';

@Module({
  imports: [AuthLibModule],
  controllers: [MessagesController],
  providers: [
    MessagesService, 
    MessageRepository,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard, // Set JwtAuthGuard as a global guard
    },
  ]
})
export class MessagesModule {}
