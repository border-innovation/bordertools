import { Body, Controller, Get, Param, Patch, Post, Query, Req, Res,Headers , Session, UseGuards, UseInterceptors, ClassSerializerInterceptor, UnauthorizedException } from '@nestjs/common';
import { Serialize } from '../interceptors/serialize.interceptor';
import { UserDto, UserMinimalDto } from './dtos/user.dto';
import { UsersService } from './users.service';
import { CreateUserDto } from './dtos/create-user.dto';
import { AuthService } from '../auth/auth.service';
import { Response, Request } from 'express';
import { UpdateUserInfoDto } from './dtos/update-user.dto';
import { UserSignInDto } from './dtos/user-signIn.dto';
import { GrpcMethod, RpcException } from '@nestjs/microservices';
import { ValidateTokenDomainDto } from './dtos/validate-token-domain.dto';
import { ValidateTokenDto } from './dtos/validate-token.dto';
import { Metadata, ServerUnaryCall } from '@grpc/grpc-js';
import { ApiExtraModels, ApiOkResponse, ApiResponse } from '@nestjs/swagger';
import { SuccessResponseDto } from './dtos/success.response.dto';
import { status } from '@grpc/grpc-js';

@Controller('auth')
//@Serialize(UserDto)
export class UsersController {

    constructor(
        private authService: AuthService,
    ){}

    
    //TEST GRPC METHOD
    @GrpcMethod('AuthService', 'sayHello')
    sayHello(data: string): { message: string } {
        return { message: 'Hello, World!' };
    }
    
    @ApiOkResponse({
        description: 'Success',
        type: SuccessResponseDto,
    })
    @ApiResponse({
        status: 401,
        description: 'Unauthorized',
        type: [Error]
    })
    @Post('/validate')
    async validate(@Res() response: Response, @Body() body: ValidateTokenDomainDto){
        console.log("================================================================================")
        console.log("AUTH VALIDATING TOKEN\n")
        console.log("BODY: ", body)

        try {
            const result = await this.authService.validate(body.token, body.forwardUri, body.forwardMethod, false);
        
            if (!result) {
                const errorMessage = 'Error Validating Token';
                throw new UnauthorizedException(errorMessage);
            }

            response.setHeader('Content-Type', 'text/plain');
            response.statusCode = 200;
            response.end(result);

            return response;
        } catch(err) {
            console.error("\nERROR VALIDATING TOKEN: ", err.message)
            throw new UnauthorizedException(err.message); 
        }
                  
    }

    @GrpcMethod('AuthService', 'Validate')
    async validateGRPC(@Body() body: ValidateTokenDomainDto){
        console.log("================================================================================")
        console.log("AUTH VALIDATING TOKEN & DOMAIN GRPC\n")
        console.log("BODY GRPC: ", body)

        try{
            const result = await this.authService.validate(body.token,body.forwardUri,body.forwardMethod,false);
            if (!result) {
                const errorMessage = 'Error Validating Token';
                throw new RpcException({
                    code: status.PERMISSION_DENIED, 
                    message: errorMessage
                });
            }
        
            return { message: result };
            
        }catch(err){
            console.error("\nERROR VALIDATING TOKEN: ", err.message)
            throw new RpcException({
                code: status.PERMISSION_DENIED, 
                message: err.message
            });
            
        }

    }

    @Post('/validate/token')
    async validateToken(@Res() response: Response, @Body() body: ValidateTokenDto){
        console.log("================================================================================")
        console.log("AUTH VALIDATING TOKEN\n")
        try {
            const result = await this.authService.validate(body.token, undefined, undefined, true);
        
            if (!result) {
                const errorMessage = 'Error Validating Token';
                throw new UnauthorizedException(errorMessage);
            }

            response.setHeader('Content-Type', 'text/plain');
            response.statusCode = 200;
            response.end(result);

            return response;
        } catch(err) {
            console.error("\nERROR VALIDATING TOKEN: ", err.message)
            throw new UnauthorizedException(err.message); 
        }

    }

    @Post("/signup")
    async signUp(@Body() body: CreateUserDto, @Req() request: Request, @Res() response: Response){
        const user = await this.authService.signUp(body.username, body.email, body.password, body.fullName, request, response);
        return user;
    }
    @Post("/signin")
    async signIn( @Body() body: UserSignInDto,@Req() request: Request, @Res() response: Response){
        const { email, username, password } = body;
        const user = await this.authService.signIn(username, email, password, request, response);
        return user;
    }

   
}

