import { QueryRunner } from "typeorm";
import { BIND_OUT, BIND_IN, STRING, NUMBER, CURSOR } from 'oracledb'
import { ExecuteWriteProcedure } from "../executeQuery.procedure";

export class editClient extends ExecuteWriteProcedure {
    name = 'TEST.EDITCLIENT';
    queryRunner: QueryRunner;
    callTimeout = 60000;
    batchSize = 10;
    parameters = [
        { dir: BIND_IN, name: 'i_id', type: NUMBER, val: null},
        { dir: BIND_IN, name: 'i_first_name', type: STRING, val: null},
        { dir: BIND_IN, name: 'i_second_name', type: STRING, val: null},
        { dir: BIND_OUT, name: 'o_id', type: NUMBER, val: null},
        { dir: BIND_OUT, name: 'o_first_name', type: STRING, val: null},
        { dir: BIND_OUT, name: 'o_second_name', type: STRING, val: null}
    ];

    constructor(queryRunner: QueryRunner, id: number, first_name: string, second_name: string) {
        super(queryRunner);
        this.queryRunner = queryRunner;
        this.parameters[0].val = id;
        this.parameters[1].val = first_name;
        this.parameters[2].val = second_name;
    }
}