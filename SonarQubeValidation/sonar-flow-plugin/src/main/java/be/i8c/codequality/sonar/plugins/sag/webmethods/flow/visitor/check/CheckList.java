/*
 * i8c
 * Copyright (C) 2016 i8c NV
 * mailto:contact AT i8c DOT be
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check;

import java.util.List;

import com.google.common.collect.ImmutableList;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import border.tools.rules.ACLCheck;
import border.tools.rules.CommentsCheck;
import border.tools.rules.ExceptionHandlerCheck;
import border.tools.rules.LastErrorCheck;
import border.tools.rules.PubACLExecuteCheck;
import border.tools.rules.PubWrappersCheck;
import border.tools.rules.RestInputCheck;
import border.tools.rules.RestInvokedServicesCheck;
import border.tools.rules.RestOutputCheck;
import border.tools.rules.RestSoapTrySequenceCheck;
import border.tools.rules.SchedulerCheck;
import border.tools.rules.SoapInvokedServicesCheck;
import border.tools.rules.SoapOutputCheck;
import border.tools.rules.TryCatchCheck;
import border.tools.rules.WebServicesCheck;
import border.tools.rules.WrapperInvokedServicesCheck;
import border.tools.rules.WrappersOutputCheck;

/**
 * Helper class that holds all checks.
 * 
 * @author DEWANST
 *
 */
public class CheckList {

	/**
	 * Returns all checks.
	 * 
	 * @return
	 */
	public static List<Class<? extends FlowCheck>> getChecks() {
		return ImmutableList.<Class<? extends FlowCheck>>builder().addAll(getFlowChecks()).addAll(getTopLevelChecks())
				.addAll(getNodeChecks()).build();
	}

	/**
	 * Returns checks that apply to nodes.
	 * 
	 * @return
	 */
	public static List<Class<? extends FlowCheck>> getNodeChecks() {
		return ImmutableList.<Class<? extends FlowCheck>>builder().add(InterfaceCommentsCheck.class)
				.add(CommentsCheck.class).add(WrappersOutputCheck.class).add(RestOutputCheck.class)
				.add(RestInputCheck.class).add(SoapOutputCheck.class).add(PubACLExecuteCheck.class).build();
	}

	/**
	 * Returns checks that apply to top-level flows.
	 * 
	 * @return
	 */
	public static List<Class<? extends FlowCheck>> getTopLevelChecks() {
		return ImmutableList.<Class<? extends FlowCheck>>builder().add(TryCatchCheck.class).build();
	}

	/**
	 * Returns checks that apply to flows.
	 * 
	 * @return
	 */
	public static List<Class<? extends FlowCheck>> getFlowChecks() {
		return ImmutableList.<Class<? extends FlowCheck>>builder().add(QualifiedNameCheck.class)
				.add(SavePipelineCheck.class).add(DisabledCheck.class).add(ExitCheck.class).add(EmptyMapCheck.class)
				.add(BranchPropertiesCheck.class).add(EmptyFlowCheck.class).add(ACLCheck.class)
				.add(WrapperInvokedServicesCheck.class).add(ExceptionHandlerCheck.class).add(PubWrappersCheck.class)
				.add(RestSoapTrySequenceCheck.class).add(LastErrorCheck.class).add(RestInvokedServicesCheck.class)
				.add(SoapInvokedServicesCheck.class).add(SchedulerCheck.class).add(WebServicesCheck.class).build();
	}
}
