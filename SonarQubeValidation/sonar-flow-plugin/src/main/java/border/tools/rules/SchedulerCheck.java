package border.tools.rules;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;
import com.wm.app.b2b.client.Context;
import com.wm.app.b2b.client.ServiceException;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataUtil;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.FlowGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;

@Rule(key = "S00030", name = "Scheduler Tasks must have descriptions, run by specified users and be associated to Pub Services", priority = Priority.MAJOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class SchedulerCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(SchedulerCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(FlowGrammar.FLOW);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String service = FlowUtils.getQualifiedName(this.getContext().getFile());
		String server = "";
		Context context = new Context();
		try (InputStream input = new FileInputStream("./sonar-project.properties")) {
			Properties prop = new Properties();
			prop.load(input);
			server = prop.getProperty("integrationServer");
			String username = "Administrator";
			String password = "manage";
			context.connect(server, username, password);
		} catch (FileNotFoundException e1) {
			System.out.println("\n\tCannot find the sonar-project.properties \"");
			System.exit(0);
		} catch (IOException e1) {
			System.out.println("\n\tProblem while trying to find server property in sonar-project.properties \"");
			System.exit(0);
		} catch (ServiceException e) {
			System.out.println("\n\tCannot connect to Integration Server \"" + server + "\"");
			System.exit(0);
		}

		try {
			// *** Invoke the Service and Disconnect ***
			IData outputDocument = invoke(context);
			context.disconnect();
			IDataCursor myCursor = outputDocument.getCursor();
			IData[] tasks = IDataUtil.getIDataArray(myCursor, "tasks");
			for (IData task : tasks) {
				IDataCursor taskCursor = task.getCursor();
				String scheduledService = IDataUtil.getString(taskCursor, "service");
				if (service.equals(scheduledService)) {
					String description = IDataUtil.getString(taskCursor, "description");
					String runAsUser = IDataUtil.getString(taskCursor, "runAsUser");
					if (!service.contains("_pub")) {
						getContext().createLineViolation(this, "Scheduled Tasks must call Public Services", astNode);
					}
					if (description.isBlank()) {
						getContext().createLineViolation(this,
								"The Schedule Task invoked by this service must have a description", astNode);
					}
					if (runAsUser.equals("Administrator")) {
						getContext().createLineViolation(this,
								"The Scheduled Task invoked by this service must specify a user that isnt Administrator",
								astNode);
					}
				}
			}
		} catch (IOException e) {
			System.err.println(e);
		} catch (ServiceException e) {
			System.err.println(e);
		}
	}

	public static IData invoke(Context context) throws IOException, ServiceException {
		IData out = context.invoke("pub.scheduler", "getUserTaskList", null);
		IData outputDocument = out;
		return outputDocument;
	}

	@Override
	public boolean isFlowCheck() {
		return true;
	}

	@Override
	public boolean isNodeCheck() {
		return false;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}

}
