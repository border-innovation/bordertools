import { Module } from '@nestjs/common';
import { ApisService } from './apis.service';
import { ApisController } from './apis.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { API } from './apis.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([API])
  ],
  providers: [ApisService],
  controllers: [ApisController],
})
export class ApisModule {}
