import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { Login } from './auth.actions';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {

  saveUserToLocalStorage$ = createEffect(() => this.actions$.pipe(
    ofType(Login),
    tap((action) => {
      const user = action.user;
      localStorage.setItem('user', JSON.stringify(user));
      window.dispatchEvent(new StorageEvent('storage', { key: 'activeUser' }));
      this.router.navigate(['/profile']);
    })
  ), { dispatch: false });

  constructor(private actions$: Actions, private router: Router) { }
}
