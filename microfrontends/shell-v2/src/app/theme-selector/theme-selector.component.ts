import { Component, OnDestroy, OnInit } from '@angular/core';
import { __values } from 'tslib';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { OverlayContainer } from '@angular/cdk/overlay';

interface Theme {
  displayName: string,
  name: string,
  isDark: boolean,
  isDefault: boolean
}

@Component({
  selector: 'app-theme-selector',
  standalone: true,
  imports: [MatFormFieldModule, MatSelectModule, MatInputModule, FormsModule],
  templateUrl: './theme-selector.component.html',
  styleUrl: './theme-selector.component.scss'
})
export class ThemeSelectorComponent implements OnInit {
  currentTheme: Theme | undefined;

  // The below colors need to align with the themes defined in theme-picker.scss
  themes: Theme[] = [
    {
      displayName: 'OSS theme',
      name: 'oss',
      isDark: false,
      isDefault: true
    },
    {
      displayName: 'Custom-light',
      name: 'light',
      isDark: false,
      isDefault: false
    },
    {
      displayName: 'Custom-Dark',
      name: 'dark',
      isDark: true,
      isDefault: false
    },
    {
      displayName: 'Deep Purple & Amber',
      name: 'deeppurple-amber',
      isDark: false,
      isDefault: false
    },
    {
      displayName: 'Indigo & Pink',
      name: 'indigo-pink',
      isDark: false,
      isDefault: false
    },
    {
      displayName: 'Pink & Blue-grey',
      name: 'pink-bluegrey',
      isDark: true,
      isDefault: false
    },
    {
      displayName: 'Purple & Green',
      name: 'purple-green',
      isDark: true,
      isDefault: false
    },
  ];
  onThemeChange(event: MatSelectChange) {
    const selectedTheme = event.value;
    this.applyTheme(selectedTheme);
  }

  ngOnInit(): void {
    this.applyTheme('oss');
  }



  applyTheme(themeName: string) {
    // Remove existing theme classes
    document.body.classList.remove('deeppurple-amber-theme', 'indigo-pink-theme', 'pink-bluegrey-theme', 'purple-green-theme', 'light-theme', 'dark-theme', 'oss-theme');

    // Apply the selected theme class
    document.body.classList.add(themeName + '-theme');

    if (this.currentTheme) {

      this.currentTheme.name = themeName;
    }
  }
}
