import { IsString } from "@nestjs/class-validator";

export class PostDto {
    
    @IsString()
    title: string;

    @IsString()
    date: string;

    @IsString()
    message: string;
}