import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { MailController } from './mail.controller';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';

@Module({
  imports: [RabbitMQModule.forRoot(RabbitMQModule, {
    exchanges: [
      {
        name: 'topic_exchange',
        type: 'topic'
      },
    ],
    uri: 'amqp://admin:admin@localhost:5672',
    connectionInitOptions: { wait: false }
  })],
  providers: [MailService],
  controllers: [MailController]
})
export class MailModule {}
