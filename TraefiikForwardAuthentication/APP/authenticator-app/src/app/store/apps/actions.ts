import { Action } from "@ngrx/store";
import { AppModel } from "./models/apps.model";
import { HttpErrorResponse } from "@angular/common/http";


export enum AppsActionTypes {
  GET = "APPS_GET",
  GET_SUCCESS = "APPS_GET_SUCCESS",
  GET_FAILED = "APPS_GET_FAILED",
  GET_ALL = "APPS_GET_ALL",
  GET_ALL_SUCCESS = "APPS_GET_ALL_SUCCESS",
  GET_ALL_FAILED = "APPS_GET_ALL_FAILED",
  ADD_NEW = "APPS_ADD_NEW",
  ADD_NEW_SUCCESS = "APPS_ADD_NEW_SUCCESS",
  ADD_NEW_FAILED = "APPS_ADD_NEW_FAILED",
  UPDATE_ONE = "APPS_UPDATE_ONE",
  UPDATE_ONE_SUCCESS = "APPS_UPDATE_ONE_SUCCESS",
  UPDATE_ONE_FAILED = "APPS_UPDATE_ONE_FAILED",
  REMOVE_ID = "APPS_REMOVE_ID",
  REMOVE_ID_SUCCESS = "APPS_REMOVE_ID_SUCCESS",
  REMOVE_ID_FAILED = "APPS_REMOVE_ID_FAILED",
}

// TODO: When possible, create updated models to APi responses,
// and apply on services and effects
export class ListAppsRequest implements Action {
  readonly type = AppsActionTypes.GET;
  constructor() {}
}
export class ListAppsSuccess implements Action {
  readonly type = AppsActionTypes.GET_SUCCESS;
  constructor(public apps: AppModel[]) {
    console.log("Get apps success:", apps)
  }
}
export class ListAppsFailed implements Action {
  readonly type = AppsActionTypes.GET_FAILED;
  constructor(public error: HttpErrorResponse) {
    console.log("Get apps failed:", error)
  }
}

export class ListAllAppsRequest implements Action {
  readonly type = AppsActionTypes.GET_ALL;
  constructor() {}
}
export class ListAllAppsSuccess implements Action {
  readonly type = AppsActionTypes.GET_ALL_SUCCESS;
  constructor(public apps: AppModel[]) {
    console.log("Get all apps success:", apps)
  }
}
export class ListAllAppsFailed implements Action {
  readonly type = AppsActionTypes.GET_ALL_FAILED;
  constructor(public error: HttpErrorResponse) {
    console.log("Get all apps failed:", error)
  }
}

export class AddAppRequest implements Action {
  readonly type = AppsActionTypes.ADD_NEW;
  constructor(
    public app: any
  ) {}
}
export class AddAppSuccess implements Action {
  readonly type = AppsActionTypes.ADD_NEW_SUCCESS;
  constructor(public app: AppModel) {
    console.log("Add App Success:", app);
  }
}
export class AddAppFailed implements Action {
  readonly type = AppsActionTypes.ADD_NEW_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export class UpdateAppRequest implements Action {
  readonly type = AppsActionTypes.UPDATE_ONE;
  constructor(
    public app: any
  ) {
    console.log("Update Apps Request: ", app)
  }
}
export class UpdateAppSuccess implements Action {
  readonly type = AppsActionTypes.UPDATE_ONE_SUCCESS;
  constructor(public app: AppModel) {}
}
export class UpdateAppFailed implements Action {
  readonly type = AppsActionTypes.UPDATE_ONE_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export class RemoveAppByIdRequest implements Action {
  readonly type = AppsActionTypes.REMOVE_ID;
  constructor(
    public id: string
  ) {
    console.log("Deleting App Request ID: ", id)
  }
}
export class RemoveAppByIdSuccess implements Action {
  readonly type = AppsActionTypes.REMOVE_ID_SUCCESS;
  constructor(public app: AppModel) {
    console.log("Deleting App Request Success ID: ", app)
  }
}
export class RemoveAppByIdFailed implements Action {
  readonly type = AppsActionTypes.REMOVE_ID_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export type AppActions =
    ListAllAppsRequest
  | ListAllAppsSuccess
  | ListAllAppsFailed
  | ListAppsRequest
  | ListAppsSuccess
  | ListAppsFailed
  | AddAppRequest
  | AddAppSuccess
  | AddAppFailed
  | UpdateAppRequest
  | UpdateAppSuccess
  | UpdateAppFailed
  | RemoveAppByIdRequest
  | RemoveAppByIdSuccess
  | RemoveAppByIdFailed;
