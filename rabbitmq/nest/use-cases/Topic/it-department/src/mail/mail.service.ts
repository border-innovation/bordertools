import { AmqpConnection, RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { MailDto } from './mail.dto';

@Injectable()
export class MailService {
    @RabbitSubscribe({
        exchange: 'topic_exchange',
        routingKey: ['all.#', 'it.#', '#.it.#']
    })
    async getMail(mail: MailDto) {
        console.log('[it] ', mail);
    }
}
