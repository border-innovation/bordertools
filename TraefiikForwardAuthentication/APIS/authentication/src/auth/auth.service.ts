import { BadRequestException, Injectable, NotFoundException, Res, UnauthorizedException } from '@nestjs/common';

import { UsersService } from '../users/users.service';
import { scrypt as _scrypt, randomBytes } from "crypto";
import { JwtService } from '@nestjs/jwt';
import { promisify } from "util";
import { ConfigService } from '@nestjs/config';
import { Request,Response} from 'express';
import { APIsService } from 'src/apis/apis.service';
import { RpcException } from '@nestjs/microservices';
import { status } from '@grpc/grpc-js';

const scrypt = promisify(_scrypt);

@Injectable()
export class AuthService {
    TOKEN_SECRET = this.configService.get<string>('api.secret')
    ENVIRONMENT = this.configService.get<string>('env')
    EXPIRES_IN = this.configService.get<string>('api.expiresIn')
    
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService,
        private configService: ConfigService,
        private apisService: APIsService,
    ){ }

    sayHello(data: string ) {
        const re = { message: `Hello, ${data}!` };
        console.log(re)
        return re;
    }

    async signUp(
        username: string, 
        email: string, 
        password: string, 
        fullName: string, 
        req: Request, 
        res: Response
        ){
        
        //Email exists?
        const users = await this.usersService.findBy(email);
        if(users.length) throw new BadRequestException('Email already in use');
        
        //See if there is the username already
        const user_name = await this.usersService.findBy(undefined,username);
        if(user_name.length) throw new BadRequestException('Username already in use');
        
        //Generate Salt
        const salt = randomBytes(8).toString('hex');
        //Hash the salt and password together
        const hash = (await scrypt(password, salt, 32)) as Buffer;
        //Join the hashed result and salt
        const result = salt + '.' + hash.toString('hex');
        
        //Create New user and save it
        const user = await this.usersService.create(username, email,result, fullName)
        
        const payload : any= {
            username: user.username,
            mail: user.email,
            fullName: user.fullName,
        };

        //Create JWT token
        const token = await this.signToken(
            user.username,
            payload,
            //user.apps,
            [],
            //users.roles,
            [],
            this.TOKEN_SECRET
        )
        //res.setHeader('Set-Cookie', `token=${token}; HttpOnly; Secure; SameSite=Lax`);
    
        payload.apps = [];
        payload.roles = []
        payload.environment = this.ENVIRONMENT;
    
        res.setHeader('Content-Type', 'application/json');
        res.statusCode = 200
        return res.end(JSON.stringify(payload));
    }

    async signIn(
        username: string, 
        email: string, 
        password: string, 
        req: Request, 
        res: Response
    ){
        if (!email && !username) {
            throw new BadRequestException('Either email or username must be provided.');
        }

        let user;
        if(email){
            const [usersByEmail] = await this.usersService.findBy(email,undefined);
            if (!usersByEmail) {
                throw new UnauthorizedException('Email not found.');
            }
            user = usersByEmail;
        } else if(username){
            const [usersByUsername] = await this.usersService.findBy(undefined, username);
            if (!usersByUsername) {
                throw new UnauthorizedException('Username not found.');
            }
            user = usersByUsername;
        } 
       
        const[salt, storedHash] = user.password.split('.');
        const hash = (await scrypt(password, salt, 32)) as Buffer;

        if(storedHash !== hash.toString('hex'))
            throw new UnauthorizedException('Incorrect Password')

        const payload : any= {
            username: user.username,
            mail: user.email,
            fullName: user.fullName
        };

        const roles = await this.usersService.getPermissions(user.username)
        
        //Create JWT token
        const token = await this.signToken(
            user.username,
            payload,
            //users.apps,
            [],
            roles,
            this.TOKEN_SECRET
        )

        //Set token cookie
       // res.setHeader('Set-Cookie', `token=${token}; HttpOnly; Secure; SameSite=Lax`);
        
       res.cookie('token', token, {
        httpOnly: true,
        secure: false, // Set to true if using HTTPS
        sameSite: 'lax', // Allows the cookie to be sent on cross-origin requests
      });

        payload.apps = [];
        payload.roles = roles;
        payload.environment = this.ENVIRONMENT;
        payload.token = token;
    
        res.setHeader('Content-Type', 'application/json');
        res.statusCode = 200
        return res.end(JSON.stringify(payload));        
    } 


    private async signToken(
        username: string,
        responseJson: any, 
        apps: string[], 
        roles: string[], 
        secret: string
        ): Promise<string> {
            const token = await this.jwtService.sign({
                sub: username.toUpperCase(),
                username: responseJson.username,
                fullName: responseJson.fullName,
                mail: responseJson.mail,
                expiresIn: responseJson.expiresIn,
                apps: apps?.sort(),
                environment: this.ENVIRONMENT,
                }, {
                secret: secret ,
                algorithm: 'HS256',
                ...this.getTokenClaims(roles) 
            });

            // Set token in response
            //responseJson.token = token;

            return token;
    }

    private getTokenClaims(roles: string[]): any {
        return {
          algorithm: 'HS384',
          audience: roles,
          expiresIn: this.EXPIRES_IN,
          issuer: 'auth_app',
        };
    }

    async validate(token: string, forwardURI: string, forwardMethod: string, skipDomain?: boolean){
        console.log("Validating Forward URI: ", forwardURI);

        if (!token || !forwardURI || !forwardMethod) {
            throw new UnauthorizedException();
        }
    
            const verify = await this.verifyJwtToken(token,forwardURI, forwardMethod, skipDomain);
            console.log('Verify token: ', verify)
            if(verify){
                let message = "TOKEN OK";
                return message;
            }            
    }

    async verifyJwtToken(token: string, forwardURI?: string, forwardMethod?: string, skipDomain?: boolean) {
      
            const decoded = await this.jwtService.verify(
                token,
                { 
                    secret: this.TOKEN_SECRET
                }
            );
            console.log("\nDECODED JWT TOKEN:", decoded);
            
            // Check if the token is expired
            const expirationTime = new Date(decoded.exp * 1000); // Convert expiration time to milliseconds
            const currentTime = new Date();
            if (expirationTime <= currentTime) {
                console.log("JWT token has expired.");  
                throw new UnauthorizedException();
            }

            if(!skipDomain){
                //Validate Domain
                const domain = this.extractDomain(forwardURI)
                const validate = await this.validateDomain(domain,decoded.aud,decoded.username,forwardMethod);
                if(!validate){
                    throw new UnauthorizedException("Unauthorized domain: " + forwardMethod + domain);
                }
            
                console.log("\nFORWARDING REQUEST OK: ", forwardMethod, forwardURI)
            }
            
            return true;
    }


    extractToken(req: Request): string | undefined {
        const cookieHeader = req.headers.cookie as string;
        const cookies = cookieHeader ? cookieHeader.split('; ') : [];
        for (const cookie of cookies) {
            const [name, value] = cookie.split('=');
            if (name.trim() === 'token') {
                console.log('Extracted Token value: ', value)
                return value;
            }
        }
        const authorizationHeader = req.headers.authorization;

        if (authorizationHeader && authorizationHeader.startsWith('Bearer ')) {
            console.log('authorizationHeader: ', authorizationHeader)
            const token = authorizationHeader.split(' ')[1];
            console.log('Extracted Token: ', token)
            return token;
        }
    
        console.log("No token found in cookies or authorization header.");
        return undefined;
    }

    async validateDomain(domain: string, roles: string[], username: string, verb: string): Promise<boolean> {
        if(!roles) {
            console.log("USER AS NO ROLES!");
            return false;
        }
        console.log('\n====== VALIDATING DOMAIN ======');
        console.log("Endpoint: ", domain)
        console.log("\HTTP VERB: ", verb)

        // Step 1: Retrieve API roles based on the domain and verb
        const apis = await this.apisService.findByPair(domain,verb)
        
        const endpointRoles = apis.map(api => api.role); // Extract roles from the APIs

        if (!endpointRoles || endpointRoles.length === 0) {
            console.error(`No roles configured for the endpoint: ${domain} with verb: ${verb}`);
            return false;
        }

        console.log("Endpoint Roles: ", endpointRoles);

        // Step 2: Compare roles to see if there's an intersection
        const hasAccess = endpointRoles.some((endpointRole) => roles.includes(endpointRole));

        if (hasAccess) {
            console.log(`User ${username} has access to ${domain} with verb ${verb}`);
            return true;
        } else {
            console.warn(`User ${username} does NOT have access to ${domain} with verb ${verb}`);
            return false;
        }
    }
    

    extractDomain(url: string): string {
        
        const trimmedUrl = url.replace(/^\/+|\/+$/g, '');    
        const segments = trimmedUrl.split('/');
        const domain = segments[0] ? '/' + segments[0] : '/';
        
        return domain;
    }

}