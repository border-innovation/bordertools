export class ReferenceData {
  constructor(
    public system: string,
    public resource: string,
    public value: string
  ) {}
}
