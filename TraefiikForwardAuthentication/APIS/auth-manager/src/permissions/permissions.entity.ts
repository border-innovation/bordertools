import { Entity, PrimaryGeneratedColumn, Column, AfterInsert, AfterUpdate, AfterRemove, OneToMany, ManyToOne } from 'typeorm';

@Entity({name: 'USER_ROLE'})
export class Permission {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    role: string;

    @Column()
    username: string;

    @AfterInsert()
    logInsert() {
        console.log('Inserted permission: ', this);
    }

    @AfterUpdate()
    logUpdate() {
        console.log('Updated permission: ', this);
    }

    @AfterRemove()
    logRemove() {
        console.log('Removed permission: ', this);
    }
}