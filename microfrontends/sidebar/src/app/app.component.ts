import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { RouterModule } from '@angular/router';
import { User } from './user.model';
import { ThemeSelectorComponent } from "./theme-selector/theme-selector.component";
import { MatButtonModule } from '@angular/material/button';
import { MatList, MatListItem, MatNavList } from '@angular/material/list';
import { MatDivider } from '@angular/material/divider';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDrawer, MatSidenav, MatSidenavModule } from '@angular/material/sidenav';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  imports: [RouterModule, 
    ThemeSelectorComponent, MatNavList, MatButtonToggleModule, MatButtonModule, MatListItem, MatList, MatDivider, MatSidenavModule]
})
export class AppComponent {
  activeUser: User | undefined;

  private storageEventListener: (event: StorageEvent) => void;

  constructor() {
    this.storageEventListener = this.handleStorageEvent.bind(this);
  }

  @ViewChild('drawer') drawer!: MatDrawer;

  toggleSidebar() {
    this.drawer.toggle();
  }

  ngOnInit(): void {
    this.activeUser = this.getUserFromLocalStorage();
    window.addEventListener('storage', this.storageEventListener);
  }

  ngOnDestroy(): void {
    window.removeEventListener('storage', this.storageEventListener);
  }

  private handleStorageEvent(event: StorageEvent): void {
    if (event.key === 'activeUser') {
      this.activeUser = this.getUserFromLocalStorage();
    }
  }

  private getUserFromLocalStorage(): any {
    const userData = localStorage.getItem('activeUser');
    console.log("header:", userData);
    return userData ? JSON.parse(userData) : null;
  }

  @Output() navigate = new EventEmitter<string>();

  navigateToPage(page: string) {
    this.navigate.emit(page);
  }


}
