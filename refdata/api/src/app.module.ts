import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { TerminusModule } from '@nestjs/terminus';
import { ResourceSystemEntity } from './resources-properties/resource-system.entity';
import { ResourcesPropertiesModule } from './resources-properties/resources-properties.module';
import { ResourcePropertiesEntity } from './resources-properties/resource-properties.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "oracle",
      host: "localhost",
      port: 1521,
      username: "system",
      password: "password",
      serviceName: "FREEPDB1",
      entities: [
        ResourcePropertiesEntity,
        ResourceSystemEntity
      ],
      synchronize: true,
      keepConnectionAlive: true,
      poolSize: 5, 
      extra: {
        poolIncrement: 1,
        poolMin: 5
      }
    }),
    ResourcesPropertiesModule,
    TerminusModule,
    ResourcesPropertiesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor (private dataSource: DataSource) {}
}
