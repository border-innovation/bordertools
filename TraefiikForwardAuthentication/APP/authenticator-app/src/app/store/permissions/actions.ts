import { Action } from "@ngrx/store";
import { PermissionModel } from "./models/permissions.model";
import { HttpErrorResponse } from "@angular/common/http";


export enum PermissionsActionTypes {
  GET_ALL = "PERMISSIONS_GET_ALL",
  GET_ALL_SUCCESS = "PERMISSIONS_GET_ALL_SUCCESS",
  GET_ALL_FAILED = "PERMISSIONS_GET_ALL_FAILED",
  ADD_NEW = "PERMISSIONS_ADD_NEW",
  ADD_NEW_SUCCESS = "PERMISSIONS_ADD_NEW_SUCCESS",
  ADD_NEW_FAILED = "PERMISSIONS_ADD_NEW_FAILED",
  UPDATE_ONE = "PERMISSIONS_UPDATE_ONE",
  UPDATE_ONE_SUCCESS = "PERMISSIONS_UPDATE_ONE_SUCCESS",
  UPDATE_ONE_FAILED = "PERMISSIONS_UPDATE_ONE_FAILED",
  REMOVE_ID = "PERMISSIONS_REMOVE_ID",
  REMOVE_ID_SUCCESS = "PERMISSIONS_REMOVE_ID_SUCCESS",
  REMOVE_ID_FAILED = "PERMISSIONS_REMOVE_ID_FAILED",
}

export class ListPermissionsRequest implements Action {
  readonly type = PermissionsActionTypes.GET_ALL;
  constructor() {}
}
export class ListPermissionsSuccess implements Action {
  readonly type = PermissionsActionTypes.GET_ALL_SUCCESS;
  constructor(public permissions: PermissionModel[]) {
    console.log("Get permissions success:", permissions)
  }
}
export class ListPermissionsFailed implements Action {
  readonly type = PermissionsActionTypes.GET_ALL_FAILED;
  constructor(public error: HttpErrorResponse) {
    console.log("Get permissions failed:", error)
  }
}

export class AddPermissionRequest implements Action {
  readonly type = PermissionsActionTypes.ADD_NEW;
  constructor(
    public permission: PermissionModel
  ) {
    console.log("Adding permission action:", permission);
  }
}
export class AddPermissionSuccess implements Action {
  readonly type = PermissionsActionTypes.ADD_NEW_SUCCESS;
  constructor(public permission: PermissionModel) {}
}
export class AddPermissionFailed implements Action {
  readonly type = PermissionsActionTypes.ADD_NEW_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export class UpdatePermissionRequest implements Action {
  readonly type = PermissionsActionTypes.UPDATE_ONE;
  constructor(
    public permission: PermissionModel
  ) {
    console.log("Update Permissions Request: ", permission)

  }
}
export class UpdatePermissionSuccess implements Action {
  readonly type = PermissionsActionTypes.UPDATE_ONE_SUCCESS;
  constructor(public permission: PermissionModel) {}
}
export class UpdatePermissionFailed implements Action {
  readonly type = PermissionsActionTypes.UPDATE_ONE_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export class RemovePermissionByIdRequest implements Action {
  readonly type = PermissionsActionTypes.REMOVE_ID;
  constructor(
    public id: string
  ) {
    console.log("Delete Permissions Request id: ", id)
  }
}
export class RemovePermissionByIdSuccess implements Action {
  readonly type = PermissionsActionTypes.REMOVE_ID_SUCCESS;
  constructor(public permission: PermissionModel) {}
}
export class RemovePermissionByIdFailed implements Action {
  readonly type = PermissionsActionTypes.REMOVE_ID_FAILED;
  constructor(public error: HttpErrorResponse) {}
}

export type PermissionActions =
  | ListPermissionsRequest
  | ListPermissionsSuccess
  | ListPermissionsFailed
  | AddPermissionRequest
  | AddPermissionSuccess
  | AddPermissionFailed
  | UpdatePermissionRequest
  | UpdatePermissionSuccess
  | UpdatePermissionFailed
  | RemovePermissionByIdRequest
  | RemovePermissionByIdSuccess
  | RemovePermissionByIdFailed;
