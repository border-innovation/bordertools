import { Component } from '@angular/core';
import { ReviewItemComponent } from "./review-item/review-item.component";
import { Router } from '@angular/router';
interface album {
  name: string;
  path: string;
}
@Component({
  selector: 'app-reviews-home',
  standalone: true,
  templateUrl: './reviews-home.component.html',
  styleUrl: './reviews-home.component.scss',
  imports: [ReviewItemComponent]
})
export class ReviewsHomeComponent {

  album1: album = {name: "Lotus", path: "album1"}
  album2: album = {name: "Imperial", path: "album2"}

  constructor(private router: Router) { }

  navigateToReview() {
    this.router.navigate(['reviews/review'])
  }

}
