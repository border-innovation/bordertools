import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { SidebarComponent } from "./sidebar/sidebar.component";

import { UserCommunicationService } from '@pedro_azevedo_border/communication-lib';
import { User } from './user.model';
import { WrapperHeaderComponent } from "./wrapper-header/wrapper-header.component";
import { MatButtonModule } from '@angular/material/button';
import { ThemeSelectorComponent } from './theme-selector/theme-selector.component';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  imports: [RouterOutlet, SidebarComponent, WrapperHeaderComponent, MatButtonModule, ThemeSelectorComponent]
})
export class AppComponent implements OnInit {
  title = 'shell';
  activeUser: User | undefined;
  @ViewChild('sidebar') sidebar: SidebarComponent;


  toggleSidebar() {
    this.sidebar.toggleSidebar();
  }
  constructor(private userCommunicationService: UserCommunicationService, private router: Router) {
    console.log('UserCommunicationService instance:', userCommunicationService);

  }

  ngOnInit(): void {
    localStorage.clear();
    this.userCommunicationService.getUser().subscribe(user => {
      this.activeUser = user;
      localStorage.setItem('activeUser', JSON.stringify(user));
      window.dispatchEvent(new StorageEvent('storage', { key: 'activeUser' }));
      this.router.navigate(['/profile']);
    });
  }

}
