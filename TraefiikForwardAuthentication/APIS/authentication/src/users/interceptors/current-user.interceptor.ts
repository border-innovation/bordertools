import { CallHandler, ExecutionContext, Injectable, NestInterceptor, UseInterceptors } from "@nestjs/common";
import { UsersService } from "../users.service";


@Injectable()
export class CurrentUserInterceptor implements NestInterceptor{

    constructor(
        private userService: UsersService
    ){}

    async intercept(context: ExecutionContext, next: CallHandler<any>){
        // Run something before the request is handled
        const request = context.switchToHttp().getRequest()
        const { userId } = request.session || {};
        
        if(userId){
            const user = await this.userService.find(userId);
            request.currentUser = user;
        }
        return next.handle();
    }
}