import { Body, Headers, Controller, Get, NotFoundException, Param, Post } from '@nestjs/common';
import { createMessageDto } from './dto/create-message.dto';
import { MessagesService } from './messages.service';
import axios from 'axios';

@Controller('messages')
export class MessagesController {

    constructor(
        public messageService: MessagesService
        ){
    }

    @Get("/verify")
    verify(@Headers() headers: any) {
        return "Messages API verified!"
    }
    @Get()
    listMessages(@Headers() headers: any) {
        return this.messageService.findAll();
    }

    @Post()
    createMessage(@Body() body: createMessageDto ){
        return this.messageService.create(body.content)
    }

    @Get('/:id')
    async getMessage(@Param('id') id: string){
        const message= await  this.messageService.findOne(id)
        if(!message) throw new NotFoundException("Message not found");
        return message;
    }

}
