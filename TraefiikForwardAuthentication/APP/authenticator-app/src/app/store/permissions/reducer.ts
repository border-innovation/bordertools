import {  PermissionActions, PermissionsActionTypes } from "./actions";
import { PermissionModel } from "./models/permissions.model";

export interface PermissionsState {
  permissions: PermissionModel[];
  isLoading: boolean;
}

export const initialState: PermissionsState = {
  permissions: [],
  isLoading: false,
};

export function permissionsReducer(
  state = initialState,
  action: PermissionActions
): PermissionsState {
  switch (action.type) {
    case PermissionsActionTypes.GET_ALL:
      return {
        ...state,
        isLoading: true,
      };
    case PermissionsActionTypes.GET_ALL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        permissions: action.permissions,
      };
    case PermissionsActionTypes.GET_ALL_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case PermissionsActionTypes.ADD_NEW:
      return {
        ...state,
        isLoading: true,
      };
    case PermissionsActionTypes.ADD_NEW_SUCCESS:
      return {
        ...state,
        isLoading: false,
        permissions: [...state.permissions, action.permission],
      };
    case PermissionsActionTypes.ADD_NEW_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case PermissionsActionTypes.UPDATE_ONE:
      return {
        ...state,
        isLoading: true,
      };
    // case PermissionsActionTypes.UPDATE_ONE_SUCCESS:
    //   const updatedApisList = state.apis.map((r) => {
    //     if (r.id !== action.api.id) return r;
    //     return { ...r, ...action.api };
    //   });
    //   return {
    //     ...state,
    //     isLoading: false,
    //     permissions: [...updatedApisList],
    //   };
    case PermissionsActionTypes.UPDATE_ONE_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case PermissionsActionTypes.REMOVE_ID:
      return {
        ...state,
        isLoading: true,
      };
    case PermissionsActionTypes.REMOVE_ID_SUCCESS:
      return {
        ...state,
        isLoading: false,
        permissions: state.permissions.filter((v) => v.id !== action.permission.id),
      };
    case PermissionsActionTypes.REMOVE_ID_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
