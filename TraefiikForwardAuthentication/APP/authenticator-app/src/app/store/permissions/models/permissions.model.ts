export interface PermissionModel {
  id?: string;
  username: string;
  role: string;
}
