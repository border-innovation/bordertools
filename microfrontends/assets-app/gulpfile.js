import gulp from 'gulp';
import changed from 'gulp-changed';
import sourcemaps from 'gulp-sourcemaps';

import dartSass from 'sass'
import gulpSass from 'gulp-sass'
const sass = gulpSass(dartSass)

const scssSrc = './src/assets/scss/**/*.scss';
const cssDest = './src/assets/css';

// Only process changed SCSS files
function processChangedSCSS() {
  return gulp.src(scssSrc)
    .pipe(changed(cssDest, { extension: '.css' }))
    .pipe(sourcemaps.init()) // Initialize sourcemaps
    .pipe(sass({
      includePaths: ['./node_modules/'] // Include the node_modules path
    }).on('error', sass.logError))
    .pipe(sourcemaps.write(cssDest)) // Write the sourcemaps
    .pipe(gulp.dest(cssDest));
}

function watchSass() {
  gulp.watch('./src/assets/scss/**/*.scss', processChangedSCSS);
}

export { watchSass };
