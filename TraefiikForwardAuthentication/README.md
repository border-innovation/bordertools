## Test App on Border Server

1. Connect to Border VPN

2. Access:

App:
http://vasco.tst.berzuk.com:8000/authenticator/login

Traefik Dashboard
http://vasco.tst.berzuk.com:8050/dashboard/#/

3. If there are errors in cluster, check ONE TIME STEPS under file /playbook-cluster-tst.yaml


## API's Swaggers

http://vasco.tst.berzuk.com:8000/apps/swagger/api
http://vasco.tst.berzuk.com:8000/auth/swagger/api

### Run Locally with Kind cluster 

1. Run script ./local-cluster.sh

2. Port forward traefik:

kubectl port-forward $(kubectl get pods --selector "app.kubernetes.io/name=traefik" --output=name) 8000:8000 9000:9000

3. Access App:

http://localhost:8000/authenticator/login

Login Credentials:
    User: revesv
    pass: 123

4. Access traefik dasboard:

http://localhost:9000/dashboard/#/
or
http://localhost:8050/dashboard/#/

# NOTES:
- To test requests all API's have a folder /requests with test files

- Changed port of Auth-API to 4000, if auth don't work check for references for 3000 port.

- Traefik-chart-values was changed, no references to port 9000, now its port 8050, might be different for local cluster.

- Sidecar Proxy: Started by proving concept with the example at ./sidecar-proxy, still using that docker image. Then adapted the app-manager deployment file to deploy both images in one pod. (deploymentSideProxy.yaml)


### DOCUMENTATION

## 1. Intro

Tradicionalmente, o controle de acesso a APIs é realizado dentro das próprias APIs, através de bibliotecas Node.js responsáveis pela validação de tokens ou utilizadores. 

Desta forma, cada API é responsável por gerir o processo de validação de forma independente, armazenando as suas próprias chaves de autenticação, sem um ponto centralizado de controlo.

![Screenshot 2024-09-10 at 15.57.38.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/5d1f89cc-c5f0-4514-8df3-0866e2475b4e/30748d85-8f9b-4dfd-8ee1-3cce8af96eec/Screenshot_2024-09-10_at_15.57.38.png)

Utilizando como exemplo, a infraestrutura da Vodafone, as APIs estão hosted num cluster Kubernetes, e onde acesso é centralizado através de um reverse proxy, como o Traefik. Este proxy encarrega-se de encaminhar os requests para as APIs.

Dentro de cada API, o token é validado juntamente com as informações associadas, como permissões (roles) e configurações. 

Embora a segurança global dependa do proxy (Traefik), a responsabilidade pela validação final do token recai sobre cada API. Isso implica que os developers são responsáveis pela proteção de cada API individual. 

No entanto, nesta infraestrutura se uma API for comprometida, todas as outras estão comprometidas, visto que o Token Key está presente em todas.

## Concept

Pretende-se avaliar soluções alternativas e uma das mais promissoras é o **Traefik Forward Authentication.**

O middleware Forward Authentication delega a autenticação para um serviço externo (neste caso: Authentication API). Se o serviço responder com um código 2XX, o acesso é concedido e o pedido original é executado. Caso contrário, a resposta do servidor de autenticação é devolvida.

Além da avaliação de possíveis alternativas, pretende-se criar um protótipo demonstrando a capacidade do Traefik de fazer Forward Authentication.

Para criar um protótipo que demonstre a capacidade do Traefik de realizar Forward Authentication,  precisamos de configurar um ambiente onde o Traefik atua como um reverse proxy e encaminha pedidos de autenticação para um serviço externo (nova API) antes de conceder acesso aos recursos protegidos.

Nesta solução, continuamos a ter o Traefik como reverse proxy, e todos os pedidos que entram no Traefik são antes re-direcionados para a API de Autenticação.

Traefik Forward Authentication Documentation: [Traefik ForwardAuth Documentation - Traefik](https://doc.traefik.io/traefik/middlewares/http/forwardauth/)

### Vantagens / Objectivos desta Implementação:

- **Redirecionamento Centralizado:** Todos os pedidos que entram na infraestrutura são redirecionados para a Auth API, juntamente com o token de autenticação.
- **Validação Avançada de Token:** A validação de tokens é mais complexa, não verifica apenas  a existência do token, mas também valida se o token está expirado, verifica os grupos de utilizadores e o endpoint ao qual o utilizador está a aceder.
- **Centralização da Chave de Token:** A chave de autenticação do token é centralizada numa única localização, em vez de ser replicada em todas as aplicações. Isto garante que, mesmo que uma aplicação seja comprometida, as restantes permanecem seguras.
- **Eliminação de Ficheiros de Chaves nas Aplicações:** Deixa de ser necessário armazenar ficheiros de chave em cada aplicação, centralizando o controlo de acesso e reduzindo riscos de segurança.
- **Separação entre APIs (Backends) e Frontends:** A arquitetura permite uma clara separação entre o acesso ao front-end e aos back-ends, simplificando a gestão e aumentando a segurança.
- **Remoção de Bibliotecas Internas para Segurança nas APIs:** Idealmente, as APIs podem deixar de depender de bibliotecas internas de proteção (como NetsJs Guards), uma vez que a segurança passa a ser centralizada para toda a infraestrutura.
- **Resiliência em Caso de Comprometimento:** Mesmo que um pod seja comprometido, a validação de tokens continua a ser feita pelo Traefik, mantendo os outros pods protegidos e seguros. Sem que informação sensivel seja comprometida.


## Proof of concept

Começamos por criar um protótipo que valida o conceito de Traefik Forward Auth.

Nesta implementação tem de ser possivel:

- Login para obtenção de token jwt
- Dummy API sem qualquer proteção e sem token key
- Chamada a dummy api com sucesso (passando pelo Forward Authentication)
- Chamada a dummy api com insucesso:
    - jwt expirado
    - sem permissões por falta de audience
    - sem permissões por falta de token jwt
    - Chamada direta ao port da Dummy API

Para tal foi necessário desenvolver:

1. **Auth API** em nestjs com endpoints de:
    - /login: dado user e password, gera um jwt válido
    - /authorize: valida token e autoriza o acesso a outras API’s
2. **Dummy API** em nestjs com endpoints para testes
3. Traefik Proxy configurado para Forward Authentication

Com estas 2 API’s lançadas em docker containers, foi possivel validar todos os pressupostos.

**QUESTION: WHY NOT VALIDATE TOKEN INSIDE TRAEFIK?** É possível ter a validação do token dentro do traefik com a utilização de plugins mas dessa forma não é validado apps nem roles apenas se Token é valido. Não é escalavel.

https://plugins.traefik.io/plugins/6416508caef219d3834c2cac/jwt-validation-middleware

## Implementation

Neste capitulo está descrita a implementação das API’s descritas antes:

### Authentication API (API Manager)

A API de Autenticação, desenvolvida em Nest JS, divide-se em 2 módulos Auth e Endpoints.

### Auth Module

O modulo ‘Auth’ é utilizado para a gestão de users, com possibilidade de:

- Criar Users
- Log In User - Devolve JWT Token
- Listar Users
- Detalhes de User
- Atualização de users

### Endpoint Module

O modulo ‘Endpoints’ é utilizado para a gestão de API’s, cada API é definida por um endpoint, verbo http e role de acesso.

### Base de Dados

| BD | FIELDS |
| --- | --- |
| Users | Username, FullName, Email, Password |
| Endpoints | Role, Endpoint, Verb |
| User_Roles | Role, Username |
- **Users**: Tabela com informação sobre utiliadores
- **Endpoints**: Tabela com informação de API’s, endpoint de acesso, role para aceder e verbo http. É utilizado o verbo para distinguir acessos a consulta de informação (GET) e acesso com permissão para inserir dados (POST/PUT/PATCH).
- **User_Roles**: Tabela com relação entre username e role de acesso, para validar acesso a API.   Cada vez que se quer dar permissão a um novo user para aceder uma API , é inserido um registo na tabela.

### APP Manager

App Manager API, desenvolvida em Nest JS, é utilizada para a gestão de aplicações, isto é, gestão de FrontEnds. 

Era um dos objectivos iniciais conseguir a separação de permissões para APPs e API’s (FrontEnds e BackEnds). Foi utilizada esta abordagem de separação, visto que cada FrontEnd pode utilizar multiplas API’s, e nestes casos, dependia do developer proteger o acesso a certas API’s durante desenvolvimento. 

Desta forma é possivel dar acesso a uma aplicação e este não ter acesso a qualquer informação na aplicação sem que seja atribuido. 
Podendo ter diferentes niveis de acessos dentro de cada aplicação.

A APP Manager é constituida por apenas um módulo, onde é possivel:

- Criar ‘Apps’ para aceder
- Listar todas as Apps com acesso perante token.
- Listar todas as Apps da Infraestrutura
- Detalhes de App
- Atualização de Apps

### Base de Dados

| BD | FIELDS |
| --- | --- |
| Apps | AppName, Endpoint, Role |
- **Apps**: Tabela com informação de Apps disponiveis, endpoint de acesso e role para aceder. A relação entre este role e o user é tambem guardado na tabela User_roles da API Manager.

## Multiple pods and communication

### Multiple Containers

Tendo mais do que um container (Dummy API A, Dummy API B), como conseguimos limitar os pedidos entre containers?

- Requisitos:

Apenas pedidos encaminhados pelo traefik podem ser entregues nos containers

Traefik garante que, primeiro, o acesso é validado pelo Auth

Containers na mesma stack podem fazer pedidos entre si

Containers de stacks diferentes não conseguem fazer pedidos directos entre si, devem fazê-los ao Traefik e este depois encaminha para o destino

Containers não fazem pedidos directos ao Auth, devem fazê-los ao Traefik e este depois encaminha para o destino

- Teste:

Dentro do container da API A é feito um pedido direto para o container da API B. Este pedido tem de ser recusado. 

SOLUTION 1: DOCKER NETWORKS

<aside>
💡

Esta solução, ainda que eficaz numa infraestrutura pequena como este exemplo, consome muitos recursos, e num ambiente mais complexo não é a abordagem correta.

</aside>

A primeira abordagem encontrada foi utilizar Docker Networks.

Questões de DEVOPS:

Como adicionar, dinamicamente, o Traefik a uma network?

docker network connect dummy-network traefik

docker network connect messages-network traefik

SOLUTION 2: KUBERNETES NETWORK POLICIES

De forma a testar esta solução foi necessário criar um cluster de kubernetes local com as docker images utilizadas no exemplo anterior.

Para isto foi o utilizado o KIND (Kubernetes IN Docker), this tool creates a local cluster running on a Docker container on your machine.

Definimos uma network policy que seleciona todos os pods.

Nestes pods, apenas o tráfico vindo do pod do Traefik pode fazer pedidos para esta API.



Esta solução é escalável numa grande infraestrutura e fácil de implementar globalmente.


## Pod Security

Ate ponto da implementação foi possivel provar:

- Não existe comunicação entre pods/containers;
- Todos os requests passam pelo traefik;
- Não existe comunicação dos pods com o exterior;

Ainda assim, toda a segurança do ambiente fica no Traefik, nomeadamente no Traefik Forward Authentication

O que acontece caso o Traefik seja comprometido?

Nestes caso, pode haver comunicação entre o traefik e os pods no cluster, sem passar pela Auth API, consequentemente, sem token.

Solução: https://medium.com/@lukas.eichler/securing-pods-with-sidecar-proxies-d84f8d34be3e

### SIDE CAR PROXY

Ao invés da forward authentication ficar ao cargo apenas do Traefik, dentro de cada pod podemos ter um sidecar proxy que faz este mesmo redirecionamento.

Isto é, dentro de cada pod temos 2 imagens, a imagem da API e por cima uma imagem do proxy (TraefiikForwardAuthentication/sidecar-proxy/nginx-proxy/nginx.conf) que faz o forward para a Auth API.

Desta forma, expomos apenas o porto do sidecar e o porto da API deixa de ser exposto no manifesto de kubernetes, estando apenas acessivel internamente ao pod.

Esta implementação permite que:

 - Em cada pod, o único port exposto para fora é o port 80 (porto do side car);
 - As API's nunca ficam expostas fora do pods, o porto da API deixa de ser exposto no manifesto de k8s;
 - As API's apenas escutam pedidos no 127.0.0.1:3000, isto é, apenas o sidecar consegue redirecionar os pedidos para a API.

Isto permite que não seja possivel fazer pedidos diretamente pela shell traefik, nem de qualquer outro pod sem que se tenha um token válido, pois todos os pedidos passam no sidecar e consequentemente na autenticação.

### Nginx.conf implementation

Este proxy tem um funcionamento bastante simples:

- Escuta no porto 80
- Envia o pedido para http://vasco.tst.berzuk.com:8000/auth;
- Injeta os headers: X-Forwarded-method & X-Forwarded-uri , com os dados recebidos no pedido 
- Valida a resposta 200 OK da Auth API
- Redireciona para o 127.0.0.1:3000/{{ X-Forwarded-uri }}
- Devolve a resposta


resolver 10.96.0.10;:
Specifies the DNS resolver (in this case, 10.96.0.10) that NGINX will use to resolve domain names in proxy_pass or other directives that involve dynamic name resolution. This is typically necessary when you're using a domain name (rather than a static IP address) for backend services, as NGINX needs to resolve the domain to an IP address.

The IP 10.96.0.10 is the Kubernetes DNS service (as Kubernetes clusters usually have DNS services running on a similar IP range). This is common in Kubernetes clusters to resolve services within the cluster (like service-name.namespace.svc.cluster.local).

valid=10s;:
Specifies how long the DNS results (i.e., the resolved IP address) will be cached by NGINX. In this case, NGINX will cache the DNS lookup results for 10 seconds before it queries the DNS server again to check for updates.

Use case:
This is useful in environments like Kubernetes where the backend service IPs can change frequently, but the service domain name remains the same. By using a resolver, NGINX ensures that it dynamically resolves the backend's IP address rather than using a hardcoded IP. The valid=10s ensures that the DNS cache is refreshed often, in case the service IP changes.


## Ports and Routes

| PRODUCT | PORT |  |
| --- | --- | --- |
| Traefik Dashboard | 8050 | [http://192.168.59.101:30271](http://192.168.59.101:30271/) |
| Traefik Web | 8000 | [http://192.168.59.101:30601](http://192.168.59.101:30601/)  |
| Traefik Web Secure | 8043 | [http://192.168.59.101:31535](http://192.168.59.101:31535/) |


## Performance Tests

The method /validate, used to validate the token and the domain, is availble for 2 different protocols REST and GRPC.
In order to know what's the best approacht to use this endpoint there was a need to do performance tests.

#### HTTP

For this tests i used Postman Desktop APP.
Documentation: https://learning.postman.com/docs/collections/performance-testing/performance-test-configuration/

### GRCP

For this I used the tool ghz:

Documentation: https://ghz.sh/docs/intro

Example command to run the tests:

ghz --insecure \
  --proto ./auth.proto \
  --call auth.AuthService/Validate \
  -d '{
  "forwardUri": "/apps/token",
  "forwardMethod": "GET",
  "token": "eyJhbGciOiJIUzM4NCIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJSRVZFU1YiLCJ1c2VybmFtZSI6InJldmVzdiIsImZ1bGxOYW1lIjoiVmFzY28gUmV2ZXMiLCJtYWlsIjoidmFzY29AYm9yZGVyLmNvbSIsImFwcHMiOltdLCJlbnZpcm9ubWVudCI6InRzdCIsImlhdCI6MTczNDYxNjc3MSwiZXhwIjoxNzM0NjIzOTcxLCJhdWQiOlsiQWNjZXNzTWVzc2FnZXNBcHAiLCJBY2Nlc3NEdW1teUFwcCIsIlJFQURfQVBJIiwiV1JJVEVfQVBJIiwiQURNSU5fQVBJIl0sImlzcyI6ImF1dGhfYXBwIn0.3xpeLRlh8Tll3N7QS-NeAdStQSO7Ozh5R_-aTM-yXUo0BD3AOxzfGaZX-VzlDxQG"
}' \
  -z 2m \
  -c 5 \
  -n 1 \
  0.0.0.0:5001

--proto: proto file for the GRPC request

--call: Service name defined in the proto file

-z: Duration of the tests

-c: Number of concorrunt users

-n: Total nr requests 

-d: Body of the request