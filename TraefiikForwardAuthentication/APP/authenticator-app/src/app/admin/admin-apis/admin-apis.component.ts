import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectAPIs } from '../../store/apis/selector';
import { MatTableDataSource } from '@angular/material/table';
import { AddAPIRequest, ListAPIsRequest, RemoveAPIByIdRequest, UpdateAPIRequest } from '../../store/apis/actions';
import { MatSort } from '@angular/material/sort';


// TODO: Should add name to API's ??
interface api {
  id?: string;
  endpoint: string;
  role: string;
  verb: string;
  isEdit?: boolean;
}

@Component({
  selector: 'app-admin-apis',
  templateUrl: './admin-apis.component.html',
  styleUrl: './admin-apis.component.scss'
})
export class AdminApisComponent implements OnInit/* , AfterViewInit */ {
  // @ViewChild(MatSort) sort: MatSort;

  createAPIForm: FormGroup;
  editAPIForm: FormGroup;

  dataSource;
  apiList: api[];
  displayedColumns: string[] = ['endpoint', 'role', 'verb', 'actions'];


  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private cdr: ChangeDetectorRef // Add ChangeDetectorRef
  ){
    this.createAPIForm = this.formBuilder.group({
      endpoint: new FormControl(""),
      role: new FormControl(""),
      verb: new FormControl(""),
    });

    this.editAPIForm = this.formBuilder.group({
      endpoint: new FormControl(""),
      role: new FormControl(""),
      verb: new FormControl(""),
    });
  }

 /*  ngAfterViewInit() {
    console.log("Sorting API: ", this.sort, this.dataSource)

    if (this.sort && this.dataSource) {
      this.dataSource.sort = this.sort;
    } else {
      console.error('API MatSort or dataSource is not initialized properly.');
    }
  } */

  ngOnInit() {
    // Subscribe to the combined apps and loading state
    this.store.select(selectAPIs)
      .subscribe(({ apis, isLoading }) => {
        // console.log("NgOnInit APIs: ", apis)
        if (apis && apis.length > 0) {
          this.apiList = apis;
          this.dataSource = new MatTableDataSource(this.apiList);
          // this.dataSource.sort = this.sort; 
        } else if (!isLoading) {
          console.log("No API's available...");
        }
        this.cdr.detectChanges();
      });
  }

  onSubmit(): void {
    if (this.createAPIForm.valid) {
      const apiData = this.createAPIForm.value; // Access form values
      console.log('Form Submitted:', apiData);
      this.store.dispatch(new AddAPIRequest(apiData));
      this.createAPIForm.reset();
    } else {
      console.log('Form not valid');
    }
  }


  removeAPI(api: api) {
    console.log("Deleting api: ", api)
    this.store.dispatch(new RemoveAPIByIdRequest(api.id));

  }

  editAPI(api: api) {
    console.log("IsEditingRow api: ", api)

    this.editAPIForm.patchValue({
      endpoint: api.endpoint,
      role: api.role,
      verb: api.verb,
    });


    if (api.isEdit) {
      this.updateAPI(api); // Call save method when "Done" is clicked
    }
    api.isEdit = !api.isEdit; // Toggle edit mode
  }

  updateAPI(api: api) {
    // TODO
    // Validate Response, if ok alert ui
    console.log('Saving api:', api);
    this.store.dispatch(new UpdateAPIRequest(api));

  }

  cancelEdit(api: api){
    api.isEdit = !api.isEdit;
  }
}
