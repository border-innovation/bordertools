import { createAction, props } from "@ngrx/store";
import { User } from "../models/user";



export const Login = createAction(
  '[Com] Login',
  props<{user: User, int: string}>()
);
