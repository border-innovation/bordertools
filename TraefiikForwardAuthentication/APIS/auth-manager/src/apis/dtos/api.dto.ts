import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";

export class APIDto{
    @ApiProperty()
    @Expose()
    id: string;
    @ApiProperty()
    @Expose()
    name: string;
    @ApiProperty()
    @Expose()
    endpoint: string;
    @ApiProperty()
    @Expose()
    role: string;
    @ApiProperty()
    @Expose()
    verb: string;
}