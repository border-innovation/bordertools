# RUN DOCKER

docker-compose -f docker-compose.yml up --build

docker-compose -f apis-traefik-auth.yml -f apis-services.yml up --build

# K8S cluster 

## Create KIND cluster with registry for local Docker images
    
Run script 
./local_cluster.sh

OR 

## Create just the cluster
    
kind create cluster

## Install calico for network policies

### Install the operator on your cluster.

kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.28.0/manifests/tigera-operator.yaml


### ONLY ONE TIME - Download the custom resources necessary to configure Calico (already in repo)

curl https://raw.githubusercontent.com/projectcalico/calico/v3.28.0/manifests/custom-resources.yaml -O

### Create the manifest to install Calico.

kubectl create -f custom-resources.yaml


## Create docker image tags

docker tag traefiikforwardauthentication_messages-api:latest localhost:5001/traefiikforwardauthentication_messages-api:latest

docker tag traefiikforwardauthentication_dummy-api:latest localhost:5001/traefiikforwardauthentication_dummy-api:latest

docker tag traefiikforwardauthentication_auth-api:latest localhost:5001/traefiikforwardauthentication_auth-api:latest

docker tag traefiikforwardauthentication_apps-api:latest localhost:5001/traefiikforwardauthentication_apps-api:latest

docker tag traefiikforwardauthentication_authenticator-app:latest localhost:5001/traefiikforwardauthentication_authenticator-app:latest



## Push to KIND registry

docker push localhost:5001/traefiikforwardauthentication_messages-api:latest
docker push localhost:5001/traefiikforwardauthentication_dummy-api:latest
docker push localhost:5001/traefiikforwardauthentication_apps-api:latest
docker push localhost:5001/traefiikforwardauthentication_auth-api:latest
docker push localhost:5001/traefiikforwardauthentication_authenticator-app:latest


## Install TRAEFIK with helm

helm install traefik traefik/traefik -f ./k8s/traefik-chart-values.yaml 

If using flag:
--namespace traefik
kubectl create namespace traefik

### Update TRAEFIK with value chart
helm upgrade traefik traefik/traefik -f ./k8s/traefik-chart-values.yaml

### Get cluster CIDR

kubectl cluster-info dump | grep -m 1 cluster-cidr
"--cluster-cidr=192.168.0.0/16"

traefik Possible values:
- "--entryPoints.web.forwardedHeaders.insecure"
- "--entryPoints.websecure.forwardedHeaders.insecure"
- "--entryPoints.web.forwardedHeaders.trustedIPs=192.168.0.0/16"
- "--entryPoints.websecure.forwardedHeaders.trustedIPs=192.168.0.0/16"


## Apply API deployment manifests to cluster

kubectl apply -f ./k8s/deployment.yaml

## Run port forward

kubectl port-forward $(kubectl get pods --selector "app.kubernetes.io/name=traefik" --output=name) 8000:8000 9000:9000

9000 -> Trafeik dashboard
8000 -> web

### IF TRAEFIK IS CREATED IN NAMESPACE: -n traefik

kubectl port-forward -n traefik $(kubectl get pods -n traefik --selector "app.kubernetes.io/name=traefik" --output=name) 8000:8000 9000:9000


# DEMO 

## Get Pod info all namespaces
kubectl get pods -o wide

## Before setting network policies - should pass
Request to messages:
curl http://192.168.82.15:3000/messages/verify

Request to dummy:
curl http://192.168.82.10:4000/dummy/verify 

# Errors

## 1
ERROR TRAEFIK:"no matches for KIND "IngressRoute" in version "traefik.containo.us/v1alpha1" ensure CRDs are installed first":

kubectl apply -f https://raw.githubusercontent.com/traefik/traefik/v2.11/docs/content/reference/dynamic-configuration/kubernetes-crd-definition-v1.yml

## 2

Error: INSTALLATION FAILED: repo traefik not found

helm repo add traefik https://traefik.github.io/charts
"traefik" has been added to your repositories


######
# Get VIM in container

apt-get update 
apt-get install vim 


docker build -t traefiikforwardauthentication_authenticator-app:latest .

docker run -p 4200:80 traefiikforwardauthentication_authenticator-app:latest