import { Body, Controller, Post } from '@nestjs/common';
import { MailDto } from './mail.dto';
import { MailService } from './mail.service';


@Controller('mail')
export class MailController {

    constructor(private readonly mailService: MailService){}

    @Post('post')
    postmail(@Body() mail: MailDto){
        return this.mailService.postmail(mail);
    }

}
