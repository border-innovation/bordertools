import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsEmail, IsOptional, IsString } from "class-validator";

export class CreateAppDto{
    @ApiProperty()
    @IsString()
    name: string;
    
    @ApiProperty()
    @IsString()
    endpoint: string; 

    @ApiProperty()
    @IsString()
    role: string;

}