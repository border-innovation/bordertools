package border.tools.rules;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.FlowGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;
import border.tools.utils.BorderUtils;

@Rule(key = "S00023", name = "Certain Catch Sequences must call getLastError service", priority = Priority.MAJOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class LastErrorCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(WrapperInvokedServicesCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(FlowGrammar.SEQUENCE);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String serviceName = FlowUtils.getQualifiedName(this.getContext().getFile());
		String filename = this.getContext().getFile().getAbsolutePath();
		if (serviceName.endsWith("_wrp") || BorderUtils.checkIfRestService(serviceName, filename)
				|| BorderUtils.checkIfLegacyRestService(serviceName)
				|| BorderUtils.checkIfSoapService(serviceName, filename)) {
			String sequenceType = getSequenceType(astNode);
			if (sequenceType != null && sequenceType.equalsIgnoreCase("DONE")) {
				checkCatchServices(getContent(astNode));
			}
		}
	}

	private void checkCatchServices(AstNode catchNode) {
		List<AstNode> catchServicesList = catchNode.getChildren(FlowGrammar.INVOKE);
		if (catchServicesList.size() != 1) {
			getContext().createLineViolation(this, "Catch Sequence must only invoke pub.flow:getLastError ", catchNode);
		}
		AstNode lastErrorNode = catchNode.getFirstChild(FlowGrammar.INVOKE);
		AstNode attributes = lastErrorNode.getFirstChild(FlowGrammar.ATTRIBUTES);
		if (attributes != null) {
			AstNode service = attributes.getFirstChild(FlowAttTypes.SERVICE);
			if (service != null) {
				if (!service.getTokenValue().equalsIgnoreCase("pub.flow:getLastError")) {
					getContext().createLineViolation(this, "Catch Sequence must only invoke pub.flow:getLastError.",
							lastErrorNode);
				}
			}
		}
	}

	private String getSequenceType(AstNode sequenceNode) {
		if (sequenceNode != null) {
			AstNode attributes = sequenceNode.getFirstChild(FlowGrammar.ATTRIBUTES);
			if (attributes != null) {
				AstNode exitOn = attributes.getFirstChild(FlowAttTypes.EXITON);
				if (exitOn != null) {
					return exitOn.getTokenValue();
				}
			}
		}
		return null;
	}

	private AstNode getContent(AstNode sequenceNode) {
		if (sequenceNode != null) {
			return sequenceNode.getFirstChild(FlowGrammar.CONTENT);
		}
		return null;
	}

	@Override
	public boolean isFlowCheck() {
		return true;
	}

	@Override
	public boolean isNodeCheck() {
		return false;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}

}
