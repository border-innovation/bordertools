import { Component } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
@Component({
  selector: 'app-review-full',
  standalone: true,
  imports: [MatCardModule],
  templateUrl: './review-full.component.html',
  styleUrl: './review-full.component.scss'
})
export class ReviewFullComponent {

}
