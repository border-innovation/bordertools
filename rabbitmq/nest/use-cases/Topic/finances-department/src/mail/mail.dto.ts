import { IsString } from "@nestjs/class-validator";

export class MailDto {
    @IsString()
    title: string;

    @IsString()
    topic: string;

    @IsString()
    mail: string;
}