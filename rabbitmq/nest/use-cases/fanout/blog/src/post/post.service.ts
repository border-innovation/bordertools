import { Injectable } from '@nestjs/common';
import { PostDto } from './post.dto';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class PostService {

    constructor() { }

    async addNewPost(post: PostDto, amqpConnection: AmqpConnection) {

        try {
            await amqpConnection.publish('notification-exchange',"subscribe-route", post );
            
            console.log('Message sent successfully:', post);
        } catch (error) {
            console.error('Failed to send message:', error);
            throw error;
        }
    }
}

