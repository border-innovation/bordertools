package border.tools.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class BorderUtils {

	public static boolean checkIfRestService(String service, String filename) {
		if (filename.contains("_\\") && filename.contains("_pub")) {
			try {
				String filepath = filename.substring(0, filename.indexOf("_\\"));
				File file = new File(filepath + "\\node.ndf");
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document doc = db.parse(file);
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("value");
				Node restResource = nodeList.item(0);
				if (restResource.getTextContent().equals("restResource")) {
					for (int i = 1; i < nodeList.getLength(); i++) {
						Node node = nodeList.item(i);
						NamedNodeMap attr = node.getAttributes();
						if (attr.getLength() > 0) {
							String name = attr.getNamedItem("name").getNodeValue();
							if (name.equals("serviceName") && node.getTextContent().equals(service)) {
								return true;
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public static Boolean checkIfLegacyRestService(String service) {
		if (service.contains("_pub")
				&& (service.endsWith("_get") || service.endsWith("_post") || service.endsWith("_put"))) {
			return true;
		}
		return false;
	}

	public static String getPubServiceACLName(String service) {
		return service.replace("_", "").replace(".", "_").replace(":", "_");
	}
	
	public static boolean checkIfSoapService(String service, String filename) {
		if (filename.contains("_pub")) {
			try {
				String filepath = filename.substring(0, filename.indexOf("_pub\\"));
				File file = new File(filepath + "\\_pub");
				List<File> wsdFiles = listFilesForFolder(file);
				for (File nodeFile : wsdFiles) {
					DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document doc = db.parse(nodeFile);
					doc.getDocumentElement().normalize();
					NodeList nodeList = doc.getElementsByTagName("value");
					for (int i = 1; i < nodeList.getLength(); i++) {
						Node node = nodeList.item(i);
						NamedNodeMap attr = node.getAttributes();
						if (attr.getLength() > 0) {
							String name = attr.getNamedItem("name").getNodeValue();
							if (name.equals("serviceName") && node.getTextContent().equals(service)) {
								return true;
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;

	}

	public static List<File> listFilesForFolder(final File folder) {
		List<File> wsdFiles = new ArrayList<File>();
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				File nodeFile = checkWebServiceDescriptors(fileEntry);
				if (nodeFile != null) {
					wsdFiles.add(nodeFile);
				}
			}
		}
		return wsdFiles;
	}

	private static File checkWebServiceDescriptors(File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.getName().equalsIgnoreCase("node.ndf")) {
				try {
					DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document doc = db.parse(fileEntry);
					doc.getDocumentElement().normalize();
					NodeList nodeList = doc.getElementsByTagName("value");
					Node webServiceDescriptor = nodeList.item(0);
					if (webServiceDescriptor.getTextContent().equalsIgnoreCase("webServiceDescriptor")) {
						return fileEntry;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
