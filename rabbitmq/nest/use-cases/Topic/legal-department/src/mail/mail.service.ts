import { AmqpConnection, RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { MailDto } from './mail.dto';

@Injectable()
export class MailService {
    @RabbitSubscribe({
        exchange: 'topic_exchange',
        routingKey: ['all.#', 'legal.#', '#.legal.#']
    })
    async getMail(mail: MailDto) {
        console.log('[legal] ', mail);
    }
}
