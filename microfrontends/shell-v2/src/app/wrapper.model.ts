export interface WrapperConfig{
  remoteName: string;
  exposeModule: string;
  elementName: string;
}

export const initWrapperConfig: WrapperConfig ={
  remoteName: '',
  exposeModule: '',
  elementName: ''
}
