import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ClientEntity } from './client.entity';
import { StoredProcedureService } from 'src/stored-procedure/stored-procedure.service';

@Injectable()
export class ClientService {

    constructor (
        @InjectRepository(ClientEntity) private clientRepository: Repository<ClientEntity>,
        private storedProcedure: StoredProcedureService
    ) {}

    public async getClients(): Promise<ClientEntity[]> {
        const result = await this.storedProcedure.getAllClients();
        //console.log(await this.clientRepository.manager.connection.driver.obtainMasterConnection());
        return result;
    }

    public async getClientById(id: number): Promise<any> {
        const client = await this.storedProcedure.getClientById(id);
        if (!client) {
            throw new HttpException('Not Found', 404);
        }
        return client;
    }

    public async createClient(client) {
        return await this.storedProcedure.createClient(client)
    }

    public async editClient(id: number, first_name: string, second_name: string): Promise<any> {
        const clientId = await this.storedProcedure.editClient(id, first_name, second_name);
        if (!clientId) {
            throw new HttpException('Not Found', 404);
        }
        return clientId;
    }

    public async deleteClient(id: number): Promise<number> {
        const idRemoved = await this.storedProcedure.removeClient(id);
        if (!idRemoved) {
            throw new HttpException('Not Found', 404);
        }
        return idRemoved;
    }
}
