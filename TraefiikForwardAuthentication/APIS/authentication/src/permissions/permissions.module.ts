import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Permission } from './permissions.entity';
import { PermissionService } from './permissions.service';

@Module({
    imports: [
      TypeOrmModule.forFeature([Permission])
    ],
    providers: [
     PermissionService
    ],
    exports: [
      PermissionService
    ]
  })
export class PermissionsModule {}
