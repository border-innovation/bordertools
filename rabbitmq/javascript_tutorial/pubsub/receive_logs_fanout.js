var amqp = require('amqplib/callback_api');

//Same as the sender to start the connection and declare the queue to consume

amqp.connect('amqp://localhost', (error0, connection) => {
    if (error0) throw error0;

    connection.createChannel((error1, channel) => {
        if (error1) throw error1;

        var exchange = 'logs';

        channel.assertExchange(exchange, 'fanout', {
            durable: false
        });

        // This makes sure the queue is declared before attempting to consume from it
        channel.assertQueue('', {
            exclusive: true
        }, function (error2, q) {
            if (error2) throw error2;
            console.log(` [*] Waiting for messages in ${q.queue}. To exit press CTRL+C`);

            channel.bindQueue(q.queue, exchange, '');

            channel.consume(q.queue, function (msg) {
                if (msg.content) {
                    console.log(" [x] ", msg.content.toString());
                }
            }, {
                noAck: true
            });
        });
    });
});