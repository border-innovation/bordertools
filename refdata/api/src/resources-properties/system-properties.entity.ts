import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { ResourcePropertiesEntity } from './resource-properties.entity';
import { ResourceSystemEntity } from './resource-system.entity';

@Entity('SYSTEMPROPERTIES')
export class SystemPropertiesEntity {
  
  @PrimaryColumn({
    name: 'SYSTEMID',
    type: 'number',
  })
  systemId: number;

  @PrimaryColumn({
    name: 'RESOURCEKEY',
    type: 'number',
  })
  resourceKey: number;

  @Column({
    name: 'RESOURCEVALUE',
    type: 'varchar2',
    length: 4000,
    nullable: false,
  })
  resourceValue: string;

  @ManyToOne(() => ResourcePropertiesEntity)
  @JoinColumn({ name: 'RESOURCEKEY' })
  resource: ResourcePropertiesEntity;

  @ManyToOne(() => ResourceSystemEntity)
  @JoinColumn({ name: 'SYSTEMID' })
  system: ResourceSystemEntity;
}