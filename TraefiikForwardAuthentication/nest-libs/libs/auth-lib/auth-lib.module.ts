import { Module } from '@nestjs/common';
import { AuthLibService } from './auth-lib.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule.register({
    timeout: 5000,
    maxRedirects: 5,
  }),],
  providers: [AuthLibService],
  exports: [
    AuthLibService, 
    HttpModule
  ],
})
export class AuthLibModule {

}
