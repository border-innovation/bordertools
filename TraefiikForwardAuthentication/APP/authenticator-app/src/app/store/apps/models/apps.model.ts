export interface AppModel {
  id?: string;
  appName: string;
  endpoint: string;
  role: string;
}
