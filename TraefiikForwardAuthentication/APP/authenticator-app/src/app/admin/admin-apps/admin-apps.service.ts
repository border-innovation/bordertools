import { Injectable, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { environment } from "../../../environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { AppModel } from "../../store/apps/models/apps.model";

@Injectable({
    providedIn: 'root'
  })
  
  export class AdminAppsService {
    constructor(
        private httpClient: HttpClient
    ){

    }

    public getApps(){
      let url = `${environment.API_BASE_URL}${environment.APPS_API}`;
      return this.httpClient.get<AppModel[]>(url);
  }

    public getAllApps(){
        let url = `${environment.API_BASE_URL}${environment.APPS_LIST_API}`;
        return this.httpClient.get<AppModel[]>(url);
    }

    /*
    Request body example:
      {
          "appName": "MessagesApp",
          "endpoint": "/messages",
          "role": "Access_Messages_Role"
      }
    */ 
    public addApps(app: any){
      let url = `${environment.API_BASE_URL}${environment.APPS_API}`;
      console.log("Adding app: ", url, app);
      return this.httpClient.post<AppModel[]>(url,app);
    }

    public updateApps(app: any){
      let url = `${environment.API_BASE_URL}${environment.APPS_API}/${app.id}`;
      console.log("Updating app service: ", url, app);
      return this.httpClient.patch<AppModel[]>(url,app);
    }

    public removeApp(id: string){
      let url = `${environment.API_BASE_URL}${environment.APPS_API}/${id}`;
      console.log("Deleting app service: ", url);
      return this.httpClient.delete<AppModel[]>(url);
    }
  }