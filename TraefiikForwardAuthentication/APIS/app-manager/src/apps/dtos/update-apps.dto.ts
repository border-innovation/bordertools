import { IsOptional, IsString } from "class-validator";

export class UpdateAppsDto{

    @IsString()
    @IsOptional()
    appName: string;

    @IsString()
    @IsOptional()
    endpoint: string; 

    @IsString()
    @IsOptional()
    role: string;

}