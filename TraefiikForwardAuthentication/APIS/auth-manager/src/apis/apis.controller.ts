import { Body, Controller, Delete, Get, NotFoundException, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { ApisService } from './apis.service';
import { CreateAPIDto } from './dtos/create-api.dto';
import { UpdateAPIDto } from './dtos/update-api.dto';

@Controller('apis')
export class ApisController {
    constructor(
        private apisService: ApisService
    ){}

    @Get()
    async getAllAPIS(){
        return this.apisService.findAll();
    }

    @Get('/:id')
    async getAPI(@Param('id') id: string){
        return await this.apisService.find(parseInt(id));
    }

    @Post()
    createAPI(@Body() body: CreateAPIDto){
        console.log("Creating Endpoint: ", body)
        return this.apisService.create(body.name, body.endpoint, body.role, body.verb);
    }

    @Patch('/:id')
    updateAPI(@Param('id') id: string, @Body() body: UpdateAPIDto){
        return this.apisService.updateAPI(parseInt(id), body)
    }

    @Delete('/:id')
    deleteAPI(@Param('id') id: string){
        return this.apisService.remove(parseInt(id));
    }
}
