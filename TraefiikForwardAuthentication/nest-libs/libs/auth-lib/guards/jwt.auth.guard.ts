import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from "@nestjs/common";
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from "rxjs";


@Injectable()
export class JwtAuthGuard implements CanActivate {
  constructor(
    private readonly httpService: HttpService
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    console.log("JWT AUDIT GUARD");

    const request = context.switchToHttp().getRequest();
    const token = this.getToken(request);

    if (!token) {
      throw new UnauthorizedException('Token not found');
    }
    
    const forwardUri = request.url;
    const forwardMethod = request.method;

    const body = {
      token: token,
      forwardUri: forwardUri,
      forwardMethod: forwardMethod,

  }
    console.log("body: ",  body )

    const authEndpoint = 'http://localhost:4000/auth/validate'; 

    try {
      console.log("Sending request to: ", authEndpoint)

      // Send a request to the endpoint with the token in the Authorization header
      const response = await this.httpService.post(authEndpoint, body).toPromise();
      
      console.log("Response: ", response.data)
      return true; // Token is valid, proceed with the request
    } catch (error) {
      console.error("Error Validating token: ", error?.data?.message);
      throw new UnauthorizedException('Token validation failed');
    }
  }

  getToken(request): string {
    // Check cookies
    if (request.cookies && request.cookies.token) {
        return request.cookies.token;
    } 

    // Check authorization header
    if (request.headers.authorization?.split(" ")[0] === "Bearer") {
        return request.headers.authorization.split(" ")[1];
    } 

    // Check rawHeaders for Authorization or Cookie with token
    for (let i = 0; i < request.rawHeaders.length; i++) {
        // Check for Authorization header
        if (request.rawHeaders[i].toLowerCase() === 'authorization' && request.rawHeaders[i + 1]?.startsWith("Bearer ")) {
            return request.rawHeaders[i + 1].split(" ")[1];
        }
        
        // Check for token in Cookie header
        if (request.rawHeaders[i].toLowerCase() === 'cookie') {
            const cookieHeader = request.rawHeaders[i + 1];
            const tokenMatch = cookieHeader.match(/token=([^;]+)/);
            if (tokenMatch) {
                return tokenMatch[1];
            }
        }
    }

    return undefined;
}


}
