import { CallHandler, ExecutionContext, NestInterceptor, UseInterceptors } from "@nestjs/common";
import { plainToClass } from "class-transformer";
import { Observable, map } from "rxjs";

interface ClassConstructor {
    new (...args:any[]): {}
}

export function Serialize(dto: ClassConstructor){
    return UseInterceptors(new SerializeInterceptor(dto))
}

export class SerializeInterceptor implements NestInterceptor{

    constructor(private dto: ClassConstructor){

    }

    intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        // Run something before the request is handled
        
        //console.log('im running  before the handler')

        return next.handle().pipe(
            map((data: any) => {
                //Run something before the response is sent out
                //console.log('im running after handler and before response is sent: ')
                return plainToClass(
                    this.dto, 
                    data, 
                    {
                        excludeExtraneousValues: true,
                    }
                )
            })
        )
    }
}