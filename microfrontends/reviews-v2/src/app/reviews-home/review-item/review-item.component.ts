import { Component, Input } from '@angular/core';
import { MatCardHeader, MatCardModule } from '@angular/material/card';

interface album{
  name: string;
  path: string;
}

@Component({
  selector: 'app-review-item',
  standalone: true,
  imports: [MatCardHeader, MatCardModule],
  templateUrl: './review-item.component.html',
  styleUrl: './review-item.component.scss'
})
export class ReviewItemComponent {
@Input() album: album | undefined;

}
