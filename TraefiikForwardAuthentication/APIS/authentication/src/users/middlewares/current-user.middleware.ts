import { Injectable, NestMiddleware } from "@nestjs/common";
import { User } from "../user.entity";
import { UsersService } from "../users.service";
import { NextFunction } from "express";

;

declare global {
    namespace Express {
        interface Request {
            currentUser?: User;
        }
    }
}

@Injectable()
export class CurrentUserMiddleware implements NestMiddleware {

    constructor(
        private usersService: UsersService
    ){}

    async use(req: any, res: any, next: NextFunction) {
        const user = req['user'] || {};
        const userId = user.userId
        console.log("user", user)

        if(userId){
            const user = await this.usersService.find(userId)
            req.currentUser = user;
        }

        next();
    }
}