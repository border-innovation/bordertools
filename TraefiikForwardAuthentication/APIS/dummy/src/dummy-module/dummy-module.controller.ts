import { Controller, Get } from '@nestjs/common';
import axios from 'axios';

@Controller('dummy')
export class DummyModuleController {

    constructor(
    ){

    }
    @Get("/validate")
    async validateConnectionBetweenContainers(){
        try {
            const response = await axios.get('http://172.20.0.5:3000/messages');
            return response.data; // Return the data from the response
        } catch (error) {
            throw new Error(`Failed to fetch data from messages API: ${error.message}`);
        }
    }
    @Get('/verify')
    async verify(){
        console.log("Dummy API Verified")
        return "Dummy API Verified"
        
    }
}
