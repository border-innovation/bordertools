import { createSelector } from "@ngrx/store";
import { GlobalState } from "../../app.store";
import { AppsState } from "./reducer";

// Define a selector that returns both apps and loading state
export const selectAppsState = (state: GlobalState) => state.apps;
export const selectAllAppsState = (state: GlobalState) => state.allApps;


export const selectApps = createSelector(
  selectAppsState,
  (state: AppsState) => {
    console.log("Selecting apps: ", state)
    return ({
    apps: state?.apps ?? [], 
    isLoading: state?.isLoading ?? false
  })}
);

export const selectAllApps = createSelector(
  selectAllAppsState,
  (state: AppsState) => {
    console.log("Selecting all apps: ", state)
    return ({
      allApps: state?.allApps ?? [], 
      isLoading: state?.isLoading ?? false
    })}
);