import { User } from "../models/user";
import { createReducer, on } from "@ngrx/store";
import * as AuthActions from './auth.actions';

export interface State{
  user: User | undefined;
}

const inicialState : State ={
  user: undefined
}

export const authReducer = createReducer(
  inicialState,
  on(AuthActions.Login, (state, action) => {
    const user = action.user;
    return {
      ...state, user: user
    }
  }),

);
