import { AuthService } from './../auth.service';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable, pipe } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

import { environment } from './../../../environments/environment';
// import { AlertService } from './../../shared/alert/alert.service';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

    snapshot: RouterStateSnapshot

    constructor(private router: Router, private cookieService: CookieService, private authenticationService: AuthService) {
        this.snapshot = router.routerState.snapshot;
    }

    /**
     * Deals with the errors from the server for all http responses
     * @param {HttpRequest} request
     * @param {HttpHandler} next
     * @returns {Observable<HttpEvent<any>>} the result
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request)
        .pipe(
            tap((event: HttpEvent<any>) => {
                //look only at the errors
            }, (error: HttpErrorResponse) => {
                switch (error.status) {
                    case 400:
                        this._400(); break;
                    case 401:
                        this._401(error.statusText); break;
                    case 500:
                        this._500(error); break;//
                    default: {
                        let _err = error.status + ": " + error.statusText;
                        // this.alert.error(_err, false);
                        break;
                    }
                }
            })
        );
    }

    /* handlers for each error type */

    /**
     * Client error (Bad request)
     */
    _400() {
        // this.alert.warn('Tenta novamente mais tarde', false);
    }

    /**
     * Client error (Unauthorized)
     */
    _401(statusText: string) {
        // this.alert.warn(statusText + ' - A redirecionar para login', false);

        if (this.snapshot.url === '/' || this.snapshot.url === '') {
            var returnUrl = "";
        } else {
            var returnUrl = '?returnUrl=' + this.snapshot.url;
        }
        setTimeout(() => {
            const domain = this.authenticationService.getDomain();
            this.cookieService.delete('u_session', '/', domain);
            window.location.href = environment.authentication_url + returnUrl;
        }, 3000);
    }

    /**
     * Server error (Internal Server Error)
     */
    _500(error) {
        let _err = error.status + ": " + error.statusText;
        // this.alert.error(_err, false);
    }
}
