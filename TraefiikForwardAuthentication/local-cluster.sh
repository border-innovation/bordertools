## Script to create local KIND cluster

#!/bin/bash
retry=0
max_retries=5
sleep_time=10

# Ensure the kind-with-registry.sh script is executable and run it
chmod +x ./APIS/kind-with-registry.sh
./APIS/kind-with-registry.sh

# Exit if the script fails
if [ $? -ne 0 ]; then
  echo "Failed to run kind-with-registry.sh"
  exit 1
fi

# Create Calico resources
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.28.0/manifests/tigera-operator.yaml
if [ $? -ne 0 ]; then
  echo "Failed to create tigera-operator"
  exit 1
fi

until kubectl create -f ./APIS/k8s/custom-resources.yaml; do
  retry=$((retry+1))
  if [ $retry -ge $max_retries ]; then
    echo "Failed to create custom resources after $max_retries attempts"
    exit 1
  fi
  echo "Retrying to create custom resources in $sleep_time seconds..."
  sleep $sleep_time
done


# Build Docker images
docker build -t traefiikforwardauthentication_authenticator-app ./APP/authenticator-app
docker build -t traefiikforwardauthentication_auth-api ./APIS/authentication
docker build -t traefiikforwardauthentication_messages-api ./APIS/messages
docker build -t traefiikforwardauthentication_dummy-api ./APIS/dummy-api
docker build -t traefiikforwardauthentication_apps-api ./APIS/app-manager
docker build -t traefiikforwardauthentication_nginx-proxy ./sidecar-proxy/nginx-proxy


# Tag and push Docker images
images=(
  "traefiikforwardauthentication_authenticator-app"
  "traefiikforwardauthentication_auth-api"
  "traefiikforwardauthentication_messages-api"
  "traefiikforwardauthentication_dummy-api"
  "traefiikforwardauthentication_apps-api"
  "traefiikforwardauthentication_nginx-proxy"
)

for image in "${images[@]}"; do
  docker tag "${image}:latest" "localhost:5001/${image}:latest"
  if [ $? -ne 0 ]; then
    echo "Failed to tag ${image}"
    exit 1
  fi

  docker push "localhost:5001/${image}:latest"
  if [ $? -ne 0 ]; then
    echo "Failed to push ${image}"
    exit 1
  fi
done

# Install Traefik with Helm
helm install traefik traefik/traefik -f ./k8s/traefik-chart-values.yaml
if [ $? -ne 0 ]; then
  echo "Failed to install Traefik"
  exit 1
fi


## NETWORK POLICIES
kubectl apply -f ./APIS/k8s/api-network-policy.yaml
if [ $? -ne 0 ]; then
  echo "Failed to apply API network policy"
  exit 1
fi

## MIDDLEWARES
kubectl apply -f ./APIS/k8s/api-middleware.yaml
if [ $? -ne 0 ]; then
  echo "Failed to apply API middleware"
  exit 1
fi

# Apply Kubernetes configurations

## Authentication API 
kubectl apply -f ./APIS/authentication/k8s/deployment.yaml
if [ $? -ne 0 ]; then
  echo "Failed to apply authentication deployment"
  exit 1
fi

## App Manager API
kubectl apply -f ./APIS/app-manager/k8s/deployment.yaml
if [ $? -ne 0 ]; then
  echo "Failed to apply app-manager deployment"
  exit 1
fi

## App Manager API (With SideCar proxy)
# kubectl apply -f ./APIS/app-manager/k8s/deploymentSideProxy.yaml
# if [ $? -ne 0 ]; then
#   echo "Failed to apply app-manager deployment"
#   exit 1
# fi

## Dummy API
kubectl apply -f ./APIS/dummy/k8s/deployment.yaml
if [ $? -ne 0 ]; then
  echo "Failed to apply dummy deployment"
  exit 1
fi

## Messages API
kubectl apply -f ./APIS/messages/k8s/deployment.yaml
if [ $? -ne 0 ]; then
  echo "Failed to apply messages deployment"
  exit 1
fi

## Authneticator APP Front End 
kubectl apply -f ./APP/authenticator-app/k8s/deployment.yaml
if [ $? -ne 0 ]; then
  echo "Failed to apply APP deployment"
  exit 1
fi

echo "Script executed successfully!!"
