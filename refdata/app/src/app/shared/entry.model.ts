export class Entry {
  constructor(
    public systemName: string,
    public resourceName: string,
    public resourceValue: string,
    public systemId: number,
    public resourceId: number,
    public id: number
  ) {}
}
