import { AmqpConnection, RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { MailDto } from './mail.dto';

@Injectable()
export class MailService {
    @RabbitSubscribe({
        exchange: 'topic_exchange',
        routingKey: ['all.#', 'finances.#', '#.finances.#']
    })
    async getMail(mail: MailDto){
        console.log('[finances] ', mail);
    }
}
