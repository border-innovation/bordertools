import { AfterInsert, AfterRemove, AfterUpdate, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: 'USERS'})
export class User {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    username: string;

    @Column()
    fullName: string; 

    @Column()
    email: string;

    @Column()
    password: string;

    @AfterInsert()
    logInsert(){
        console.log('Inserted user with id: ', this.id)
    }

    @AfterUpdate()
    logUpdate(){
        console.log('Updated user with id: ', this.id)
    }

    @AfterRemove()
    logRemove(){
        console.log('Removed user with id: ', this.id)
    }
}