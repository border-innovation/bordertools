import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { ArticleDto } from './article.dto';

@Injectable()
export class ArticleService {
    @RabbitSubscribe({
        exchange: 'article-exchange',
    })
    async receiveNotifications(article: ArticleDto){
        const category = article.category; // Access message headers
        if (category === 'technology') {
            console.log('Received technology article:', article);
        }
    }
}

