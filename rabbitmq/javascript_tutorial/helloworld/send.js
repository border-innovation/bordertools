var amqp = require('amqplib/callback_api');

//Conection to the RabbitMQ server
amqp.connect('amqp://localhost', (error0, connection) => {

    if (error0) throw error0;

    //Create a channel, which most of the API for getting things done resides
    connection.createChannel((error1, channel) => {
        if (error1) throw error1;

        //to send, declare a queue to send, and then publish a message to the queue
        var queue = 'hello';
        var msg = 'Hello World PA';

        channel.assertQueue(queue, {
            durable: false
        });

        channel.sendToQueue(queue, Buffer.from(msg));
        console.log(`[x] Sent '${msg}' `);
    });

    //Close the connection
    setTimeout(() => {
        connection.close();
        process.exit(0);
    }, 500);
});

