import { Injectable } from '@nestjs/common';

import { UserDto } from './user.dto';
import {  RabbitRPC, RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class UserService {
    @RabbitSubscribe({
        exchange: 'direct_exchange',
        routingKey: 'new-user'
    })
    async newUserHandler(user: UserDto){
       console.log(`Received User: ${JSON.stringify(user)}`)
    }

    @RabbitRPC({
        exchange: 'rpc_exchange',
        routingKey: 'rpc-route',
    })
    async rpcHandler(msg: {}){
        console.log('[Consumer] New User: ', JSON.stringify(msg))
        return {
            message: 'User Added [Sendend by the consumer]'
        };
    }

}   