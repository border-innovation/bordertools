import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { MatTabsModule } from '@angular/material/tabs';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RouterComponent } from './router/router.component';
import { AdminComponent } from './admin/admin.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select'; 
import { AuthComponent } from './auth/auth.component';
import { HTTP_INTERCEPTORS, HttpClientModule, provideHttpClient, withFetch } from '@angular/common/http';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { AdminAppsComponent } from './admin/admin-apps/admin-apps.component';
import { AdminApisComponent } from './admin/admin-apis/admin-apis.component';
import { AdminPermissionsComponent } from './admin/admin-permissions/admin-permissions.component';
import { MatTableModule } from '@angular/material/table';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { CookieService } from 'ngx-cookie-service';
import { AlertComponent } from './shared/alert/alert.component';
import { PlaceHolderDirective } from './shared/placeholder/placeholder.directive';
import { ActionReducerMap, StoreModule, combineReducers } from '@ngrx/store';
import { appsReducer } from './store/apps/reducer';
import { EffectsModule } from '@ngrx/effects';
import { combinedReducers, storeEffects } from './app.store';
import { MatSortModule } from '@angular/material/sort';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RouterComponent,
    AuthComponent,
    AdminComponent,
    LoadingSpinnerComponent,
    AdminAppsComponent,
    AdminApisComponent,
    AdminPermissionsComponent,
    AlertComponent,
    PlaceHolderDirective
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatAutocompleteModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return document.cookie.match(/^(.*;)?\s*token\s*=\s*[^;]+(.*)?$/)?.[2];
        }
      }
    }),
    StoreModule.forRoot(combinedReducers),
    EffectsModule.forRoot(storeEffects),
  ],
  providers: [
    CookieService,
    provideHttpClient(withFetch()),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    // JwtHelperService
  ],
  bootstrap: [AppComponent],
  //entryComponents: []
})


export class AppModule { }
