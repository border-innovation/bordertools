import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { PermissionDto } from './dtos/user-permissions.dto';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { PermissionsService } from './permissions.service';
import { CreatePermissionDto } from './dtos/create-permissions.dto';

@Controller('permissions')
@Serialize(PermissionDto)
export class PermissionsController {
    constructor(
        private permissionService : PermissionsService 
    ){}

    @Get()
    async getAllPermissions(){
        return this.permissionService.findAll();
    }

    @Get('/:id')
    async getPermission(@Param('id') id: string){
        return await this.permissionService.find(parseInt(id));
    }

    @Post()
    createPermission(@Body() body: CreatePermissionDto){
        console.log("Creating permission: ", body)
        return this.permissionService.create(body.username, body.role);
    }

    @Delete('/:id')
    deleteEndpoint(@Param('id') id: string){
        return this.permissionService.remove(parseInt(id));
    }
}
