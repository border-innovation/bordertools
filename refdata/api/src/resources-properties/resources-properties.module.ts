import { Module } from '@nestjs/common';
import { ResourcesPropertiesController } from './resources-properties.controller';
import { ResourcesPropertiesService } from './resources-properties.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResourcePropertiesEntity } from './resource-properties.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ResourcePropertiesEntity])],
  controllers: [ResourcesPropertiesController],
  providers: [ResourcesPropertiesService]
})
export class ResourcesPropertiesModule {}
