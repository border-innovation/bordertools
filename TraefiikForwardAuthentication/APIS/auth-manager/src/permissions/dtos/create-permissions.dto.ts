import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class CreatePermissionDto{

    @ApiProperty()
    @IsString()
    username: string;

    @ApiProperty()
    @IsString()
    role: string;
}