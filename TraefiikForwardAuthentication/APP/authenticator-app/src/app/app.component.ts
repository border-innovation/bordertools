import { Component, OnInit, isDevMode } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { Store } from '@ngrx/store';
import { AppsActionTypes } from './store/apps/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private cookieService: CookieService,
  ){

  }
//  ngOnInit() {
//    this.authService.autoLogin();
//  }

ngOnInit() {
  const testCookie = this.cookieService.getAll();

  console.log("Test Cookie Access:", testCookie);


  
  const testCookie2 = this.authService.getTokenFromCookie();

  console.log("Test Cookie Access2:", testCookie2);

  if (isDevMode()) {
    console.log('Development!');
  } else {
    console.log('Not development!');
  }

}

}
