import mqConnection from "./connection";

const handlerIncomingNotification = (msg: string) =>{
    try{
        const parsedMessage = JSON.parse(msg);
        console.log('Received Notification', parsedMessage);
    }catch(error){
        throw new Error(error + " Error while Parsing the message");
    }
};

const listen = async () => {
    await mqConnection.connect()

    await mqConnection.consume('ts_queue_1', handlerIncomingNotification);
};

listen();