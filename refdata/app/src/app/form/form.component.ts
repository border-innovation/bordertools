import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { DialogComponent } from '../dialog/dialog.component';
import { ApiCallService } from '../services/api-call.service';
import { ReferenceData } from '../shared/reference-data.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  private _DEFAULT_SNACKBAR_DURATION = 5000;
  private _DISMISS_ACTION = 'Dismiss';
  colorSpinner: string = "#d43f3a";
  displayedColumns: string[] = ['edit', 'select', 'system', 'resource', 'value'];

  loadingResults: boolean = false;

  systems: string[] = [];
  filteredSystems!: Observable<string[]>;
  systemsControl: FormControl = new FormControl();
  resource: string = '';
  value: string = '';

  dataSource = new MatTableDataSource<ReferenceData>();

  paginator!: MatPaginator | null;

  @ViewChild("paginator")
  set matPaginator(mp: MatPaginator) {
    if (mp) {
      this.paginator = mp;
      this.dataSource.paginator = this.paginator;
    } else {
      this.paginator = null;
      this.dataSource.paginator = null;
    }
  }

  selection = new SelectionModel<ReferenceData>(true, []);
  editMode: boolean = false;

  private _systemsSubscription!: Subscription;

  private _startingEditElement: ReferenceData | null = null;

  constructor(
    private _apiCallService: ApiCallService,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog
  ) { }

  ngOnInit() {
    this._getSystems();
  }

  private _getSystems() {
    if (this._systemsSubscription) {
      this._systemsSubscription.unsubscribe();
    }
    this._systemsSubscription = this._apiCallService
      .getSystems()
      .subscribe((data: string[]) => {
        if (data) {
          this.systems = data;
          this.filteredSystems = this.systemsControl.valueChanges.pipe(
            startWith<string>(''),
            map(name => (name ? this._filter(name) : this.systems.slice()))
          );
        }
      });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.systems.filter(option =>
      option.toLowerCase().includes(filterValue)
    );
  }

  private _getSystem() {
    const system = this.systemsControl.value;
    if (system === null || system === undefined) {
      return '';
    }
    return system;
  }

  search(): void {
    this._turnSpinnerOn();
    this.dataSource.data = [];
    this.selection.clear();
    this._apiCallService
      .searchReferenceData(this._getFormReferenceData())
      .subscribe(data => {
        this.dataSource.data = data;
        this._turnSpinnerOff();
        if (!this.dataSource.data) {
          this._openSnackBar(
            'No results found!',
            this._DISMISS_ACTION,
            this._DEFAULT_SNACKBAR_DURATION
          );
        }
      });
  }

  private _getFormReferenceData(): ReferenceData {
    return new ReferenceData(this._getSystem(), this.resource, this.value);
  }

  resetSystemInput(): void {
    this.systemsControl.setValue('');
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  private _openSnackBar(message: string, action: string, duration: number) {
    this._snackBar.open(message, action, { duration: duration });
  }

  private _getMissingFields(): string {
    let missingFields = '';
    missingFields += !this._getSystem().trim() ? 'System, ' : '';
    missingFields += !this.resource.trim() ? 'Resource, ' : '';
    missingFields += !this.value.trim() ? 'Value, ' : '';
    if (missingFields.length > 0) {
      missingFields = missingFields.substring(0, missingFields.length - 2);
    }
    return missingFields;
  }

  add() {
    this._turnSpinnerOn();
    let missingFields = this._getMissingFields();
    if (missingFields.length > 0) {
      this._openSnackBar(
        '⚠️ Missing Fields!️ ' + '(' + missingFields + ')',
        this._DISMISS_ACTION,
        this._DEFAULT_SNACKBAR_DURATION
      );
      this._turnSpinnerOff();
    } else {
      let rf = this._getFormReferenceData();
      rf.value = '';
      this._apiCallService.findReferenceData(rf).subscribe(data => {
        if (data && data.length > 0) {
          this._openConfirmationDialog(data[0]);
        } else {
          this._addReferenceData();
        }
      });
    }
  }

  private _openConfirmationDialog(data: any): void {
    const dialogRef = this._dialog.open(DialogComponent, {
      width: '500px',
      data: {
        oldData: new ReferenceData(data.system, data.resource, data.value),
        newValue: this.value
      }
    });
    dialogRef.afterClosed().subscribe(action => {
      if (action === 'replace') {
        this._addReferenceData();
      } else {
        this._turnSpinnerOff();
        if (this.editMode) {
          this._turnEditModeOff();
          this._replaceCurrentFields(this.oldData);
        }
      }
    });
  }

  private _addReferenceData() {
    this._apiCallService
      .mergeReferenceData(this._getFormReferenceData())
      .subscribe(data => {
        this._showSnackBar(data);
        this._turnSpinnerOff();
        this._getSystems();
        if (this.editMode) {
          this._turnEditModeOff();
          if (
            this._equalsRF(this._startingEditElement!, {
              system: this._getSystem(),
              resource: this.resource,
              value: ''
            })
          ) {
            this._replaceValueInTable();
          }
          this._replaceCurrentFields(this.oldData);
        }
      });
  }

  private _showSnackBar(data: any) {
    if (data && data.error) {
      this._openSnackBar(
        '❌ An error occurred!️ ' + data.errorMessage,
        this._DISMISS_ACTION,
        this._DEFAULT_SNACKBAR_DURATION
      );
    } else {
      this._openSnackBar(
        '✔️ Success!️',
        this._DISMISS_ACTION,
        this._DEFAULT_SNACKBAR_DURATION
      );
    }
  }

  openDialog(): void {
    const dialogRef = this._dialog.open(DialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => { });
  }

  _turnSpinnerOn() {
    this.loadingResults = true;
  }

  _turnSpinnerOff() {
    this.loadingResults = false;
  }

  edit(element: ReferenceData) {
    this._startingEditElement = element;
    if (!this.editMode) {
      this._saveCurrentFields();
    }
    this._replaceCurrentFields(element);
    this._turnEditModeOn();
  }

  cancelEdit() {
    this._turnEditModeOff();
    this._replaceCurrentFields(this.oldData);
  }

  saveEdit() {
    this._turnSpinnerOn();
    let missingFields = this._getMissingFields();
    if (missingFields.length > 0) {
      this._openSnackBar(
        '⚠️ Missing Fields!️ ' + '(' + missingFields + ')',
        this._DISMISS_ACTION,
        this._DEFAULT_SNACKBAR_DURATION
      );
      this._turnSpinnerOff();
    } else {
      let rf = this._getFormReferenceData();
      rf.value = '';
      this._apiCallService.findReferenceData(rf).subscribe(data => {
        if (data && data.length > 0) {
          if (this._equalsRF(data[0], this._startingEditElement!)) {
            this._addReferenceData();
          } else {
            this._showSnackBar({
              error: true,
              errorMessage: 'Another RF exists with this System + Resource'
            });
            this._turnSpinnerOff();
          }
        } else {
          this._addReferenceData();
        }
      });
    }
  }

  private _replaceValueInTable() {
    this._startingEditElement!.system = this._getSystem();
    this._startingEditElement!.resource = this.resource;
    this._startingEditElement!.value = this.value;
  }

  private _equalsRF(refd1: ReferenceData, refd2: ReferenceData) {
    return refd1.system == refd2.system && refd1.resource == refd2.resource;
  }

  private _turnEditModeOn() {
    this.editMode = true;
  }

  private _turnEditModeOff() {
    this.editMode = false;
  }

  private _saveCurrentFields() {
    this.oldData.system = this._getSystem();
    this.oldData.resource = this.resource;
    this.oldData.value = this.value;
  }

  private _replaceCurrentFields(data: ReferenceData) {
    this.systemsControl.setValue(data.system);
    this.resource = data.resource;
    this.value = data.value;
  }

  oldData = new ReferenceData('', '', '');

  delete() {
    this._turnSpinnerOn();
    this._apiCallService
      .deleteReferenceData(this.selection.selected)
      .subscribe(data => {
        this._turnSpinnerOff();
        this._showSnackBar(data);
      });
  }
}