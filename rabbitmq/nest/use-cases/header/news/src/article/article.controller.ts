import { Body, Controller, Post } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleDto } from './article.dto';

@Controller('article')
export class ArticleController {

    constructor(private readonly articleService: ArticleService){}

    @Post('post')
    postArticle(@Body() article: ArticleDto){
        this.articleService.postArticle(article);

    }

}
