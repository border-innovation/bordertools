/* Modules */
const express = require('express');
const expressJWT = require('express-jwt');
const router = express.Router();
const fs = require('fs');
const cookieParser = require('cookie-parser');

/* env values */
const config = require('config');
const api = config.get('api');
const integrationServer = config.get('integrationServer');

/* */
const controller = require('@oss-ready/nodejs-baseline').controller;

///* GENERAL PURPOSE functions here *///
router.use(controller.body_parser);
router.use(controller.routes_log);
/* router.use(
    expressJWT({
        secret: new Buffer.from(fs.readFileSync(api.token_secret, 'utf8'), 'base64'),
        algorithms: ['HS384'],
        audience: api.token_audience
    })
); */
router.use(cookieParser());
router.use(
  controller.checkTokenMiddleware(api.token_audience, api.token_secret)
);
router.use(controller.error_log);

///* API routes here *///
router.get('/', (req, res) => {
    let system = req.query.system;
    let resource = req.query.resource;
    let value = req.query.value;
    let path =
        encodeURI(integrationServer.reference_data +
            '/?systemName=' + system +
            '&resourceName=' + resource +
            '&resourceValue=' + value);
    controller.pilot('GET', path, req, res);
});

router.get('/find', (req, res) => {
    let system = req.query.system;
    let resource = req.query.resource;
    let path =
        encodeURI(integrationServer.find +
            '?system=' + system +
            '&resource=' + resource);
    controller.pilot('GET', path, req, res);
});

router.post('/', (req, res) => {
    let path =
        encodeURI(integrationServer.reference_data);
    controller.pilot('POST', path, req, res, req.body);
});

router.put('/', (req, res) => {
    let path =
        encodeURI(integrationServer.reference_data);
    controller.pilot('PUT', path, req, res, req.body);
});

module.exports = router;