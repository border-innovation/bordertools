const mqtt = require('mqtt')
const topic = '/nodejs/mqtt'


const protocol = 'ws'
const host = 'broker.emqx.io'
const port = '15675'
const path = '/mqtt'
const clientId = `mqtt_${Math.random().toString(16).slice(3)}`

const connectUrl = `${protocol}://${host}:${port}${path}`


const client = mqtt.connect(connectUrl, {
    clientId,
    clean: true,
    username: 'guest',
    password: 'guest',
    reconnectPeriod: 1000
})


client.on('connect', () => {
    console.log('Connected')
    client.subscribe([topic], () => {
        console.log(`Subscribe to topic '${topic}'`)
    })
})

