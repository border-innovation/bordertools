import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
/*   imports: [ClientsModule.register([
    {
      name: 'CATS_SERVICE',
      transport: Transport.RMQ,
      options: {
        urls: ['amqp://admin:admin@127.0.0.1:5672'],
        queue: 'cats_queue',
        queueOptions: {
          durable: true
        },
      },
    },
  ]),], */
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
