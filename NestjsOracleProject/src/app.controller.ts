import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { HealthCheck, HealthCheckResult, HealthCheckService, TypeOrmHealthIndicator } from '@nestjs/terminus';
import { InjectRepository } from '@nestjs/typeorm';
import { ClientEntity } from './client/client.entity';
import { Repository } from 'typeorm';
import * as oracledb from 'oracledb';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly healthCheckService: HealthCheckService,
    private readonly typeOrmHealthIndicator: TypeOrmHealthIndicator,
    @InjectRepository(ClientEntity) private clientRepository: Repository<ClientEntity>,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @HealthCheck()
  @Get('/healthcheck')
  async healthCheck(): Promise<HealthCheckResult> {
    const pool = await oracledb.getPool();
    console.log(pool);
    const connections = await pool.getConnection();
    console.log(connections)
    return this.healthCheckService.check([]);
  }

  @HealthCheck()
  @Get('/healthcheck/database')
  databaseHealthCheck(): Promise<HealthCheckResult> {
    return this.healthCheckService.check([
      () => this.typeOrmHealthIndicator.pingCheck('database'),
    ]);
  }
}

