import {connect} from "rabbitmq-stream-js-client";

async function main() {
    const client = await connect({
        hostname: "localhost",
        port: 5552,
        username: "username",
        password: "password",
        vhost: "/"
    })

    const publisher = await client.declarePublisher({
        stream: "stream-name",
        publisherRef: "my-publisher",
    })

    await publisher.send(Buffer.from("my message content"))

    const messages = [
        { content: Buffer.from("my message content 1") },
        { content: Buffer.from("my message content 2") },
        { content: Buffer.from("my message content 3") },
        { content: Buffer.from("my message content 4") },
    ]

    await publisher.sendSubEntries(messages)
    
    await client.close()
}

main().then(() => console.log("done!")).catch((res) => console.error("Error, ", res))

