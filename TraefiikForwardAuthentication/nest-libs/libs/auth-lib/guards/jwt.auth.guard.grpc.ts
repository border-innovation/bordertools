import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from "@nestjs/common";
import { loadPackageDefinition } from "@grpc/grpc-js";
import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import * as path from 'path';
import { fileURLToPath } from 'url';



@Injectable()
export class JwtAuthGuardGRPC implements CanActivate {
  private authClient: any;

  constructor() {
    // Load the proto file

    let PROTO_PATH;
    //console.log('Path: ', path);
    if(path){
       PROTO_PATH = path.join(__dirname, '../auth.proto'); // Adjust path to the .proto location
    } else {
      PROTO_PATH = '../auth.proto';
    }
    
    console.log('PROTO_PATH: ', PROTO_PATH);


    const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
      keepCase: true,
      longs: String,
      enums: String,
      defaults: true,
      oneofs: true,
    });
    
    const grpcObject = grpc.loadPackageDefinition(packageDefinition);
    
    // Access the AuthService client definition
    const AuthService = (grpcObject as any).auth?.AuthService as grpc.ServiceClientConstructor;
    

    this.authClient = new AuthService('localhost:5001', grpc.credentials.createInsecure());
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    console.log("JWT AUDIT GUARD");

    const request = context.switchToHttp().getRequest();
    const token = this.getToken(request);

    if (!token) {
      throw new UnauthorizedException('Token not found');
    }
    
    const forwardUri = request.url;
    const forwardMethod = request.method;

    //console.log("request: ",  request )
    console.log("forwardUri: ",  forwardUri )
    console.log("forwardMethod: ",  forwardMethod )

    return new Promise((resolve, reject) => {
      console.log('Sending GRPC request');
      const payload = {
        forwardUri: forwardUri,
        forwardMethod: forwardMethod,
        token: token,
      };

      this.authClient.Validate(payload, (error, response) => {
        if (error) {
          console.error('GRPC Error:', error.data?.message);
          reject(new UnauthorizedException('Token validation failed'));
        } else {
          console.log('GRPC Response:', response);
          resolve(true);
        }
      });
    });
  }

  getToken(request): string {
    // Check cookies
    if (request.cookies && request.cookies.token) {
        return request.cookies.token;
    } 

    // Check authorization header
    if (request.headers.authorization?.split(" ")[0] === "Bearer") {
        return request.headers.authorization.split(" ")[1];
    } 

    // Check rawHeaders for Authorization or Cookie with token
    for (let i = 0; i < request.rawHeaders.length; i++) {
        // Check for Authorization header
        if (request.rawHeaders[i].toLowerCase() === 'authorization' && request.rawHeaders[i + 1]?.startsWith("Bearer ")) {
            return request.rawHeaders[i + 1].split(" ")[1];
        }
        
        // Check for token in Cookie header
        if (request.rawHeaders[i].toLowerCase() === 'cookie') {
            const cookieHeader = request.rawHeaders[i + 1];
            const tokenMatch = cookieHeader.match(/token=([^;]+)/);
            if (tokenMatch) {
                return tokenMatch[1];
            }
        }
    }

    return undefined;
}


}
