import { Inject, Injectable, InternalServerErrorException } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { firstValueFrom, Observable } from 'rxjs';


@Injectable()
export class CatsService {

    constructor(@Inject('CATS_SERVICE') private readonly client: ClientProxy) { this.client.connect() }

    async sendCatToQueue(cat: any) {
        try {
            const catSent = await firstValueFrom(
                this.client.send('msgCat', cat),
            );
            return catSent;
        }
        catch (e) {
            throw new InternalServerErrorException(e);
        }
        /*         console.log('Sendind cat: ', cat);  
                const pattern = {cmd: 'msgCat'};
                return this.client.send<any>('msgCat', cat);  */
    }
    /*    async sendCatToQueue(cat: any): Promise<any> {
           this.client.emit('cats', cat);
           return {
               message: 'Customer create successfully with his wallet',
               cat,
           };
       } */
}
