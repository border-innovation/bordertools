import { Body, Controller, Get, Param, Patch, Post, Delete, Query, Req, Res,Headers , Session, UseGuards } from '@nestjs/common';
import { AppsService } from './apps.service';
import { CreateAppDto } from './dtos/create-apps.dto';
import { UpdateAppsDto } from './dtos/update-apps.dto';
import { Request } from 'express';

@Controller('apps')
export class AppsController {

    constructor(
        private appsService: AppsService
    ){}
    

    @Get()
    listAll(){
        const results = this.appsService.findAll();
        return results;
    }

    @Get('/token')
    listByToken(@Req() request: Request){
        return this.appsService.listApps(request);
    }
    
    @Get('/:appId')
    getApp(@Param('appId') id: string){
        return this.appsService.find(parseInt(id));
    }
     
    @Post()
    async signUp(@Body() body: CreateAppDto){
        return this.appsService.create(body.name, body.endpoint, body.role);
    }
   
    @Patch('/:appId')
    update(@Param('appId') id: string, @Body() body: UpdateAppsDto){
        return this.appsService.update(parseInt(id) ,body)
    } 

    @Delete('/:appId')
    delete(@Param('appId') id: string){
        return this.appsService.remove(parseInt(id))
    }
}
