import { Controller, Get, Query } from '@nestjs/common';
import { ResourcesPropertiesService } from './resources-properties.service';

interface resourcesProperties {
    systemName: string,
    resourceName: string,
    resourceValue: string,
}

interface ResourceSystem {
    system: string,
    resource: string
}

@Controller('/api/reference-data-api')
export class ResourcesPropertiesController {
    constructor (private resourceService: ResourcesPropertiesService) {}

    @Get()
    public async getAllResourcesProperties (@Query() query: resourcesProperties) {
        const { systemName, resourceName, resourceValue } = query;
        return await this.resourceService.getAllResourceProperties(systemName, resourceName, resourceValue);
    }

    @Get('/find')
    public async findResourceSystem (@Query() query: ResourceSystem) {
        const { system, resource } = query;
        return await this.resourceService.findResourceSystem(system, resource);
    }
}
