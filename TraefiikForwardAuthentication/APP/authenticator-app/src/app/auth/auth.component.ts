import { Component, ComponentFactoryResolver, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AuthResponseData, AuthService } from './auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { registerLocaleData } from '@angular/common';
import { Router } from '@angular/router';
import { AlertComponent } from '../shared/alert/alert.component';
import { PlaceHolderDirective } from '../shared/placeholder/placeholder.directive';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrl: './auth.component.scss'
})
export class AuthComponent implements OnDestroy {
  isLoginMode = true;
  isLoading = false;
  error: string = null;
  @ViewChild(PlaceHolderDirective) alertHost: PlaceHolderDirective;
  private closeSub: Subscription;

  logInForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    this.logInForm = this.formBuilder.group({
      username: new FormControl(""),
      password: new FormControl("")
    });
  }

  onSubmit() {
    

    let authObs: Observable <AuthResponseData>
    console.log("Signing up");
    if (this.logInForm.valid) {
      this.isLoading = true;
      if(this.isLoginMode){
        const { username, password } = this.logInForm.value;
        authObs = this.authService.login(username, password);
      } else {
        const { username, password, email, fullName} = this.logInForm.value;
        authObs = this.authService.signUp(username,email,password, fullName);        
      }
      
    } else {
      console.log('Form is invalid');
    }

    authObs.subscribe(
      resData => {
        console.log(resData);
        this.isLoading = false;
        //this.error = null;
        this.router.navigate(['/router'])
        
      },
      error => {
        console.log(error)
        this.isLoading = false;
        //this.error = error.message
        this.showErrorAlert(error.message ? error.message : error)
      }
    )
  }
  
  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
    if (!this.isLoginMode) {
      // Switch to sign-up mode, add email control
      this.logInForm.addControl('email', new FormControl(''));
      this.logInForm.addControl('fullName', new FormControl(''));

    } else {
      // Switch to login mode, remove email control
      this.logInForm.removeControl('email');
      this.logInForm.removeControl('fullName');

    }
  }

  onHandleError(){
    //this.error = null;
  }

  private showErrorAlert(error: string){
    console.log("Showing alert error", error)
    const alertCmpFactory = this.componentFactoryResolver.resolveComponentFactory(AlertComponent);
    const host = this.alertHost.viewContainerRef;
    host.clear();

    const hostRef = host.createComponent(alertCmpFactory);

    hostRef.instance.message = error;
    this.closeSub =  hostRef.instance.close.subscribe(()=> {
      this.closeSub.unsubscribe(); 
      host.clear();
    })
  }

  ngOnDestroy() {
    if(this.closeSub){
      this.closeSub.unsubscribe();
    }
  }

}
