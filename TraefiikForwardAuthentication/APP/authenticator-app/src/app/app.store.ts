import { ActionReducerMap } from "@ngrx/store";
import { appsReducer, AppsState } from "./store/apps/reducer";
import { AppEffects } from "./store/apps/effects";
import { apisReducer, APIsState } from "./store/apis/reducer";
import { APIEffects } from "./store/apis/effects";
import { permissionsReducer, PermissionsState } from "./store/permissions/reducer";
import { PermissionsEffects } from "./store/permissions/effects";



export interface GlobalState {
  apps: AppsState;
  allApps: AppsState;
  apis: APIsState;
  permission: PermissionsState;
}

// export type AppActions = VariableActions | RuleActions;

export const combinedReducers: ActionReducerMap<GlobalState, any> = {
  apps: appsReducer,
  allApps: appsReducer,
  apis: apisReducer,
  permission: permissionsReducer,
};

export const storeEffects = [
  AppEffects,
  APIEffects,
  PermissionsEffects
];
