import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_PIPE } from '@nestjs/core';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport'; 
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppsModule } from './apps/apps.module';
import { Apps } from './apps/apps.entitty';
import { readFileSync } from 'fs';


const configPath = join(__dirname, '..', 'config', 'default.json');
const configJson = JSON.parse(readFileSync(configPath, 'utf-8'));

@Module({
  imports: [
    PassportModule.register({defaultStrategy: 'jwt'}),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [() => configJson], // Load the JSON config as a source
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        type: 'postgres',
        host: config.get<string>('database.host', 'localhost'),
        port: config.get<number>('database.port', 5432),
        username: config.get<string>('database.user'),
        password: config.get<string>('database.password'),
        database: config.get<string>('database.name'),
        entities: [Apps], 
        synchronize: true, // WARNING: Not recommended for production
      }),
    }),
    AppsModule,
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        const tokenSecret = config.get<string>('database.secret');
        const expiresIn = config.get<string>('api.expiresIn');; // Adjust as needed from config or hardcoded
        
        return {
          secret: tokenSecret,
          signOptions: { expiresIn },
        };
      },
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_PIPE,
      useValue:  new ValidationPipe({
        whitelist: true //Doesn't allow to add additional properties to request body
      })
    }],
})
export class AppModule {}
