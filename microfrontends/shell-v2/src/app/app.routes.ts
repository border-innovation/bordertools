import { loadRemoteModule } from '@angular-architects/native-federation';
import { Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';


export const routes: Routes = [

  {
    path: "",
    redirectTo: "auth",
    pathMatch: 'full'
  },
  {
    path: "profile",
    loadComponent: () => loadRemoteModule('profile', './Component')
      .then((m) => m.AppComponent)
      .catch(err => {
        console.log("[Eu] Auth cant be loaded: ", err);
        return ErrorComponent;
      })
  },
  {
    path: "auth",
    loadComponent: () => loadRemoteModule('auth', './Component')
      .then((m) => m.AppComponent)
      .catch(err => {
        console.log("[Eu] Auth cant be loaded: ", err);
        return ErrorComponent;
      })
  },
  {
    path: "review",
    loadComponent: () => loadRemoteModule('reviews', './Component')
    .then((m) => m.AppComponent)
    .catch(err => {
      console.log("[Eu] Auth cant be loaded: ", err);
      return ErrorComponent;
    })
  },
  {
    path: "reviews",
    loadChildren: () =>
      loadRemoteModule('reviews', './routes').then((m) => m.routes)
  },
  { path: 'error', component: ErrorComponent }
];
