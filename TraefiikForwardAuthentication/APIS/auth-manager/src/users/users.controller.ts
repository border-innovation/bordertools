import { Body, ClassSerializerInterceptor, Controller, Delete, Get, Param, Patch, UseInterceptors } from '@nestjs/common';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { UserDto, UserMinimalDto } from './dtos/user.dto';
import { UsersService } from './users.service';
import { UpdateUserInfoDto } from './dtos/update-user.dto';

@Controller('users')
@Serialize(UserDto)
export class UsersController {
    constructor(
        private usersService: UsersService
    ){}

    @Get()
    @UseInterceptors(ClassSerializerInterceptor)
    @Serialize(UserMinimalDto)
    listAll(){
        return this.usersService.findAll();
    }
    @Get('/:username')
    getUser(@Param('username') username: string){
        return this.usersService.findBy(undefined,username);
    } 
    @Patch('/:userId')
    update(@Param('userId') id: string, @Body() body: UpdateUserInfoDto){
        return this.usersService.update(parseInt(id) ,body)
    } 
    @Delete('/:userId')
    delete(@Param('userId') id: string){
        return this.usersService.remove(parseInt(id))
    } 
}
