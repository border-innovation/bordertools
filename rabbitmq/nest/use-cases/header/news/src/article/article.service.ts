import { Injectable } from '@nestjs/common';
import { ArticleDto } from './article.dto';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class ArticleService {

    constructor(private readonly amqpConnection: AmqpConnection){}

    postArticle(article: ArticleDto) {
        const headers = {category: article.category, sub_category: article.sub_category}
        this.amqpConnection.publish<ArticleDto>('article-exchange', '', article, {headers})

    }
}
