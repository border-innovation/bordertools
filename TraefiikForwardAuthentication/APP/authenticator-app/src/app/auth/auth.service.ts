// src/app/services/auth.service.ts
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable, OnDestroy, PLATFORM_ID } from '@angular/core';
import { BehaviorSubject, Observable, Subject, catchError, map, of, tap, throwError } from 'rxjs';
import { User } from './user.model';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

import { JwtHelperService } from '@auth0/angular-jwt';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../environments/environment';


export interface AuthResponseData {
  username: string;
  fullName: string;
  apps: string[];
  roles: string[];
  environment: string;
  expiresIn?: string; //TODO: To add to response in milliseconds
}

export interface UsersResponseData {
  id: string;
  username: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user = new BehaviorSubject<User>(null);

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private http: HttpClient,
    private cookieService: CookieService,
    private router: Router,
    private jwtService: JwtHelperService
){

  }

  login(username: string, password: string): Observable<AuthResponseData> {
      return this.http.post<AuthResponseData>(
         environment.API_BASE_URL + environment.AUTH_API_SIGN_IN,
        { username, password },
        { withCredentials: true } // Include credentials (cookies) in the request
      ).pipe(
        tap(resData => {
          this.handleAuthentication(
            resData.username, 
            resData.roles, 
            resData.expiresIn
          )
          
        }),
        catchError(this.handleError)
      );
  } 

  logOut(){
    this.user.next(null);
    this.cookieService.delete('token','/')
  }

  private handleError(errorRes: HttpErrorResponse){
    console.log("Handling error: ",errorRes);
    let errorMessage = 'An unknown error occured!'
    if(!errorRes.error || !errorRes.error.error) {
      return throwError(errorMessage);
    }
    errorMessage = errorRes.error ? errorRes.error : errorRes.error.error
    return throwError(errorMessage);

  }

  private handleAuthentication(username, roles, expIn){
    const token = this.cookieService.get('token'); 
    const tk = this.cookieService.getAll()
    console.log('Token', token)
    console.log('Token tk', tk)

    const expirationDate = new Date(new Date().getTime() + +expIn * 1000);
    const user= new User(
      username, 
      roles, 
      token, 
      expirationDate
    );
    this.user.next(user);
  }

  autoLogin(){
    
    const token = this.cookieService.get('token'); 
    console.log("AutoLogin", token);

    if (token) {
      try {
        const decodedToken = this.jwtService.decodeToken(token);
        console.log("Decoded Token:", decodedToken);

        const currentTime = Date.now() / 1000;
        if (decodedToken.exp && decodedToken.exp < currentTime) {
          console.log("Token has expired");
          this.router.navigate(['/login'])
        } else {
          // Proceed with auto login using the decoded token
          // e.g., set user data in state, make authenticated requests, etc.
          //TODO
          //    this.user.next(user);
        }
      } catch (error) {
        console.error("Invalid token:", error);
      }
    } else {
      console.log("No token found");
      // Handle case where token is not available
    }
  }

  getTokenFromCookie(): string | null {
    if (isPlatformBrowser(this.platformId)) {
   
      const cookies = document.cookie;
      const output = document

      console.log("Cookies --", cookies)
      console.log("output --", output)

      for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i].trim();
        if (cookie.startsWith('token=')) {
          return cookie.substring('token='.length);
        }
      }
    }
    return null;
  }

  signUp(username: string, email: string, password: string, fullName: string): Observable<AuthResponseData> {
    return this.http.post<AuthResponseData>(
       environment.API_BASE_URL + environment.AUTH_API_SIGN_UP,
      { username, password, email, fullName },
      { withCredentials: true } // Include credentials (cookies) in the request
    ).pipe(
      tap(resData => {
        this.handleAuthentication(
          resData.username, 
          resData.roles, 
          resData.expiresIn
        )
        
      }),
      catchError(this.handleError)
    );
  } 

  isAdminUser(){
    const adminRole = environment.ADMIN_ROLE; 
    const currentUser = this.user.value; 
    return currentUser.getRoles().includes(adminRole)
  }

  getUsers(){
    return this.http.get<UsersResponseData>(
      environment.API_BASE_URL + environment.AUTH_API_USERS
    ).pipe(
      map(responseData => responseData) // Assuming 'users' is the array of users in the response
    );
  }

}


