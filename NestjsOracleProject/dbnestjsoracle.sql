--------------------------------------------------------
--  File created - sexta-feira-maio-17-2024   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CLIENT
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."CLIENT" 
   (	"ID" NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"FIRST_NAME" VARCHAR2(50 BYTE), 
	"SECOND_NAME" VARCHAR2(50 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index CLIENT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."CLIENT_PK" ON "SYSTEM"."CLIENT" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Package TEST
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE PACKAGE "SYSTEM"."TEST" as 

    TYPE responses IS REF CURSOR;
    
    PROCEDURE getAll(o_response OUT responses);
    
    PROCEDURE getClientById(
        i_id IN NUMBER,
        o_response OUT responses
    );
    
    PROCEDURE createClient(
        i_first_name IN VARCHAR,
        i_second_name IN VARCHAR,
        o_id OUT NUMBER
    );
    
    PROCEDURE editClient (
        i_id IN NUMBER,
        i_first_name IN VARCHAR,
        i_second_name IN VARCHAR,
        o_id OUT NUMBER,
        o_first_name OUT VARCHAR,
        o_second_name OUT VARCHAR
    );
    
    PROCEDURE deleteClient (
        i_id IN NUMBER, 
        o_id OUT NUMBER
    );
    
end test;

/
--------------------------------------------------------
--  DDL for Package Body TEST
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE PACKAGE BODY "SYSTEM"."TEST" as

    PROCEDURE getAll(o_response OUT responses) 
    IS
        start_time TIMESTAMP := systimestamp;
    BEGIN    
        open o_response for
        select id, first_name, second_name
        from client
        order by id;
    END getAll;
    
    PROCEDURE getClientById(
        i_id IN NUMBER,
        o_response OUT responses
    )
    IS
    BEGIN
        open o_response for
        select first_name, second_name
        from client
        where id = i_id;
    END getClientById;
    
    PROCEDURE createClient(
        i_first_name IN VARCHAR,
        i_second_name IN VARCHAR,
        o_id OUT NUMBER
    )
    IS
    BEGIN
        insert into client(first_name, second_name)
        values(i_first_name, i_second_name)
        returning id into o_id;
    END;
    
    PROCEDURE editClient (
        i_id IN NUMBER,
        i_first_name IN VARCHAR,
        i_second_name IN VARCHAR,
        o_id OUT NUMBER,
        o_first_name OUT VARCHAR,
        o_second_name OUT VARCHAR
    )
    IS
    BEGIN
        update client
        set first_name = i_first_name, second_name = i_second_name
        where id = i_id
        returning id, first_name, second_name into o_id, o_first_name, o_second_name;
    END;
    
    PROCEDURE deleteClient (
        i_id IN NUMBER, 
        o_id OUT NUMBER
    )
    IS
    BEGIN
        delete from client
        where id = i_id
        returning id into o_id;
    END;

end test;

/
--------------------------------------------------------
--  Constraints for Table CLIENT
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."CLIENT" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."CLIENT" ADD CONSTRAINT "CLIENT_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
