import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository, getDataSourceName } from '@nestjs/typeorm';
import { scrypt as _scrypt, randomBytes } from "crypto";
import { Repository, getConnection, getConnectionManager } from 'typeorm';
import { Apps } from './apps.entitty';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';

//const scrypt = promisify(_scrypt);

@Injectable()
export class AppsService {

    constructor(
        @InjectRepository(Apps) private repo: Repository<Apps>,
        private jwtService: JwtService,
    ){}

    create(appName: string, endpoint: string, role: string) {
        const application: Apps = this.repo.create({appName,endpoint,role})
        return this.repo.save(application)
    }

    find(id: number){
        if(!id) throw new BadRequestException('Missing id.');
        return this.repo.findOneBy({id});
    }

    findBy(appName?: string, endpoint?: string, role?: string ){
        if(appName) 
            return this.repo.findBy({appName});
        if(endpoint) 
            return this.repo.findBy({endpoint});
        if(role){ 
            return this.repo.findBy({role});
        }
        throw new BadRequestException('No parameters provided.');
    }
    
    async remove(id: number){
        if(!id) throw new NotFoundException('Id not provided.');
        const apps = await this.find(id);
        if(!apps) throw new NotFoundException('App not found.');
        return this.repo.remove(apps);
        
    }

    async findAll(){
        const obj = await this.repo.findAndCount()
        return obj[0];
    }

    async update(id: number, attrs: Partial<Apps>){
        console.log('Updating: ',id,attrs)
        const app = await this.find(id);
        if(!app) 
            throw new BadRequestException('App Not found')
        Object.assign(app,attrs);
        return this.repo.save(app)
    }

    async listApps(request: Request){
        const token = this.extractToken(request);
        if (!token) {
            throw new UnauthorizedException();
        }
        try {
            const decodedToken = await this.getTokenDecoded(token);
            console.log("User roles: ", decodedToken.aud)
            const apps = this.getApps(decodedToken.aud);
            return apps;
        } catch(err) {
            throw new UnauthorizedException();
        }
    }

    getTokenDecoded(token: string){
         try {
            const decoded = this.jwtService.decode(
                token
            );
            return decoded;
        } catch(e){
            console.error(e)
        }  
    }

    extractToken(req: Request): string | undefined {
        const cookieHeader = req.headers.cookie as string;
        const cookies = cookieHeader ? cookieHeader.split('; ') : [];
        for (const cookie of cookies) {
            const [name, value] = cookie.split('=');
            if (name.trim() === 'token') {
              return value;
            }
        }
        const authorizationHeader = req.headers.authorization;

        if (authorizationHeader && authorizationHeader.startsWith('Bearer ')) {
            const token = authorizationHeader.split(' ')[1];
            return token;
        }
    
        console.log("No token found in cookies or authorization header.");
        return undefined;
    }

    async getApps(roles: string[]){
        let appNames = [];
        for(const role of roles){
            const app = await this.findBy(undefined,undefined,role);
            if(app && app.length){
                appNames.push(app[0])
            }
        }
        return appNames;
    }
}