package border.tools.rules;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.FlowGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;
import border.tools.utils.BorderUtils;

/**
 * Checks if pub files have ACL on them.
 * 
 * @author DuarteCunha
 *
 */

@Rule(key = "S00016", name = "Pub services must have ACL's connected to them, otherwise it must inherit Internal ACL", priority = Priority.CRITICAL, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.SECURITY_HOTSPOT)

public class ACLCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(ACLCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(FlowGrammar.FLOW);
	}

	@Override
	public void visitNode(AstNode astNode) {
		try {
			String service = FlowUtils.getQualifiedName(this.getContext().getFile());
			File file = new File(
					"C:\\SoftwareAG\\WM1011\\IntegrationServer\\instances\\default\\config\\aclmap_sm.cnf");
			if (file != null) {
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document doc = db.parse(file);
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("value");
				if (service.contains("_pub")) {
					String aclName = BorderUtils.getPubServiceACLName(service);
					for (int i = 0; i < nodeList.getLength(); i++) {
						Node node = nodeList.item(i);
						NamedNodeMap attr = node.getAttributes();
						String name = attr.getNamedItem("name").getNodeValue();
						if (name.equalsIgnoreCase(service) && node.getTextContent().equals(aclName)) {
							return;
						}
					}
					getContext().createLineViolation(this, "Add ACL with the name " + aclName, astNode);
				} else {
					String serviceRoot = service.substring(0, service.indexOf("."));
					for (int i = 0; i < nodeList.getLength(); i++) {
						Node node = nodeList.item(i);
						NamedNodeMap attr = node.getAttributes();
						String name = attr.getNamedItem("name").getNodeValue();
						if (name.equalsIgnoreCase(service)) {
							getContext().createLineViolation(this, "Non-Public Services must inherit an Internal ACL",
									astNode);
							return;
						}
						if (name.equalsIgnoreCase(serviceRoot)
								&& !(node.getTextContent()).equalsIgnoreCase("Internal")) {
							getContext().createLineViolation(this, "Non-Public Services must inherit an Internal ACL",
									astNode);
							return;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isFlowCheck() {
		return true;
	}

	@Override
	public boolean isNodeCheck() {
		return false;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}

}
