import { bootstrapApplication, createApplication } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { provideRouter } from '@angular/router';
import { routes } from './app/app.routes';
import { createCustomElement } from '@angular/elements';


(async () => {
  const app = await createApplication({
    providers: [provideRouter(routes)],
  });

  const sidebarRoot = createCustomElement(AppComponent, {
    injector: app.injector,
  });

  console.log(sidebarRoot);

  customElements.define('sidebar-root', sidebarRoot);
})();


