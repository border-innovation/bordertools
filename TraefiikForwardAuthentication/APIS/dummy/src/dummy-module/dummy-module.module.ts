import { Module } from '@nestjs/common';
import { DummyModuleController } from './dummy-module.controller';

@Module({
  controllers: [DummyModuleController]
})
export class DummyModuleModule {}
