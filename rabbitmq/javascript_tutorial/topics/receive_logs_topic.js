#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

var args = process.argv.slice(2);

if (args.length == 0) {
    console.log("Usage: receive_logs_topics.js <facility>.<severity>");
    process.exit(1);
}

amqp.connect('amqp://localhost', function (error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }
        var exchange = 'topic_logs';

        channel.assertExchange(exchange, 'topic', {
            durable: false
        });

        channel.assertQueue('', {
            exclusive: true
        }, function (error2, q) {
            if (error2) {
                throw error2;
            }
            console.log(' [*] Waiting for logs. To exit press CTRL+C');

            args.forEach(function (key) {
                channel.bindQueue(q.queue, exchange, key);
            });

            channel.consume(q.queue, function (msg) {
                console.log(" [x] %s: '%s'", msg.fields.routingKey, msg.content.toString());
            }, {
                noAck: true
            });
        });
    });
});

/* :\Users\PedroAzevedo\PedroDesk\RabbitMQ\topics>node receive_logs_topic.js "#"
 [*] Waiting for logs. To exit press CTRL+C
 [x] anonymous.info: 'Hello World!'
 [x] kern.critical: 'A critical kernel error'


node receive_logs_topic.js "kern.*"
 [*] Waiting for logs. To exit press CTRL+C
 [x] kern.critical: 'A critical kernel error'


 node receive_logs_topic.js "*.critical"
 [*] Waiting for logs. To exit press CTRL+C
 [x] kern.critical: 'A critical kernel error'


 MQ\topics>node receive_logs_topic.js "kern.*" "*.critical"
 [*] Waiting for logs. To exit press CTRL+C
 [x] kern.critical: 'A critical kernel error'

 */