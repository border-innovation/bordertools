import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";

export class UserDto{
    @ApiProperty()
    @Expose()
    id: number;

    @ApiProperty()
    @Expose()
    email: string;

    @ApiProperty()
    @Expose()
    username: string;
    
    @ApiProperty()
    @Expose()
    fullName: string;
}

export class UserMinimalDto {
    @ApiProperty()
    @Expose()
    id: number;

    @ApiProperty()
    @Expose()
    username: string;
}