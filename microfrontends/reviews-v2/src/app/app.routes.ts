import { Routes } from '@angular/router';
import { ReviewsHomeComponent } from './reviews-home/reviews-home.component';
import { ReviewFullComponent } from './review-full/review-full.component';

export const routes: Routes = [

  {
    path: '',
    redirectTo: 'reviews',
    pathMatch:'full'
  },
  {
    path: 'home-reviews',
    component: ReviewsHomeComponent,
  },
  {
    path: 'review',
    component: ReviewFullComponent
  }

];
