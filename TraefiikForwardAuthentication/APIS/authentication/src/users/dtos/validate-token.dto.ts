import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsEmail, IsOptional, IsString } from "class-validator";

export class ValidateTokenDto{

    @ApiProperty()
    @IsString()
    token: string;
}