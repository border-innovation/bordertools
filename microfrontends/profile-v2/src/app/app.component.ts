import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { User } from './user.model';
import { MatCardModule } from '@angular/material/card';
import { TableBasicExample } from "./table/table.component";

@Component({
    selector: 'app-profile',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [RouterOutlet, MatCardModule, TableBasicExample]
})
export class AppComponent implements OnInit {
  title = 'profile';
  activeUser: User | undefined;

  ngOnInit(): void {
    const storedUser = localStorage.getItem('activeUser');
    if (storedUser !== null) {
      this.activeUser = JSON.parse(storedUser);
    }
  }
}
