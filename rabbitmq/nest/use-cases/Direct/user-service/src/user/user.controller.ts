import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { UserDto } from './user.dto';



@Controller('user')
export class UserController {

  constructor(private readonly userService: UserService, private readonly amqpConnection: AmqpConnection) { }

  @Post()
  addUser(@Body() user: UserDto): UserDto {
    return this.userService.addUser(user, this.amqpConnection);
  }
  @Post('rpc')
  addUserRcp(@Body() user: UserDto): any {
    return this.userService.addUserRPC(user, this.amqpConnection);
  }

}
