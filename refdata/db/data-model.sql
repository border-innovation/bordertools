--------------------------------------------------------
--  File created - quinta-feira-maio-23-2024   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table RESOURCEPROPERTIES
--------------------------------------------------------
 
  CREATE TABLE "RESOURCEPROPERTIES" ("RESOURCEKEY" NUMBER, "RESOURCENAME" VARCHAR2(100), "PARENTKEYID" NUMBER(1,0))
--------------------------------------------------------
--  DDL for Table RESOURCESYSTEM
--------------------------------------------------------
 
  CREATE TABLE "RESOURCESYSTEM" ("SYSTEMID" NUMBER, "SYSTEMNAME" VARCHAR2(100))
--------------------------------------------------------
--  DDL for Table SYSTEMPROPERTIES
--------------------------------------------------------
 
  CREATE TABLE "SYSTEMPROPERTIES" ("RESOURCEKEY" NUMBER, "SYSTEMID" NUMBER, "RESOURCEVALUE" VARCHAR2(4000 CHAR))
--------------------------------------------------------
--  DDL for Index RESOURCEPROPERTIES_PK
--------------------------------------------------------
 
  CREATE UNIQUE INDEX "RESOURCEPROPERTIES_PK" ON "RESOURCEPROPERTIES" ("RESOURCEKEY")
--------------------------------------------------------
--  DDL for Index RESOURCEPROPERTIES_UK
--------------------------------------------------------
 
  CREATE UNIQUE INDEX "RESOURCEPROPERTIES_UK" ON "RESOURCEPROPERTIES" ("RESOURCENAME")
--------------------------------------------------------
--  DDL for Index RESOURCESYSTEM_PK
--------------------------------------------------------
 
  CREATE UNIQUE INDEX "RESOURCESYSTEM_PK" ON "RESOURCESYSTEM" ("SYSTEMID")
--------------------------------------------------------
--  DDL for Index RESOURCESYSTEM_UK
--------------------------------------------------------
 
  CREATE UNIQUE INDEX "RESOURCESYSTEM_UK" ON "RESOURCESYSTEM" ("SYSTEMNAME")
--------------------------------------------------------
--  DDL for Index SYSTEMPROPERTIES_PK
--------------------------------------------------------
 
  CREATE UNIQUE INDEX "SYSTEMPROPERTIES_PK" ON "SYSTEMPROPERTIES" ("SYSTEMID", "RESOURCEKEY")
--------------------------------------------------------
--  DDL for Trigger RESOURCEPROPERTIES_TRG
--------------------------------------------------------
 
  CREATE OR REPLACE EDITIONABLE TRIGGER "RESOURCEPROPERTIES_TRG" 
BEFORE INSERT ON RESOURCEPROPERTIES 
FOR EACH ROW 
BEGIN
<<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.RESOURCEKEY IS NULL THEN
      SELECT RESOURCEPROPERTIES_SEQ.NEXTVAL INTO :NEW.RESOURCEKEY FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
 
ALTER TRIGGER "RESOURCEPROPERTIES_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger RESOURCESYSTEM_TRG
--------------------------------------------------------
 
  CREATE OR REPLACE EDITIONABLE TRIGGER "RESOURCESYSTEM_TRG" 
BEFORE INSERT ON RESOURCESYSTEM 
FOR EACH ROW 
BEGIN
<<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.SYSTEMID IS NULL THEN
      SELECT RESOURCESYSTEM_SEQ.NEXTVAL INTO :NEW.SYSTEMID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
 
ALTER TRIGGER "RESOURCESYSTEM_TRG" ENABLE
--------------------------------------------------------
--  Constraints for Table RESOURCEPROPERTIES
--------------------------------------------------------
 
  ALTER TABLE "RESOURCEPROPERTIES" ADD CONSTRAINT "RESOURCEPROPERTIES_UK" UNIQUE ("RESOURCENAME") USING INDEX "RESOURCEPROPERTIES_UK"  ENABLE
  ALTER TABLE "RESOURCEPROPERTIES" ADD CONSTRAINT "RESOURCEPROPERTIES_PK" PRIMARY KEY ("RESOURCEKEY") USING INDEX "RESOURCEPROPERTIES_PK"  ENABLE
  ALTER TABLE "RESOURCEPROPERTIES" MODIFY ("RESOURCENAME" NOT NULL ENABLE)
  ALTER TABLE "RESOURCEPROPERTIES" MODIFY ("RESOURCEKEY" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table RESOURCESYSTEM
--------------------------------------------------------
 
  ALTER TABLE "RESOURCESYSTEM" ADD CONSTRAINT "RESOURCESYSTEM_UK" UNIQUE ("SYSTEMNAME") USING INDEX "RESOURCESYSTEM_UK"  ENABLE
  ALTER TABLE "RESOURCESYSTEM" ADD CONSTRAINT "RESOURCESYSTEM_PK" PRIMARY KEY ("SYSTEMID") USING INDEX "RESOURCESYSTEM_PK"  ENABLE
  ALTER TABLE "RESOURCESYSTEM" MODIFY ("SYSTEMNAME" NOT NULL ENABLE)
  ALTER TABLE "RESOURCESYSTEM" MODIFY ("SYSTEMID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table SYSTEMPROPERTIES
--------------------------------------------------------
 
  ALTER TABLE "SYSTEMPROPERTIES" ADD CONSTRAINT "SYSTEMPROPERTIES_PK" PRIMARY KEY ("SYSTEMID", "RESOURCEKEY") USING INDEX "SYSTEMPROPERTIES_PK"  ENABLE
  ALTER TABLE "SYSTEMPROPERTIES" MODIFY ("RESOURCEVALUE" NOT NULL ENABLE)
  ALTER TABLE "SYSTEMPROPERTIES" MODIFY ("SYSTEMID" NOT NULL ENABLE)
  ALTER TABLE "SYSTEMPROPERTIES" MODIFY ("RESOURCEKEY" NOT NULL ENABLE)
--------------------------------------------------------
--  Ref Constraints for Table SYSTEMPROPERTIES
--------------------------------------------------------
 
  ALTER TABLE "SYSTEMPROPERTIES" ADD CONSTRAINT "SYSTEMPROPERTIES_RP_FK" FOREIGN KEY ("RESOURCEKEY") REFERENCES "RESOURCEPROPERTIES" ("RESOURCEKEY") ENABLE
  ALTER TABLE "SYSTEMPROPERTIES" ADD CONSTRAINT "SYSTEMPROPERTIES_RS_FK" FOREIGN KEY ("SYSTEMID") REFERENCES "RESOURCESYSTEM" ("SYSTEMID") ENABLE