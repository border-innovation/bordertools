package border.tools.rules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.NodeGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;
import border.tools.utils.BorderUtils;

@Rule(key = "S00017", name = "Wrappers, REST and SOAP Services must have comments in the Comments Tab", priority = Priority.MAJOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class CommentsCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(CommentsCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(NodeGrammar.VALUES);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String service = FlowUtils.getQualifiedName(this.getContext().getFile());
		String filename = this.getContext().getFile().getAbsolutePath();
		if (service.endsWith("_wrp") || BorderUtils.checkIfRestService(service, filename)
				|| BorderUtils.checkIfLegacyRestService(service) || BorderUtils.checkIfSoapService(service, filename)) {
			checkComments(astNode);
		}
	}

	private void checkComments(AstNode astNode) {
		for (AstNode value : astNode.getChildren(NodeGrammar.VALUE)) {
			for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
				for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
					if (name.getTokenValue().equals("NODE_COMMENT")) {
						if (attr.getParent().getChildren(FlowTypes.ELEMENT_VALUE).size() <= 0) {
							getContext().createLineViolation(this, "Add comment in the Comments tab", value);
						}
						return;
					}
				}
			}
		}
		getContext().createLineViolation(this, "Add comment in the Comments tab", astNode);
	}

	@Override
	public boolean isFlowCheck() {
		return false;
	}

	@Override
	public boolean isNodeCheck() {
		return true;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}

}
