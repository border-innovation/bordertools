import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppService } from './app.service';
import { AppsModule } from './apps/apps.module';
import { join } from 'path';
import { readFileSync } from 'fs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Apps } from './apps/apps.entitty';
import { ApisModule } from './apis/apis.module';
import { API } from './apis/apis.entity';
import { UsersModule } from './users/users.module';
import { PermissionsModule } from './permissions/permissions.module';
import { Permission } from './permissions/permissions.entity';
import { User } from './users/user.entity';
import { APP_GUARD, APP_PIPE } from '@nestjs/core';
import { AuthLibModule, JwtAuthGuard, JwtAuthGuardGRPC } from '@VascoRevesBorder/auth-lib';



const configPath = join(__dirname, '..', 'config', 'default.json');
const configJson = JSON.parse(readFileSync(configPath, 'utf-8'));

@Module({
  imports: [
    AuthLibModule,
    AppsModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [() => configJson], // Load the JSON config as a source
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        type: 'postgres',
        host: config.get<string>('database.host', 'localhost'),
        port: config.get<number>('database.port', 5432),
        username: config.get<string>('database.user'),
        password: config.get<string>('database.password'),
        database: config.get<string>('database.name'),
        entities: [Apps, API, Permission, User], 
        synchronize: true, // WARNING: Not recommended for production
      }),
    }),
    ApisModule,
    UsersModule,
    PermissionsModule,
  ],
  controllers: [],
  providers: [
    AppService,
    {
      provide: APP_PIPE,
      useValue:  new ValidationPipe({
        whitelist: true //Doesnt allow to add additional properties to request body
      })
    },
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard // Set JwtAuthGuard as a global guard
    },
  ],
})
export class AppModule {}
