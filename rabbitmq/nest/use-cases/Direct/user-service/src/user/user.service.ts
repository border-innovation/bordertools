import { Injectable } from '@nestjs/common';

import { UserDto } from './user.dto';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class UserService {

    addUser(user: UserDto, amqpConnection: AmqpConnection): any {
        console.log(amqpConnection.publish<UserDto>('direct_exchange', 'new-user', user))
        return ;
      
    }

    async addUserRPC(user: UserDto, amqpConnection: AmqpConnection) {

        const response = await amqpConnection.request<any>({
            exchange: 'rpc_exchange',
            routingKey: 'rpc-route',
            payload: {
                request: user
            }
        });

        console.log('response: ', response)
        return response
    }
}