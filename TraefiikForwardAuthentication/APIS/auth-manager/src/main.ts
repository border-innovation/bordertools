import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('Auth Manager')
    .setDescription('The Authentication Manager API description')
    .setVersion('1.0')
    .addTag('AuthManager')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('manager/swagger/api', app, document);
  await app.listen(3000);

}
bootstrap();
