import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ClientService } from './client.service'
import { ClientEntity } from './client.entity';
import { sleep } from '@nestjs/terminus/dist/utils';

@Controller('client')
export class ClientController {
    constructor (private clientService: ClientService) {}

    @Get()
    public async getClient() {
        return new Promise((resolve, reject) => {
            setTimeout(async () => {
                try {
                  const result = await this.clientService.getClients();
                  resolve(result);
                } catch (error) {
                  reject(error);
                }
              }, 100);
        })
    }

    @Get(':id')
    public async getClientById(@Param('id') id: number) {
        return await this.clientService.getClientById(Number(id));
    }

    @Post()
    public createClient(@Body() client: { first_name: string, second_name: string }) {
        return this.clientService.createClient(client);
    }

    // @Put(':id')
    // public async editClient(@Param('id') id: number, @Query() query) {
    //     const first_name = query.first_name;
    //     const second_name = query.second_name;
    //     return await this.clientService.editClient(Number(id), first_name, second_name);
    // }

    @Put(':id')
    public async editClient(@Param('id') id: number, @Query() query) {
        return await this.clientService.editClient(Number(id), query.first_name, query.second_name);
    }

    @Delete(':id')
    public async removeClient(@Param('id') id: number) {
        return await this.clientService.deleteClient(Number(id));
    }
}
