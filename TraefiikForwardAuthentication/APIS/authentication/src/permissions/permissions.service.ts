import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import { Permission } from './permissions.entity';
@Injectable()
export class PermissionService {     
    constructor(
        @InjectRepository(Permission) private repo: Repository<Permission>,
    ){}

    findBy( role?: string, username?: string){
        if(role) 
            return this.repo.findBy({role});
        if(username) 
            return this.repo.findBy({username});
        throw new BadRequestException('No parameters');
    }

    async find(id: number){
        if(!id) 
            throw new NotFoundException("Endpoint not found");
        const endpoint = await this.repo.findOneBy({id});
        if(!endpoint) 
            throw new NotFoundException("Endpoint not found");
        return endpoint
    }

    async findAll(){
        const obj = await this.repo.findAndCount();
        if(!obj.length) 
            throw new NotFoundException("No user roles available")
        return obj[0];
    }

    // async create( username: string, role: string) {
    //     //Validate Inputs
    //     if(!role) throw new BadRequestException('Missing role.');
    //     if(!username) throw new BadRequestException('Missing username.');

    //     const permission = await this.findBy(role,username);
    //     if (permission.some(x => x.role === role && x.username === username)) {
    //         throw new BadRequestException('User role pair already exists.');
    //     }

    //     const ep: Permission = this.repo.create({username,role})
    //     return this.repo.save(ep)
    // }

    // async remove(id: number){
    //     if(!id) 
    //     throw new NotFoundException('Endpoint not found!');
    //     const endpoint = await this.find(id);
    //     if(!endpoint) 
    //         throw new NotFoundException('Endpoint not found!');
    //     return this.repo.remove(endpoint);
    // }

    /* async updateUserRole(id: number, attrs: Partial<UserRole>){
        if(!id) 
            throw new NotFoundException("Endpoint not found");
        const UserRole = await this.find(id);
        if(!UserRole) 
            throw new NotFoundException('Endpoint not found')
        Object.assign(UserRole, attrs);
        return this.repo.save(UserRole);
    } */
    
}
