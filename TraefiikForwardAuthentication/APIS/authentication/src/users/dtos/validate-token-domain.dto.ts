import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsEmail, IsOptional, IsString } from "class-validator";

export class ValidateTokenDomainDto{
    @ApiProperty()
    @IsString()
    forwardUri: string;

    @ApiProperty()
    @IsString()
    forwardMethod: string;

    @ApiProperty()
    @IsString()
    token: string;
}