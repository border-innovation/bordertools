import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';


@Module({
  imports: [RabbitMQModule.forRoot(RabbitMQModule, {
    exchanges: [
      {
        name: 'direct_exchange',
        type: 'direct'
      },
      {
        name: 'rpc_exchange',
        type: 'direct'
      }
    ],
    uri: 'amqp://admin:admin@localhost:5672',
    connectionInitOptions: { wait: false }
  })],
  providers: [UserService],
  controllers: [UserController]
})
export class UserModule { }
