import { Module } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';

@Module({
  imports: [RabbitMQModule.forRoot(RabbitMQModule, {
    exchanges: [
      {
        name: 'article-exchange',
        type: 'headers'
      },
    ],
    uri: 'amqp://admin:admin@localhost:5672',
    connectionInitOptions: { wait: false },
  })],
  providers: [ArticleService],
  controllers: [ArticleController]
})
export class ArticleModule {}
