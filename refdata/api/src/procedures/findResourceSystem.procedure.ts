import { QueryRunner } from "typeorm";
import { BIND_OUT, BIND_IN, STRING, NUMBER, CURSOR } from 'oracledb'
import { ExecuteGetProcedure } from "src/executeQuery/selectQuery.procedure";


export class findResourceSystem extends ExecuteGetProcedure {
    name = 'TEST.FINDRESOURCESYSTEM';
    queryRunner: QueryRunner;
    callTimeout = 60000;
    batchSize = 10;
    parameters = [
        {dir: BIND_IN, name: 'i_system', type: STRING, val: null},
        {dir: BIND_IN, name: 'i_resource', type: STRING, val: null},
        {dir: BIND_OUT, name: 'o_response', type: CURSOR},
    ]; 

    constructor(queryRunner: QueryRunner, systemName: string, resourceName: string) {
        super(queryRunner);
        this.queryRunner = queryRunner;
        this.parameters[0].val = systemName;
        this.parameters[1].val = resourceName;
    }
}