import { HttpException, Injectable } from '@nestjs/common';
import { InjectDataSource } from "@nestjs/typeorm";
import { DataSource, QueryRunner } from "typeorm";
import { getAllClients } from "./_get/getAllClients.procedure";
import { getClientById } from './_get/getClientById.procedure';
import { createClient } from './_write/_post/createClient.procedure';
import { editClient } from './_write/_put/editClient.procedure';
import { deleteClient } from './_write/_delete/deleteClient.procedure';
import * as oracledb from 'oracledb';

@Injectable()
export class StoredProcedureService {
    //queryRunner: QueryRunner;
    count: number;

    constructor(@InjectDataSource() private dataSource: DataSource) {
        this.count = 0;
    } 

    async getAllClients() {
        console.log(`Request number: ${++this.count}`)
        let queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        const proc = await new getAllClients(queryRunner).execute();
        await queryRunner.release();
        return proc;
    }

    async getClientById(id: number) {
        let queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        const proc = await new getClientById(queryRunner, id).execute();
        await queryRunner.release();
        return proc;
    }

    async createClient(client: { first_name: string, second_name: string }) {
        let queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        const proc = await new createClient(queryRunner, client.first_name, client.second_name).execute();
        await queryRunner.release();
        return proc
    }

    async editClient(id: number, first_name: string, second_name: string) {
        let queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        const clientId = await new editClient(queryRunner, id, first_name, second_name).execute();
        await queryRunner.release();
        return clientId;
    }

    async removeClient(id: number) {
        let queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        const idRemoved = await new deleteClient(queryRunner, id).execute();
        await queryRunner.release();
        return idRemoved;
    }
}
