import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap } from "rxjs";
import {
  AddPermissionFailed,
  AddPermissionRequest,
  AddPermissionSuccess,
  ListPermissionsFailed,
  ListPermissionsRequest,
  ListPermissionsSuccess,
  PermissionsActionTypes,
  RemovePermissionByIdFailed,
  RemovePermissionByIdSuccess,
  UpdatePermissionFailed,
  UpdatePermissionSuccess,
} from "./actions";
import {  PermissionModel } from "./models/permissions.model";
import { AdminPermissionsService } from "../../admin/admin-permissions/admin-permissions.service";

@Injectable()
export class PermissionsEffects {
  constructor(
    private actions$: Actions,
    private permissionsService: AdminPermissionsService
  ) {}

  getPermissions$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(PermissionsActionTypes.GET_ALL),
      mergeMap(() => {
        return this.permissionsService.getAllPermissions().pipe(
          map(( permissions: any ) => {
            return new ListPermissionsSuccess(permissions)
          }),
          catchError((e) => [new ListPermissionsFailed(e)])
        )
      })
    );
  });

  addPermissions$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(PermissionsActionTypes.ADD_NEW),
      mergeMap((permission: any) => {
        console.log("Adding permission effect 2", permission.permission);
        return this.permissionsService.addPermission(permission.permission).pipe(
          map(( permissions ) => {
            console.log("Adding permissions effect 3", permissions);
            return new AddPermissionSuccess(permission.permission)
          }),
          catchError((e) => [new AddPermissionFailed(e)])
        )
      })
    );
  })

  addPermissionSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PermissionsActionTypes.ADD_NEW_SUCCESS),
      map(() => new ListPermissionsRequest())
    )
  );
  

  updatePermission$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(PermissionsActionTypes.UPDATE_ONE),
      mergeMap((permission: any) => {
        return this.permissionsService.updatePermission(permission.permission).pipe(
          map(( permissions ) => {
            console.log("Update permission effects: ", permissions)
            return new UpdatePermissionSuccess(permission)
          }),
          catchError((e) => [new UpdatePermissionFailed(e)])
        )
      })
    );
  })


  removePermissions$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(PermissionsActionTypes.REMOVE_ID),
      mergeMap((id: any) => {
        console.log("Remove Permissions Effects: ", id)
        return this.permissionsService.removePermission(id.id).pipe(
          map(( permissions ) => {
            return new RemovePermissionByIdSuccess(id)
          }),
          catchError((e) => [new RemovePermissionByIdFailed(e)])
        )
      })
    );
  })

  removePermissionSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PermissionsActionTypes.REMOVE_ID_SUCCESS),
      map(() => new ListPermissionsRequest())
    )
  );
}
