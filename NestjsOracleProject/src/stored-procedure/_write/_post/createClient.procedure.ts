import { QueryRunner } from "typeorm";
import { BIND_OUT, BIND_IN, STRING, NUMBER, CURSOR } from 'oracledb'
import { ExecuteWriteProcedure } from "../executeQuery.procedure";

export class createClient extends ExecuteWriteProcedure {
    name = 'TEST.CREATECLIENT';
    queryRunner: QueryRunner;
    callTimeout = 60000;
    batchSize = 10;
    parameters = [
        { dir: BIND_IN, name: 'i_first_name', type: STRING, val: null},
        { dir: BIND_IN, name: 'i_second_name', type: STRING, val: null},
        { dir: BIND_OUT, name: 'o_id', type: NUMBER},
    ];

    constructor(queryRunner: QueryRunner, first_name: string, second_name: string) {
        super(queryRunner);
        this.queryRunner = queryRunner;
        this.parameters[0].val = first_name;
        this.parameters[1].val = second_name;
    }
}