#!/bin/bash
retry=0
max_retries=5
sleep_time=10


kubectl delete deployment authenticator-app

cd APP/authenticator-app/

ng build

cd ../..
# Build Docker images
 docker build -t traefiikforwardauthentication_authenticator-app ./APP/authenticator-app


# Tag and push Docker images
images=(
   "traefiikforwardauthentication_authenticator-app"
)

for image in "${images[@]}"; do
  docker tag "${image}:latest" "localhost:5001/${image}:latest"
  if [ $? -ne 0 ]; then
    echo "Failed to tag ${image}"
    exit 1
  fi

  docker push "localhost:5001/${image}:latest"
  if [ $? -ne 0 ]; then
    echo "Failed to push ${image}"
    exit 1
  fi
done

kubectl apply -f ./APP/authenticator-app/k8s/deployment.yaml
if [ $? -ne 0 ]; then
  echo "Failed to apply APP deployment"
  exit 1
fi

