import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClientEntity } from './client/client.entity';
import { Repository } from 'typeorm';
import * as oracledb from 'oracledb';

@Injectable()
export class AppService implements OnModuleInit {

  constructor (
    @InjectRepository(ClientEntity) private clientRepository: Repository<ClientEntity>,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  async onModuleInit() {
    // Your code to be executed after application starts
    console.log('Application started!');
    //console.log(await oracledb.getPool());
  }
}
