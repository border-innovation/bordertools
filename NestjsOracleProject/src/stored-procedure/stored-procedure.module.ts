import { Module } from '@nestjs/common';
import { StoredProcedureService } from './stored-procedure.service';

@Module({
  providers: [StoredProcedureService]
})
export class StoredProcedureModule {}
