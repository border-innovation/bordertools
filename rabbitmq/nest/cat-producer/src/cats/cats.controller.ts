import { Body, Controller, Get, Post } from '@nestjs/common';
import { CatsService } from './cats.service';
import { EventPattern, Payload } from '@nestjs/microservices';

@Controller('cats')
export class CatsController {

    constructor(private readonly catsService: CatsService) { }

    @Post('add')
    addCat(@Body() catData: any) {
        
        return this.catsService.sendCatToQueue(catData);
    }

    
}
