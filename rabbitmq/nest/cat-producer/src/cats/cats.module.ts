import { Module } from '@nestjs/common';
import { CatsService } from './cats.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CatsController } from './cats.controller';

@Module({
  imports: [ClientsModule.register([
    {
      name: 'CATS_SERVICE',
      transport: Transport.RMQ,
      options: {
        urls: ['amqp://admin:admin@127.0.0.1:5672'],
        queue: 'cats_queue',
        queueOptions: {
          durable: false
        },
      },
    },
  ]),],
  providers: [CatsService],
  controllers: [CatsController]
})
export class CatsModule { }
