import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap } from "rxjs";
import {
  AddAppFailed,
  AddAppSuccess,
  AppsActionTypes,
  ListAllAppsFailed,
  ListAllAppsRequest,
  ListAllAppsSuccess,
  ListAppsFailed,
  ListAppsRequest,
  ListAppsSuccess,
  RemoveAppByIdFailed,
  RemoveAppByIdSuccess,
  UpdateAppFailed,
  UpdateAppSuccess,
} from "./actions";
import { AdminAppsService } from "../../admin/admin-apps/admin-apps.service";

@Injectable()
export class AppEffects {
  constructor(
    private actions$: Actions,
    private appsService: AdminAppsService
  ) {}

  getApps$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(AppsActionTypes.GET),
      mergeMap(() => {
        return this.appsService.getApps().pipe(
          map(( apps ) => {
            return new ListAppsSuccess(apps)
          }),
          catchError((e) => [new ListAppsFailed(e)])
        )
      })
    );
  });

  getAllApps$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(AppsActionTypes.GET_ALL),
      mergeMap(() => {
        return this.appsService.getAllApps().pipe(
          map(( apps ) => {
            return new ListAllAppsSuccess(apps)
          }),
          catchError((e) => [new ListAllAppsFailed(e)])
        )
      })
    );
  });
  
  addApps$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(AppsActionTypes.ADD_NEW),
      mergeMap((app: any) => {
        return this.appsService.addApps(app.app).pipe(
          map(( apps ) => {
            console.log("Add apps effects: ", apps)
            return new AddAppSuccess(app.app)
          }),
          catchError((e) => [new AddAppFailed(e)])
        )
      })
    );
  })

  addAPPSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AppsActionTypes.ADD_NEW_SUCCESS),
      map(() => new ListAllAppsRequest())
    )
  );


  updateApps$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(AppsActionTypes.UPDATE_ONE),
      mergeMap((app: any) => {
        return this.appsService.updateApps(app.app).pipe(
          map(( apps ) => {
            console.log("Update apps effects: ", apps)
            return new UpdateAppSuccess(app)
          }),
          catchError((e) => [new UpdateAppFailed(e)])
        )
      })
    );
  })

  // updateAPPSuccess$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(AppsActionTypes.UPDATE_ONE),
  //     map(() => new ListAppsRequest())
  //   )
  // );

  removeApps$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(AppsActionTypes.REMOVE_ID),
      mergeMap((id: any) => {
        console.log("Remove Apps Effects: ", id)
        return this.appsService.removeApp(id.id).pipe(
          map(( apps ) => {
            return new RemoveAppByIdSuccess(id)
          }),
          catchError((e) => [new RemoveAppByIdFailed(e)])
        )
      })
    );
  })

  removeAPPSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AppsActionTypes.REMOVE_ID_SUCCESS),
      map(() => new ListAllAppsRequest())
    )
  );
}
