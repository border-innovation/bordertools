import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";

export class PermissionDto{
    @ApiProperty()
    @Expose()
    id: string;

    @ApiProperty()
    @Expose()
    username: string;
    
    @ApiProperty()
    @Expose()
    role: string;
}