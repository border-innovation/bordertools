import { AppActions, AppsActionTypes } from "./actions";
import { AppModel } from "./models/apps.model";

export interface AppsState {
  allApps: AppModel[];
  apps: AppModel[];
  isLoading: boolean;
}

export const initialState: AppsState = {
  apps: [],
  allApps: [],
  isLoading: false,
};

export function appsReducer(
  state = initialState,
  action: AppActions
): AppsState {
  switch (action.type) {
    case AppsActionTypes.GET:
      return {
        ...state,
        isLoading: true,
      };
    case AppsActionTypes.GET_SUCCESS:
      console.log("Reducer GET_SUCCESS - Incoming apps:", action.apps); // Check if the id is here
      return {
        ...state,
        isLoading: false,
        apps: action.apps,
      };
    case AppsActionTypes.GET_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case AppsActionTypes.GET_ALL:
      return {
        ...state,
        isLoading: true,
      };
    case AppsActionTypes.GET_ALL_SUCCESS:
      console.log("Reducer GET_ALL_SUCCESS - Incoming apps:", action.apps); // Check if the id is here
      return {
        ...state,
        isLoading: false,
        allApps: action.apps,
      };
    case AppsActionTypes.GET_ALL_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case AppsActionTypes.ADD_NEW:
      return {
        ...state,
        isLoading: true,
      };
    case AppsActionTypes.ADD_NEW_SUCCESS:
      return {
        ...state,
        isLoading: false,
        allApps: [...state.allApps, action.app],
      };
    case AppsActionTypes.ADD_NEW_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case AppsActionTypes.UPDATE_ONE:
      return {
        ...state,
        isLoading: true,
      };
    case AppsActionTypes.UPDATE_ONE_SUCCESS:
      const updatedAppsList = state.apps.map((r) => {
        if (r.id !== action.app.id) return r;
        return { ...r, ...action.app };
      });
    
      const updatedAllAppsList = state.allApps.map((r) => {
        if (r.id !== action.app.id) return r;
        return { ...r, ...action.app };
      });
    
      return {
        ...state,
        isLoading: false,
        apps: [...updatedAppsList],       
        allApps: [...updatedAllAppsList], 
      };
    case AppsActionTypes.UPDATE_ONE_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case AppsActionTypes.REMOVE_ID:
      return {
        ...state,
        isLoading: true,
      };
    case AppsActionTypes.REMOVE_ID_SUCCESS:
      return {
        ...state,
        isLoading: false,
        apps: state.apps.filter((v) => v.id !== action.app.id),
        allApps: state.allApps.filter((v) => v.id !== action.app.id),

      };
    case AppsActionTypes.REMOVE_ID_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
