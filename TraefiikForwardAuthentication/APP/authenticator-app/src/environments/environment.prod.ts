export const environment = {
    production: false,
    API_BASE_URL: 'http://vasco.prd.berzuk.com:8000',
    APPS_API: '/apps',
    APPS_LIST_API: '/apps/list',
    APIS_API: '/endpoints',
    AUTH_API: '/auth',
    AUTH_API_PERMISSIONS: '/permissions',
    AUTH_API_SIGN_IN: '/auth/signin',
    AUTH_API_SIGN_UP: '/auth/signup',
    AUTH_API_USERS: '/auth/users',
    ADMIN_ROLE: 'AuthenticatorAdmin'
}