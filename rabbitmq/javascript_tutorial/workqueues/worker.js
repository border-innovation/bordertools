var amqp = require('amqplib/callback_api');

//Same as the sender to start the connection and declare the queue to consume

amqp.connect('amqp://localhost', (error0, connection) => {
    if (error0) throw error0;

    connection.createChannel((error1, channel) => {
        if (error1) throw error1;

        var queue = 'task_queue';

        // This makes sure the queue is declared before attempting to consume from it
        channel.assertQueue(queue, {
            durable: true
        });

        channel.prefetch(1);
        channel.consume(queue, function (msg) {
            var secs = msg.content.toString().split('.').length - 1;

            console.log(" [x] Received", msg.content.toString());

            setTimeout(() => {
                console.log("[x] Done")
                channel.ack(msg);
            }, secs * 1000);

        }, {
            // automatic acknowledgment mode,
            // see /docs/confirms for details
            noAck: false
        });

    });
});