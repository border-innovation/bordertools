import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { User } from './user.entity';
import { AuthService } from '../auth/auth.service';
import { ConfigService } from '@nestjs/config';
import { APIsModule } from 'src/apis/apis.module';
import { PermissionsModule } from 'src/permissions/permissions.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    APIsModule,
    PermissionsModule
  ],
  controllers: [UsersController],
  providers: [
    UsersService,  
    AuthService,
    JwtService,
    ConfigService
  ],
  exports: [
    UsersService,
    AuthService
  ]
})
export class UsersModule {

}
