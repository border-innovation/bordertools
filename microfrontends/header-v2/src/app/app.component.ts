import { Component, EventEmitter, Injector, Input, OnInit, Output, input, signal } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { User } from './user.model';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';


@Component({
  selector: 'header-root',
  standalone: true,
  imports: [RouterOutlet, MatButtonModule, MatToolbarModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  title = 'header';
  activeUser: User | undefined;

  private storageEventListener: (event: StorageEvent) => void;


  constructor() {
    this.storageEventListener = this.handleStorageEvent.bind(this);
  }

  ngOnInit(): void {
    this.activeUser = this.getUserFromLocalStorage();
    window.addEventListener('storage', this.storageEventListener);
  }

  ngOnDestroy(): void {
    window.removeEventListener('storage', this.storageEventListener);
  }

  private handleStorageEvent(event: StorageEvent): void {
    if (event.key === 'activeUser') {
      this.activeUser = this.getUserFromLocalStorage();
    }
  }

  private getUserFromLocalStorage(): any {
    const userData = localStorage.getItem('activeUser');
    console.log("header:", userData);
    return userData ? JSON.parse(userData) : null;
  }

  @Output() navigate = new EventEmitter<string>();

  navigateToPage(page: string) {
    this.navigate.emit(page);
  }


}
