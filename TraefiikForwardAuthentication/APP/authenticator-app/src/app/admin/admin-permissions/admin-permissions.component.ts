import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { selectPermissions } from '../../store/permissions/selector';
import { MatTableDataSource } from '@angular/material/table';
import { AddPermissionRequest, RemovePermissionByIdRequest, UpdatePermissionRequest } from '../../store/permissions/actions';
import { MatSort } from '@angular/material/sort';
import { AuthService } from '../../auth/auth.service';
import { map, startWith } from 'rxjs';


interface permissions {
  id?: string;
  username: string;
  role: string;
  isEdit?: boolean;
}

@Component({
  selector: 'app-admin-permissions',
  templateUrl: './admin-permissions.component.html',
  styleUrl: './admin-permissions.component.scss'
})
export class AdminPermissionsComponent implements OnInit {  
  createPermissionForm: FormGroup;
  editPermissionForm: FormGroup;
  dataSource;
  permissionList: permissions[];
  displayedColumns: string[] = ['username', 'role', 'actions'];
  users; 
  filteredUsers; 
  noMatch: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private authService: AuthService

  ){
    this.createPermissionForm = this.formBuilder.group({
      username: new FormControl(""),
      role: new FormControl("")
    });

    this.editPermissionForm = this.formBuilder.group({
      username: new FormControl(""),
      role: new FormControl("")
    });
  }

  ngOnInit() {
    this.store.select(selectPermissions)
      .subscribe(({ permissions, isLoading }) => {
        if (permissions && permissions.length > 0) {
          this.dataSource = new MatTableDataSource(permissions);
          this.permissionList = permissions;
        } else if (!isLoading) {
          console.log("No permissions available for this user...");
        }
        //this.cdr.detectChanges();
      });
 
    this.authService.getUsers().subscribe(users => {
      this.users = users;
      this.filteredUsers = users;  
      console.log("List all users: ", this.users)
      this.createPermissionForm.get('username').valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value || ''))
      ).subscribe(filtered => {
        this.filteredUsers = filtered;
      });
    });

    
  }

  private _filter(value: string): any[] {
    const filterValue = value.toLowerCase();

    const filteredUsers = this.users.filter(user => user.username.toLowerCase().includes(filterValue));

    this.noMatch = filteredUsers.length === 0;
    return filteredUsers;
  }

  onSubmit(): void {
    if (this.createPermissionForm.valid) {
      const apiData = this.createPermissionForm.value; 
      console.log('Permission Form Submitted:', apiData);
      this.store.dispatch(new AddPermissionRequest(apiData));
      this.createPermissionForm.reset();
    } else {
      console.log('Form not valid');
    }
  }


  removePermission(permission: permissions) {
    console.log("Deleting permissions: ", permission)
    this.store.dispatch(new RemovePermissionByIdRequest(permission.id));

  }

  editPermission(permission: permissions) {
    console.log("IsEditingRow: ", permission)

    this.editPermissionForm.patchValue({
      username: permission.username,
      role: permission.role
    });


    if (permission.isEdit) {
      this.updatePermission(permission);
    }
    permission.isEdit = !permission.isEdit; 
  }

  updatePermission(permission: permissions) {
    // TODO
    // Validate Response, if ok alert ui
    console.log('Saving permission:', permission);
    this.store.dispatch(new UpdatePermissionRequest(permission));

  }

  cancelEdit(permission: permissions){
    permission.isEdit = !permission.isEdit;
  }
}




  
