import { IsString } from "@nestjs/class-validator";

export class UserDto{
    @IsString()
    username: string;

    @IsString()
    password: string;
}