import { QueryRunner } from "typeorm";
import { BIND_OUT, BIND_IN, STRING, NUMBER, CURSOR } from 'oracledb'
import { ExecuteGetProcedure } from "src/executeQuery/selectQuery.procedure";


export class getAllResourceProperties extends ExecuteGetProcedure {
    name = 'TEST.GETALLRESOURCESPROPERTIES';
    queryRunner: QueryRunner;
    callTimeout = 60000;
    batchSize = 10;
    parameters = [
        {dir: BIND_IN, name: 'i_system_name', type: STRING, val: null},
        {dir: BIND_IN, name: 'i_resource_name', type: STRING, val: null},
        {dir: BIND_IN, name: 'i_resource_value', type: STRING, val: null},
        {dir: BIND_OUT, name: 'o_response', type: CURSOR},
    ]; 

    constructor(queryRunner: QueryRunner, systemName: string, resourceName: string, resourceValue: string) {
        super(queryRunner);
        this.queryRunner = queryRunner;
        this.parameters[0].val = systemName;
        this.parameters[1].val = resourceName;
        this.parameters[2].val = resourceValue;
    }
}