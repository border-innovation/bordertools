import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppsActionTypes, ListAllAppsRequest, ListAppsRequest } from '../store/apps/actions';
import { selectApps } from '../store/apps/selector';
import { ListAPIsRequest } from '../store/apis/actions';
import { ListPermissionsRequest } from '../store/permissions/actions';
import { environment } from '../../environments/environment';

interface Application {
  appName: string;
  endpoint: string;
}


@Component({
  selector: 'app-router',
  templateUrl: './router.component.html',
  styleUrl: './router.component.scss'
})
export class RouterComponent implements OnInit, OnDestroy{
  isAuthenticated = false;
  isAdminUser = false;
  private userSub: Subscription;
  
  appForm: FormGroup;
  applications: Application[] = [];
  // applications: Application[] = [
  //   {name:'', endpoint:''},
  //   { name: 'App1', endpoint: '/app1' },
  //   { name: 'App2', endpoint: '/app2' },
  //   { name: 'App3', endpoint: '/app3' }
  // ];


  constructor(
    private formBuilder: FormBuilder,
    private router : Router,
    private authService: AuthService,
    private http: HttpClient,
    private store: Store

  ) {
    this.appForm = this.formBuilder.group({
      selectedApp: new FormControl("")
    });
  
  }
  
  ngOnInit(): void {
      this.userSub = this.authService.user.subscribe(user=>{
          this.isAuthenticated = !!user;
      });
      
      this.isAdminUser = this.authService.isAdminUser();

      console.log("Getting apps router component: ", this.isAdminUser)
     
      this.store.dispatch(new ListAppsRequest());
      this.store.dispatch(new ListAllAppsRequest());
      this.store.dispatch(new ListAPIsRequest());
      this.store.dispatch(new ListPermissionsRequest());

      this.store.select(selectApps).subscribe(({ apps, isLoading }) => {
        this.applications = apps;
      });
  }

  ngOnDestroy(): void {
      this.userSub.unsubscribe();
  }

  onSubmit() {
    if (this.appForm.valid) {
      const selectedEndpoint = this.appForm.value.selectedApp;
      console.log('Redirecting to:', selectedEndpoint);
      
      window.location.href = selectedEndpoint;
      
      //window.location.href = '/messages/verify';

    } else {
      console.log('Form is invalid');
    }
  }

  loadAdminPage(){
    this.router.navigate(['/admin']);
  }

}
