package border.tools.rules;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.NodeGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;
import border.tools.utils.BorderUtils;

@Rule(key = "S00021", name = "SOAP Services must declare on their outputs a Document variable named status", priority = Priority.MAJOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class SoapOutputCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(SoapOutputCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(NodeGrammar.SIGNATURE_OUT);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String service = FlowUtils.getQualifiedName(this.getContext().getFile());
		String filename = this.getContext().getFile().getAbsolutePath();
		if (BorderUtils.checkIfSoapService(service, filename)) {
			checkOutputResults(astNode);
		}
	}

	private void checkOutputResults(AstNode astNode) {
		for (AstNode array : astNode.getChildren(NodeGrammar.REC_FIELDS)) {
			for (AstNode record : array.getChildren(NodeGrammar.RECORD)) {
				for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
					for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
						for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
							if (name.getTokenValue().equals("FIELD_NAME")) {
								String results = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue()
										.strip();
								if (results.equals("status")) {
									checkStatusType(record);
									return;
								}
							}
						}
					}
				}
			}
		}
		getContext().createLineViolation(this, "SOAP Services must declare a variable named status of Document type.",
				astNode);
	}

	private void checkStatusType(AstNode record) {
		for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
			for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
				for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
					if (name.getTokenValue().equals("FIELD_TYPE")) {
						String type = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue().strip();
						if (type.equals("record")) {
							checkStatusDocument(record);
							return;
						}
						if (type.equals("recref")) {
							String serviceName = checkDocumentReferenceName(record);
							checkInsideDocumentReference(serviceName, record);
							return;
						}
					}
				}
			}
		}
		getContext().createLineViolation(this, "status must be a Document or a Document Reference.", record);
	}

	private void checkStatusDocument(AstNode statusNode) {
		ArrayList<String> statusVariables = new ArrayList<>(
				Arrays.asList("errorCode", "errorMsg", "errorInstanceId", "errorDump", "errorOwner"));
		for (AstNode array : statusNode.getChildren(NodeGrammar.REC_FIELDS)) {
			for (AstNode record : array.getChildren(NodeGrammar.RECORD)) {
				for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
					for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
						for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
							if (name.getTokenValue().equals("FIELD_NAME")) {
								String results = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue()
										.strip();
								if (statusVariables.contains(results)) {
									statusVariables.remove(results);
									checkIfVariableString(record, results);
								}
							}
						}
					}
				}
			}
		}
		if (statusVariables.size() > 0) {
			for (String statusName : statusVariables) {
				getContext().createLineViolation(this, statusName + " must be declared inside the status Document.",
						statusNode);
			}
		}
	}

	private void checkIfVariableString(AstNode record, String results) {
		for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
			for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
				for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
					if (name.getTokenValue().equals("FIELD_TYPE")) {
						String fieldType = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue().strip();
						if (!fieldType.equals("string"))
							getContext().createLineViolation(this, results + " must be of type String.", value);
					}
				}
			}
		}
	}

	private String checkDocumentReferenceName(AstNode record) {
		for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
			for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
				for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
					if (name.getTokenValue().equals("REC_REF")) {
						return value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue().strip();
					}
				}
			}
		}
		return null;
	}

	private void checkInsideDocumentReference(String serviceName, AstNode statusNode) {
		try {
			String nodePkg = serviceName.substring(0, serviceName.indexOf("."));
			serviceName = serviceName.replace(".", "\\").replace(":", "\\");
			ArrayList<String> statusVariables = new ArrayList<>(
					Arrays.asList("errorCode", "errorMsg", "errorInstanceId", "errorDump", "errorOwner"));
			File file = new File("C:\\SoftwareAG\\WM1011\\IntegrationServer\\instances\\default\\packages\\" + nodePkg
					+ "\\ns\\" + serviceName + "\\node.ndf");
			DocumentBuilder db;
			db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("value");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NamedNodeMap attr = node.getAttributes();
				String name = attr.getNamedItem("name").getNodeValue();
				if (name.equals("field_name") && statusVariables.contains(node.getTextContent())) {
					statusVariables.remove(node.getTextContent());
				}
			}
			if (statusVariables.size() > 0) {
				for (String statusName : statusVariables) {
					getContext().createLineViolation(this, statusName + " must be declared inside the status Document.",
							statusNode);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isFlowCheck() {
		return false;
	}

	@Override
	public boolean isNodeCheck() {
		return true;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}
}
