import { Component, OnInit } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-reviews',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [RouterOutlet]
})
export class AppComponent implements OnInit{
  title = 'reviews';

  constructor(private router: Router){}

  ngOnInit(): void {
    this.router.navigate(['/reviews/home-reviews']);
  }

}
