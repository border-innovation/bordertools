import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsEmail, IsLatitude, IsLongitude, IsNumber, IsOptional, IsString, Max, Min } from "class-validator";

export class UpdateAPIDto{
    @ApiProperty()
    @IsOptional()
    @IsString()
    name: string;
    
    @ApiProperty()
    @IsOptional()
    @IsString()
    endpoint: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    role: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    verb: string;

}