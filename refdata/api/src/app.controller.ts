import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { HealthCheck, HealthCheckResult, HealthCheckService, TypeOrmHealthIndicator } from '@nestjs/terminus';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as oracledb from 'oracledb';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly healthCheckService: HealthCheckService,
    private readonly typeOrmHealthIndicator: TypeOrmHealthIndicator,
    //@InjectRepository(ClientEntity) private clientRepository: Repository<ClientEntity>,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @HealthCheck()
  @Get('/healthcheck')
  async healthCheck(): Promise<HealthCheckResult> {
    try {
      const result = await this.healthCheckService.check([]);
      console.log('Server is Up!');
      return result;
    } catch (error) {
      console.error('Health check failed:', error);
      throw error; // Rethrow the error to let NestJS handle it
    }
  }

  @HealthCheck()
  @Get('/healthcheck/database')
  async databaseHealthCheck(): Promise<HealthCheckResult> {
    try {
      const result = await this.healthCheckService.check([
        () => this.typeOrmHealthIndicator.pingCheck('database'),
      ]);
      console.log('Database health check is completed!');
      return result;
    } catch (error) {
      console.error('Database health check failed:', error);
      throw error; // Rethrow the error to let NestJS handle it
    }
  }
}