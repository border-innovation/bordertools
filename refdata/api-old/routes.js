/* Modules */
const healthcheck = require('./routes/healthcheck');
const reference_data = require('./routes/refdata');
const systems = require('./routes/systems');

/* env values */
const config = require('config');
const api = config.get('api');

/**
 * ROUTEs DECLARATION:
 * api.api_url should be used on all of them for structure.
 */

module.exports = function (app) {
  app.use(api.api_url + api.healthcheck, healthcheck);
  app.use(api.api_url + api.reference_data, reference_data);
  app.use(api.api_url + api.systems, systems);
}
