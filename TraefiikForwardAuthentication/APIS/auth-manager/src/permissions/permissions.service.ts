import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Permission } from './permissions.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PermissionsService {
    constructor(
        @InjectRepository(Permission) private repo: Repository<Permission>,
    ){}

    async create( username: string, role: string) {
        //Validate Inputs
        if(!role) throw new BadRequestException('Missing role.');
        if(!username) throw new BadRequestException('Missing username.');

        const permission = await this.findBy(role,username);
        if (permission.some(x => x.role === role && x.username === username)) {
            throw new BadRequestException('User role pair already exists.');
        }

        const ep: Permission = this.repo.create({username,role})
        console.log("Permission: ", ep)
        return this.repo.save(ep)
    }

    findBy( role?: string, username?: string){
        if(role) 
            return this.repo.findBy({role});
        if(username) 
            return this.repo.findBy({username});
        throw new BadRequestException('No parameters');
    }

    async find(id: number){
        if(!id) 
            throw new NotFoundException("Endpoint not found");
        const endpoint = await this.repo.findOneBy({id});
        if(!endpoint) 
            throw new NotFoundException("Endpoint not found");
        return endpoint
    }

    async findAll(){
        const obj = await this.repo.findAndCount();
        if(!obj.length) 
            throw new NotFoundException("No user roles available")
        return obj[0];
    }

    async remove(id: number){
        if(!id) 
        throw new NotFoundException('Endpoint not found!');
        const endpoint = await this.find(id);
        if(!endpoint) 
            throw new NotFoundException('Endpoint not found!');
        return this.repo.remove(endpoint);
    }
}
