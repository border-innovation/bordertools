package border.tools.rules;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.FlowGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;

@Rule(key = "S00031", name = "Web Services that are Invoked on other services can't have pass and user 'hardcoded' with a value.", priority = Priority.CRITICAL, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)
public class WebServicesCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(WebServicesCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(FlowGrammar.INVOKE);
	}

	@Override
	public void visitNode(AstNode astNode) {
		AstNode service = astNode.getFirstChild(FlowGrammar.ATTRIBUTES).getFirstChild(FlowAttTypes.SERVICE);
		if (service != null) {
			String name = service.getTokenValue();
			if (name.equalsIgnoreCase("pub.client:http") || name.contains("CONNECTORS")) {
				for (AstNode mapsets : astNode.getFirstChild(FlowGrammar.MAP).getFirstChild(FlowGrammar.MAPPING)
						.getChildren(FlowGrammar.MAPSET)) {
					for (AstNode attr : mapsets.getChildren(FlowGrammar.ATTRIBUTES)) {
						for (AstNode field : attr.getChildren()) {
							if (field.getTokenValue().equals("FIELD")) {
								for (AstNode fieldValue : field.getChildren()) {
									if (fieldValue.getTokenValue().contains("auth")
											&& fieldValue.getTokenValue().contains("user")) {
										checkMapsetValue(mapsets, "user");
									}
									if (fieldValue.getTokenValue().contains("auth")
											&& fieldValue.getTokenValue().contains("pass")) {
										checkMapsetValue(mapsets, "pass");
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void checkMapsetValue(AstNode mapsets, String field) {
		AstNode values = mapsets.getFirstChild(FlowGrammar.DATA).getFirstChild(FlowGrammar.VALUES);
		if (values != null) {
			List<AstNode> texts = values.getFirstChild(FlowGrammar.VALUE).getChildren(FlowTypes.ELEMENT_VALUE);
			if (texts.size() > 0) {
				getContext().createLineViolation(this,
						"Invoked Web Services cant have a 'hard-coded' " + field + " defined", mapsets);
			}
		}
	}

	@Override
	public boolean isFlowCheck() {
		return true;
	}

	@Override
	public boolean isNodeCheck() {
		return false;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}

}
