import * as mqtt from 'mqtt';

const client: mqtt.MqttClient = mqtt.connect("mqtt://localhost:1883");

client.on("connect", function () {
    console.log('Connected');
    client.publish("topicMqtt", "Pub using mqtt");
    client.end();
});