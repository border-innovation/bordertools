import { QueryRunner } from "typeorm";
import { Procedure } from "../interface/procedure.interface";
import { BadRequestException } from "@nestjs/common";
import { ResultSet } from 'oracledb';

export interface ProcedureParameter {
    dir: number; // BIND_IN or BIND_OUT
    name: string;
    type: number; // NUMBER, STRING, CURSOR
    val?: any;
}

export abstract class ExecuteWriteProcedure implements Procedure {
    name: string;
    queryRunner: QueryRunner;
    parameters: ProcedureParameter[] = [];
    callTimeout = 60000;
    batchSize?: number;

    constructor(queryRunner: QueryRunner) {
        if (!queryRunner) {
            throw new BadRequestException(`${this.constructor.name} is missing a valid query runner`);
        }
        this.queryRunner = queryRunner;
    }

    async execute() {
        const strParamVars = this.parameters.map(p => `:${p.name}`).join(',');
        const query = `CALL ${this.name}(${strParamVars})`;
        
        try {
            const allRows = await this.processQuery(this.queryRunner, query, this.parameters);
            return allRows;
        } catch(e) {
            throw new BadRequestException(`${this.constructor.name} - ${e.message}`);
        }
    }

    async processQuery(queryRunner: QueryRunner, query: string, parameters: ProcedureParameter[]): Promise<number> {
        const result: [ResultSet] = await queryRunner.query(query, parameters);
        const [id] = result;
        return id;
    }
}