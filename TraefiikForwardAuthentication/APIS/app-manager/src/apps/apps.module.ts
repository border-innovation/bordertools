import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppsController } from './apps.controller';
import { Apps } from './apps.entitty';
import { AppsService } from './apps.service';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [
    TypeOrmModule.forFeature([Apps]),
  ],
  controllers: [AppsController],
  providers: [
    AppsService,
    JwtService
  ]
})
export class AppsModule {}
