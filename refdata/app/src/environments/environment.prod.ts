export const environment = {
  production: true,
  api_url: '/api/reference-data-api/', // protocol, host and port may be omited, but not for dev
  api_base_url: '/api',
	authentication_url: '/authentication',
	healthcheck: '/healthcheck',
};
