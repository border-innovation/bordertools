// user-communication.service.ts
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserCommunicationService {
  private userSubject = new Subject<User>();
  /* user$: Observable<User | undefined> ; */

  sendUser(user: User) {
    this.userSubject.next(user)
    //this.store.dispatch(Login({ user: user }));
  }

  getUser(): Observable<User | undefined> {
    // return this.user$;
    return this.userSubject;
  }
}
