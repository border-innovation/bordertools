import { Injectable } from '@nestjs/common';
import { InjectRepository, InjectDataSource } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { ResourcePropertiesEntity } from './resource-properties.entity';
import { getAllResourceProperties } from 'src/procedures/getAllResourceProperties.procedure';
import { findResourceSystem } from 'src/procedures/findResourceSystem.procedure';

@Injectable()
export class ResourcesPropertiesService {
    constructor (
        @InjectRepository(ResourcePropertiesEntity) private resourcePropRepository: Repository<ResourcePropertiesEntity>,
        @InjectDataSource() private dataSource: DataSource
    ) {}

    public async getAllResourceProperties (systemName: string, resourceName: string, resourceValue: string): Promise<ResourcePropertiesEntity[]> {
        let queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        const result = await new getAllResourceProperties(queryRunner, systemName, resourceName, resourceValue).execute();
        await queryRunner.release();
        return result; 
    }

    public async findResourceSystem (system: string, resource: string) {
        let queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        const result = await new findResourceSystem(queryRunner, system, resource).execute();
        await queryRunner.release();
        return result;
    }
}