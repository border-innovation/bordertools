package border.tools.rules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.NodeGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;

@Rule(key = "S00029", name = "Pub services must have the flag 'Enforce Execute ACL' set to Always", priority = Priority.MAJOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class PubACLExecuteCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(PubACLExecuteCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(NodeGrammar.VALUE);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String service = FlowUtils.getQualifiedName(this.getContext().getFile());
		if (service.contains("_pub")) {
			for (AstNode attr : astNode.getChildren(NodeGrammar.ATTRIBUTES)) {
				for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
					if (name.getTokenValue().equalsIgnoreCase("check_internal_acls")) {
						String result = astNode.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue().strip();
						if (result.equals("no")) {
							getContext().createLineViolation(this,
									"Pub services must have flag Enforce Execute ACL set to Always", astNode);
						}
					}
				}
			}
		}
	}

	@Override
	public boolean isFlowCheck() {
		return false;
	}

	@Override
	public boolean isNodeCheck() {
		return true;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}
}
