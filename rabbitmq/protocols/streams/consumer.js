// import { Offset, connect } from "rabbitmq-stream-js-client";
const rabbit = require("rabbitmq-stream-js-client")
async function main() {
    const client = await rabbit.connect({
        hostname: "localhost",
        port: 5552,
        username: "username",
        password: "password",
        vhost: "/"
    })

    const consumerOption = {stream: "stream-name", offset: Offset.next()}

    const consumer = await client.declareConsumer(consumerOption, (message) => {
        console.log(message.content) 
    })

    await client.close()    

}

main()