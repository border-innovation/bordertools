import { Module, ValidationPipe } from '@nestjs/common';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { APIsModule } from './apis/apis.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { User } from './users/user.entity';
import { API } from './apis/apis.entity';
import { APP_PIPE } from '@nestjs/core';
import { PassportModule } from '@nestjs/passport'; 
import { JwtModule } from '@nestjs/jwt';
import { PermissionsModule } from './permissions/permissions.module';
import { Permission } from './permissions/permissions.entity';
import { join } from 'path';
import { readFileSync } from 'fs';
import { AppController } from './app.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';

const configPath = join(__dirname, '..', 'config', 'default.json');
const configJson = JSON.parse(readFileSync(configPath, 'utf-8'));


@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'AUTH_PACKAGE',
        transport: Transport.GRPC,
        options: {
          url: 'localhost:5001',
          package: 'auth',
          protoPath: join(__dirname, './auth/auth.proto'),
        },
      },
    ]),
    PassportModule.register({defaultStrategy: 'jwt'}),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [() => configJson], // Load the JSON config as a source
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        type: 'postgres',
        host: config.get<string>('database.host', 'localhost'),
        port: config.get<number>('database.port', 5432),
        username: config.get<string>('database.user'),
        password: config.get<string>('database.password'),
        database: config.get<string>('database.name'),
        entities: [User, API, Permission ], 
        synchronize: true, // WARNING: Not recommended for production
      }),
    }),  
    UsersModule,
    APIsModule,
    PermissionsModule,
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        const tokenSecret = config.get<string>('database.secret');
        const expiresIn = config.get<string>('api.expiresIn'); // Adjust as needed from config or hardcoded
        
        return {
          secret: tokenSecret,
          signOptions: { expiresIn },
        };
      }
    }),

  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_PIPE,
      useValue:  new ValidationPipe({
        whitelist: true //Doesnt allow to add additional properties to request body
      })
    }
  ],
  exports: [PassportModule]

})
export class AppModule {

  constructor(
  ){}
}
