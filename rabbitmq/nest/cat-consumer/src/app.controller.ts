import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';


@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

/*   @EventPattern('cats')
  async handleCats(@Payload() data: any): Promise<void> {
    console.log("Cat:", data);
  } */

  @MessagePattern('msgCat')
  addCats(@Payload() data: any): number{
    console.log('adding cat: ', data);
    return this.appService.addCats(data);
  }
}
