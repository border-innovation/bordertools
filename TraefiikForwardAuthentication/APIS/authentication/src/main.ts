import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: 'http://localhost',  // Allow your frontend origin
    credentials: true, // Allow credentials (cookies)
  });
  const config = new DocumentBuilder()
    .setTitle('Authenticator')
    .setDescription('The Authenticator API')
    .setVersion('1.0')
    .addTag('authManager')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('auth/swagger/api', app, document);

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.GRPC,
    options: {
      package: 'auth',
      protoPath: join(__dirname, './auth/auth.proto'), 
      url: 'localhost:5001'
    },
  });

  await app.startAllMicroservices();
  await app.listen(4000);
}
bootstrap();
