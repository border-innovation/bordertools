let
    chai = require('chai'),
    should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    request = require('request'),
    config = require('config'),
    serverConfig = config.get('server'),
    apiConfig = config.get('api'),
    api = supertest(serverConfig.protocol + '://localhost:' + serverConfig.port),
    server;

const jwt = require('jsonwebtoken');
const fs = require('fs');

//create the token
const myToken = jwt.sign({ sub: 'mocha' }, fs.readFileSync(apiConfig.token_secret, 'utf8'));

before(function () {
    describe('Starting the server...', function() {
        server = require('@oss-ready/nodejs-baseline').server.start();
    });
});

after(async ()  => {
    describe('Closing the server...', function() {
        server.close();
    });
});

describe('/POST', function () {
    it('The method route should exist', function (done) {
        api
            .post(apiConfig.api_url)
            .end((err, res) => {
                expect(res.status).to.not.equal(404);
                done();
            });
    });
});

describe('LOGIN SUCCESS', function () {
    it('Should return 200', function (done) {
        api
            .post(apiConfig.api_url)
            .send({"username":"keenApp", "password": "kAPP1234!"})
            .end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body).to.not.be.empty;
                expect(res.body).to.have.property('token');
                expect(res.body).to.have.property('apps');
                done();
            });
    });
    it('Should return token', function (done) {
        api
            .post(apiConfig.api_url)
            .send({"username":"keenApp", "password": "kAPP1234!"})
            .end((err, res) => {
                expect(res.body).to.not.be.empty;
                expect(res.body).to.have.property('token');
                done();
            });
    });
});

describe('LOGIN FAILURE', function () {

    it('empty body error - 401', function (done) {
        api
            .post(apiConfig.api_url)
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });
    it('wrong username error - 401', function (done) {
        api
            .post(apiConfig.api_url)
            .send({"username":"abc", "password": "kAPP1234!"})
            .end((err, res) => {
                expect(res.status).to.equal(401);
                done();
            });
    });
    it('no username error - 400', function (done) {
        api
            .post(apiConfig.api_url)
            .send({"password": "kAPP1234!"})
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });
    it('wrong password error - 401', function (done) {
        api
            .post(apiConfig.api_url)
            .send({"username":"keenApp", "password": "abc"})
            .end((err, res) => {
                expect(res.status).to.equal(401);
                done();
            });
    });
    it('no password error - 400', function (done) {
        api
            .post(apiConfig.api_url)
            .send({"username":"keenApp"})
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('user has only access to the default apps', function (done) {
      api
          .post(apiConfig.api_url)
          .send({"username":"keenApp", "password": "kAPP1234!"})
          .end((err, res) => {
              expect(res.body).to.not.be.empty;
              expect(res.body).to.have.property('token');
              expect(res.body).to.have.property('apps');
              expect(res.body.apps).to.eql(['Smart', 'Configurador']);
              done();
          });
  });


});
