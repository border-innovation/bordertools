import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap } from "rxjs";
import {
  AddAPIFailed,
  AddAPISuccess,
  APIsActionTypes,
  ListAPIsFailed,
  ListAPIsRequest,
  ListAPIsSuccess,
  RemoveAPIByIdFailed,
  RemoveAPIByIdSuccess,
  UpdateAPIFailed,
  UpdateAPISuccess,
} from "./actions";
import { AdminAPIsService } from "../../admin/admin-apis/admin-apis.service";
import { APIModel } from "./models/apis.model";

@Injectable()
export class APIEffects {
  constructor(
    private actions$: Actions,
    private apisService: AdminAPIsService
  ) {}

  getAPIs$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(APIsActionTypes.GET_ALL),
      mergeMap(() => {
        return this.apisService.getAllApis().pipe(
          map(( apis: any ) => {
            console.log("GetAPIS effects: ", apis)
            return new ListAPIsSuccess(apis)
          }),
          catchError((e) => [new ListAPIsFailed(e)])
        )
      })
    );
  });

  addAPIs$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(APIsActionTypes.ADD_NEW),
      mergeMap((api: any) => {
        console.log("AddAPIS effects: ", api)

        return this.apisService.addAPI(api.api).pipe(
          map(( apis ) => {
            return new AddAPISuccess(api.api)
          }),
          catchError((e) => [new AddAPIFailed(e)])
        )
      })
    );
  })

  addAPISuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(APIsActionTypes.ADD_NEW_SUCCESS),
      map(() => new ListAPIsRequest())
    )
  );


  updateAPIs$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(APIsActionTypes.UPDATE_ONE),
      mergeMap((api: any) => {
        return this.apisService.updateAPIS(api.api).pipe(
          map(( apis ) => {
            console.log("Update apis effects: ", apis)
            return new UpdateAPISuccess(api)
          }),
          catchError((e) => [new UpdateAPIFailed(e)])
        )
      })
    );
  })

  removeAPIS$ = createEffect((): any => {
    return this.actions$.pipe(
      ofType(APIsActionTypes.REMOVE_ID),
      mergeMap((id: any) => {
        console.log("Remove APIS Effects: ", id)
        return this.apisService.removeAPI(id.id).pipe(
          map(( apis ) => {
            return new RemoveAPIByIdSuccess(id);
          }),
          catchError((e) => [new RemoveAPIByIdFailed(e)])
        )
      })
    );
  })

  removeAPISuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(APIsActionTypes.REMOVE_ID_SUCCESS),
      map(() => new ListAPIsRequest())
    )
  );
  
}
