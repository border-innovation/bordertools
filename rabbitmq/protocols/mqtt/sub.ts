import * as mqtt from 'mqtt';

const client: mqtt.MqttClient = mqtt.connect("mqtt://localhost:1883");

client.on("connect", () => {
    client.subscribe("presence", (err) => {
        if (!err) {
            client.publish("presence", "Hello mqtt");
        }
    });
});

client.on("message", (topic, message) => {
    // message is Buffer
    console.log(message.toString());
    client.end();
});