import { Module } from '@nestjs/common';
import { APIsService } from './apis.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { API } from './apis.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([API])
  ],
  providers: [APIsService],
  exports: [APIsService],
})
export class APIsModule {}
