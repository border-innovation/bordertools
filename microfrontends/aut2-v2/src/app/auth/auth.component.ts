import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { User } from './user.model';
import { UserCommunicationService } from '@pedro_azevedo_border/communication-lib'

import { MatButtonModule } from '@angular/material/button';

import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [FormsModule, MatButtonModule, MatInputModule, MatFormFieldModule],
  templateUrl: './auth.component.html',
  styleUrl: './auth.component.scss',
})
export class AuthComponent {
  isLoginMode = true;
  users: User[] = [{ name: "Teste", email: "teste@teste.com", password: "111111" }];
  successLogin = false;
  public activeUser: User | undefined;

  constructor(private userCommunicationService: UserCommunicationService) {
     console.log('UserCommunicationService instance:', userCommunicationService);
  }

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }

    const email = form.value.email;
    const password = form.value.password;


    if (!this.isLoginMode) {
      const name = form.value.name;
      const user: User = new User(name, email, password);
      this.users.push(user);
      this.activeUser = user;
    }

    else {
      this.activeUser = this.users.find((user) => (user.email == email && user.password == password))
    }

    if (this.activeUser) {
      console.log(this.activeUser);
      this.userCommunicationService.sendUser(this.activeUser);
    }


    form.reset();
  }
}
