import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { API } from './apis.entity';
import { Repository } from 'typeorm';

@Injectable()
export class APIsService {
     
    constructor(
        @InjectRepository(API) private repo: Repository<API>,
    ){}

    async create( endpoint: string, role: string, verb: string) {
        //Validate Inputs
        if(!role) throw new BadRequestException('Missing role.');
        if(!endpoint) throw new BadRequestException('Missing endpoint.');
        if(!verb) throw new BadRequestException('Missing verb.');

        const exists = await this.repo.findBy({role, endpoint, verb });
        console.log(exists)
        if(exists && exists.length)
            throw new BadRequestException('Pair role, endpoint and verb already exists');


        //Validate role doens't already exist
        const obj = await this.findBy(role)
        if(obj && obj.length) 
            throw new BadRequestException('Role already exists');
        

        const ep: API = this.repo.create({endpoint,role, verb})
        return this.repo.save(ep)
    }

    async find(id: number){
        if(!id) 
            throw new NotFoundException("API not found");
        const endpoint = await this.repo.findOneBy({id});
        if(!endpoint) 
            throw new NotFoundException("API not found");
        return endpoint
    }
    
    async remove(id: number){
        if(!id) 
        throw new NotFoundException('API not found!');
        const endpoint = await this.find(id);
        if(!endpoint) 
            throw new NotFoundException('API not found!');
        return this.repo.remove(endpoint);
    }

    async findAll(){
        const obj = await this.repo.findAndCount();
        if(!obj.length) 
            throw new NotFoundException("No endpoints available")
        return obj[0];
    }
    
    findBy( role?: string, endpoint?: string, verb?: string){
        if(role) 
            return this.repo.findBy({role});
        if(endpoint) 
            return this.repo.findBy({endpoint})
        if(verb) 
            return this.repo.findBy({verb});
        throw new BadRequestException('No parameters');
    }

    async updateAPI(id: number, attrs: Partial<API>){
        if(!id) 
            throw new NotFoundException("API not found");
        const endpoint = await this.find(id);
        if(!endpoint) 
            throw new NotFoundException('API not found')
        Object.assign(endpoint, attrs);
        return this.repo.save(endpoint);
    }

    findByPair(endpoint: string, verb: string){
        return this.repo.findBy({endpoint,verb});

    }
    
}
