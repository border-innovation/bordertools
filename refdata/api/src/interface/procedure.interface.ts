import { QueryRunner } from "typeorm";


export interface ProcedureParameter {
    name: string;
    type: any;
    val?: any;
}

export interface ProcedureParameterInput {
    name: string;
    val: any;
}

export interface Procedure {
    name: string;
    queryRunner: QueryRunner;
    callTimeout: number;
    parameters: ProcedureParameter[];
    batchSize?: number;

    /**
     * Execute a Stored Procedure
     * 
     * @returns Return the data from the procedure
     */
    execute(): any;
}