import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReferenceData } from '../shared/reference-data.model';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { oldData: ReferenceData; newValue: string }
  ) {}

  close(): void {
    this.dialogRef.close('cancel');
  }

  replace(): void {
    this.dialogRef.close('replace');
  }
}
