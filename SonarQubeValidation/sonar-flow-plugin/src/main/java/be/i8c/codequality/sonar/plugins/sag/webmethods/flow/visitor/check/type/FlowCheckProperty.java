package be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.sonar.api.PropertyType;

@Documented
@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(FlowCheckProperties.class)
public @interface FlowCheckProperty {
  String key();
  String defaultValue();
  String name();
  PropertyType type();
  String category();
  String subCategory();
  String description();
  String onQualifiers();
}
