import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsLatitude, IsLongitude, IsNumber, IsString, Max, Min } from "class-validator";

export class CreateAPIDto {
    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsString()
    endpoint: string;

    @ApiProperty()
    @IsString()
    role: string;

    @ApiProperty()
    @IsString()
    verb: string;
}