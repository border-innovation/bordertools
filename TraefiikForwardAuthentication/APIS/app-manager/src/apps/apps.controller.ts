import { Body, Controller, Get, Param, Patch, Post, Delete, Query, Req, Res,Headers , Session, UseGuards } from '@nestjs/common';
import { AppsService } from './apps.service';
import { CreateAppDto } from './dtos/create-apps.dto';
import { UpdateAppsDto } from './dtos/update-apps.dto';
import { Response, Request } from 'express';


@Controller('apps')
export class AppsController {


    constructor(
        private appsService: AppsService
    ){}
    
    @Get()
    listByToken(@Req() request: Request){
        return this.appsService.listApps(request);
    }
    
    @Get('/list')
    listAll(){
        console.log("Listing all permissions")
        return this.appsService.findAll()
    }

    @Get('/:appId')
    getApp(@Param('appId') id: string){
        return this.appsService.find(parseInt(id));
    }
     
    @Post()
    async signUp(@Body() body: CreateAppDto){
        return this.appsService.create(body.appName, body.endpoint, body.role);
    }
   
    @Patch('/:appId')
    update(@Param('appId') id: string, @Body() body: UpdateAppsDto){
        return this.appsService.update(parseInt(id) ,body)
    } 

    @Delete('/:appId')
    delete(@Param('appId') id: string){
        return this.appsService.remove(parseInt(id))
    } 


}

