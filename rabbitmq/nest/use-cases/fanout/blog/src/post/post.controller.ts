import { Body, Controller, Post } from '@nestjs/common';
import { PostDto } from './post.dto';
import { PostService } from './post.service';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

@Controller('post')
export class PostController {

    constructor(private postService: PostService, private readonly amqpConnection: AmqpConnection){}

    @Post('new-post')
    addNewPost(@Body() post: any){
        return this.postService.addNewPost(post, this.amqpConnection);
    }

}
