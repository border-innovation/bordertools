import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [RabbitMQModule.forRoot(RabbitMQModule, {
    exchanges: [
      {
        name: 'notification-exchange',
        type: 'fanout'
      },
    ],
    uri: 'amqp://admin:admin@localhost:5672',
    connectionInitOptions: { wait: false },
  })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
