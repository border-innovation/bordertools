import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';

@Module({
  imports: [RabbitMQModule.forRoot(RabbitMQModule, {
    exchanges:[
      {
        name: 'notification-exchange',
        type: 'fanout'
      },
    ],
    uri: 'amqp://admin:admin@localhost:5672',
    connectionInitOptions:{ wait: false},
  })],
  providers: [PostService],
  controllers: [PostController]
})
export class PostModule {}
