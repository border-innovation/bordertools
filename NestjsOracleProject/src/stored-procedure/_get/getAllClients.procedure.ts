import { QueryRunner } from "typeorm";
import { BIND_OUT, BIND_IN, STRING, NUMBER, CURSOR } from 'oracledb'
import { ExecuteGetProcedure } from "./executeQuery.procedure";

export class getAllClients extends ExecuteGetProcedure {
    name = 'TEST.GETALL';
    queryRunner: QueryRunner;
    callTimeout = 60000;
    batchSize = 10;
    parameters = [
        { dir: BIND_OUT, name: 'o_response', type: CURSOR},
    ]; 

    constructor(queryRunner: QueryRunner) {
        super(queryRunner);
        this.queryRunner = queryRunner;
    }
}