import { Module, ValidationPipe } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DummyModuleModule } from './dummy-module/dummy-module.module';
import { TestModuleModule } from './test-module/test-module.module';
import { APP_GUARD, APP_PIPE } from '@nestjs/core';
import { AuthLibModule,JwtAuthGuard } from '@VascoRevesBorder/auth-lib';



@Module({
  imports: [
    AuthLibModule,
    DummyModuleModule, 
    TestModuleModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_PIPE,
      useValue:  new ValidationPipe({
        whitelist: true //Doesnt allow to add additional properties to request body
      })
    },
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard, // Set JwtAuthGuard as a global guard
    },
  ],
})
export class AppModule {}
