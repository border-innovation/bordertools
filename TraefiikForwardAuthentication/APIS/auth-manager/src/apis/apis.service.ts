import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { API } from './apis.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ApisService {
    constructor(
        @InjectRepository(API) private repo: Repository<API>,
    ){}

    VERBS = ["GET","PUT","POST","PATCH","DELETE"]

    async create( name: string, endpoint: string, role: string, verb: string) {
        //Validate Inputs
        if(!name) throw new BadRequestException('Missing name.');
        if(!role) throw new BadRequestException('Missing role.');
        if(!endpoint) throw new BadRequestException('Missing endpoint.');
        if(!verb) throw new BadRequestException('Missing verb.');

        const exists = await this.repo.findBy({ role, endpoint, verb });
        if(exists && exists.length)
            throw new BadRequestException('API already exists');

        const method = this.VERBS.find(x => x === verb);
        console.log(method);
        if(!method || method.length == 0)
            throw new BadRequestException('Verb is not allowed.');

        const ep: API = this.repo.create({name, endpoint,role, verb})
        return this.repo.save(ep)
    }

    async findAll(){
        const obj = await this.repo.findAndCount();
        if(!obj.length) 
            throw new NotFoundException("No API available")
        return obj[0];
    }

    findBy(  role?: string, endpoint?: string, verb?: string, name?: string){
        if(role) 
            return this.repo.findBy({role});
        if(endpoint) 
            return 
        this.repo.findBy({endpoint})
        if(verb) 
            return this.repo.findBy({verb});
        if(name) 
            return this.repo.findBy({name});
        throw new BadRequestException('No parameters');
    }

    async find(id: number){
        if(!id) 
            throw new NotFoundException("API not found");
        const endpoint = await this.repo.findOneBy({id});
        if(!endpoint) 
            throw new NotFoundException("API not found");
        return endpoint
    }

    async remove(id: number){
        if(!id) 
        throw new NotFoundException('API not found!');
        const endpoint = await this.find(id);
        if(!endpoint) 
            throw new NotFoundException('API not found!');
        return this.repo.remove(endpoint);
    }

    async updateAPI(id: number, attrs: Partial<API>){
        if(!id) 
            throw new NotFoundException("API not found");
        const endpoint = await this.find(id);
        if(!endpoint) 
            throw new NotFoundException('API not found')
        Object.assign(endpoint, attrs);
        return this.repo.save(endpoint);
    }
}
