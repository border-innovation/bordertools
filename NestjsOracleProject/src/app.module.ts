import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientModule } from './client/client.module';
import { ClientEntity } from './client/client.entity';
import { DataSource } from 'typeorm';
import { StoredProcedureModule } from './stored-procedure/stored-procedure.module';
import { TerminusModule } from '@nestjs/terminus';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import * as config from 'config';

@Module({
  imports: [
    TypeOrmModule.forFeature([ClientEntity]),
    ClientModule, 
    StoredProcedureModule, 
    TerminusModule,
    TypeOrmModule.forRoot(config.get('database')),
  ], 
  controllers: [
    AppController
  ],
  providers: [
    AppService
  ]
})

export class AppModule {
  constructor (private dataSource: DataSource) {
    
  }
}
