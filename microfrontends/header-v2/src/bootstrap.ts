import { bootstrapApplication, createApplication } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { provideRouter } from '@angular/router';
import { routes } from './app/app.routes';
import { createCustomElement } from '@angular/elements';

/* bootstrapApplication(AppComponent, app)
  .catch((err) => console.error(err));
 */

(async () => {
  const app = await createApplication({
    providers: [ provideRouter(routes)],
  });

  const headerRoot = createCustomElement(AppComponent, {
    injector: app.injector,
  });

  console.log(headerRoot);

  customElements.define('header-root', headerRoot);
})();


