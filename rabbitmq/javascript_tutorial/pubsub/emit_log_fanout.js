var amqp = require('amqplib/callback_api');

//Conection to the RabbitMQ server
amqp.connect('amqp://localhost', (error0, connection) => {

    if (error0) throw error0;

    //Create a channel, which most of the API for getting things done resides
    connection.createChannel((error1, channel) => {
        if (error1) throw error1;

        var exchange = 'logs';
        var msg = process.argv.slice(2).join(' ') || "Hello World";

        channel.assertExchange(exchange, 'fanout', {
            durable: false
        });

        channel.publish(exchange, '', Buffer.from(msg))
        console.log(`[x] Sent '${msg}' `);
    });

    //Close the connection
    setTimeout(() => {
        connection.close();
        process.exit(0);
    }, 500);
});