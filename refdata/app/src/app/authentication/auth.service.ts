import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';

import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public token: string;
  url = environment.api_url;
  healthcheck = environment.healthcheck;
  loggedIn = false;

  /**
   * Sets the user token to be appended to http headers if it exists in storage.
   */
  constructor(private http: HttpClient, private cookieService: CookieService) {
    // set token if saved in local storage
    try {
      var currentUser = JSON.parse(this.cookieService.get('u_session'));
      this.token = currentUser && currentUser.token;
    } catch (e) {
      alert('Please login again');
    }
  }

  /**
   * Gets the user token from the browsers local storage.
   * @returns string the corresponding token
   */
  public getToken(): string {
    let user = JSON.parse(this.cookieService.get('u_session'));
    return user.token;
  }

  public getDomain(): string {
    let user = JSON.parse(this.cookieService.get('u_session'));
    return user.domain;
  }

  public isLoggedIn(): boolean {
    if (this.cookieService.get('u_session')) {
      return true;
    } else {
      return false;
    }
  }

  public isTokenValid(): Observable<any> {
    let url = this.url + this.healthcheck;

    return this.http.get(url, { observe: 'response' });
  }

  /**
   * Removes the token from the browsers' localStorage and redirects the user out of the application
   */
  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    const domain = this.getDomain();
    this.cookieService.delete('u_session', '/', domain);
    window.location.href = environment.authentication_url;
  }
}
