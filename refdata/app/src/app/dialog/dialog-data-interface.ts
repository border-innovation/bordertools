export interface DialogData {
  system: string;
  resource: string;
  value: string;
}
