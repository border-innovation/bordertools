import { initFederation } from '@angular-architects/native-federation';


initFederation("/assets/federation.manifest.json")
  .catch(err => console.error(err))
  .then(_ => import('./bootstrap'))
  .catch(err => console.error(err));

/* import { initFederation } from '@angular-architects/native-federation';

async function initializeFederation() {
  try {
    const currentLocation = window.location;
    const currentDomain = `${currentLocation.protocol}//${currentLocation.hostname}`;
    const ports = {
      header: 4201,
      auth: 4203,
      profile: 4202,
      reviews: 4204
    };

    const manifest = {
      header: `${currentDomain}:${ports.header}/remoteEntry.json`,
      auth: `${currentDomain}:${ports.auth}/remoteEntry.json`,
      profile: `${currentDomain}:${ports.profile}/remoteEntry.json`,
      reviews: `${currentDomain}:${ports.reviews}/remoteEntry.json`
    };

    await initFederation(manifest)
      .catch(err => console.error(err))
      .then(_ => import('./bootstrap'))
      .catch(err => console.error(err));

  } catch (error) {
    console.error('Error initializing federation:', error);
  }
}

initializeFederation();
 */
