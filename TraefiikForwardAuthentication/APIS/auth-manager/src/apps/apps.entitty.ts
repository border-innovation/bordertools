import { AfterInsert, AfterRemove, AfterUpdate, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'APPS' })
export class Apps {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name: string;

    @Column()
    endpoint: string; 

    @Column()
    role: string;

    @AfterInsert()
    logInsert(){
        console.log('Inserted app with id: ', this.id)
    }

    @AfterUpdate()
    logUpdate(){
        console.log('Updated app with id: ', this.id)
    }

    @AfterRemove()
    logRemove(){
        console.log('Removed app with id: ', this.id)
    }
}