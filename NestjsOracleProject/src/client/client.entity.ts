import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity({ name: 'CLIENT' })
export class ClientEntity {
    @PrimaryColumn({ name: 'ID'}) id: number;
    @Column({ name: 'FIRST_NAME'}) first_name: string;
    @Column({ name: 'SECOND_NAME'}) second_name: string;

    constructor (id: number, first_name: string, second_name: string) {
        this.id = id;
        this.first_name = first_name;
        this.second_name = second_name;
    }
}