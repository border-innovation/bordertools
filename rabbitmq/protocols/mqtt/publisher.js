const mqtt = require('mqtt');
/* import { connect } from 'mqtt';
const client = connect("ws://127.0.0.1:15675/ws");

const topicName = 'test/connection';

client.on("connect", function (connack) {
    console.log("client connected", connack);
    const payload = { 1: "Hello world", 2: "Welcome to the test connection" }
    client.publish(topicName, JSON.stringify(payload), { qos: 1, retain: true }, (PacketCallback, err) => {

        if (err) {
            console.log(err, 'MQTT publish packet')
        }
    });

    setInterval(() => console.log("Message published"), 3000);
});

client.on("error", function (err) {
    console.log("Error: " + err)
    if (err.code == "ENOTFOUND") {
        console.log("Network error, make sure you have an active internet connection")
    }
}) 

client.on("close", function () {
    console.log("Connection closed by client")
})

client.on("reconnect", function () {
    console.log("Client trying a reconnection")
})

client.on("offline", function () {
    console.log("Client is currently offline")
})  */
const url = "ws://127.0.0.1:15675/ws"

const options = {
    clean:true,
    connectTimeout: 4000,
    clientId: "myclientid_" + parseInt(Math.random() * 100, 10),
    username: 'demo_user',
    password: 'demo_pass',
}

const client = mqtt.connect(url, options);

client.on('connect', function() {
    console.log('Connected');
    client.subscribe('test', function(err) {
        if(!err) client.publish('test', "hello mqtt");
    });
});

client.on('message', function(topic, message) {
    console.log(message.toString());
    client.end();
});

