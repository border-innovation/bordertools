import { Entity, PrimaryGeneratedColumn, Column, AfterInsert, AfterUpdate, AfterRemove, OneToMany, ManyToOne } from 'typeorm';

@Entity({ name: 'APIS' })
export class API {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    endpoint: string;

    @Column()
    role: string;
    
    @Column()
    verb: string;

    @AfterInsert()
    logInsert() {
        console.log('Inserted API: ', this);
    }

    @AfterUpdate()
    logUpdate() {
        console.log('Updated API: ', this);
    }

    @AfterRemove()
    logRemove() {
        console.log('Removed API: ', this);
    }
}