import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository, getDataSourceName } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository, getConnection, getConnectionManager } from 'typeorm';
import { APIsService } from 'src/apis/apis.service';
import { PermissionService } from 'src/permissions/permissions.service';


@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User) private repo: Repository<User>,
        private endpointsService: APIsService,
        private PermissionService: PermissionService
    ){}

    create(username: string, email: string, password: string, fullName: string) {
        const user: User = this.repo.create({username,email,password, fullName})
        return this.repo.save(user)
    }

    find(id: number){
        if(!id) throw new BadRequestException('Missing id.');
        return this.repo.findOneBy({id});
    }

    findBy(email?: string, username?: string){
        if(email) 
            return this.repo.findBy({email});
        if(username) 
            return this.repo.findBy({username});
        throw new NotFoundException('Email and username not provided.');
    }
    
    async remove(id: number){
        if(!id) throw new NotFoundException('Id not provided.');
        const user = await this.find(id);
        if(!user) throw new NotFoundException('User not found.');
        return this.repo.remove(user);
        //return this.repo.delete(id);
    }

    async findAll(){
        const obj = await this.repo.findAndCount()
        return obj[0];
    }

    async update(userId: number, attrs: Partial<User>){
        console.log('Updating: ',userId,attrs)
        const user = await this.find(userId);
        if(!user) 
            throw new BadRequestException('User Not found')
        Object.assign(user,attrs);
        return this.repo.save(user)
    }

    async getPermissions(username: string){
        const roles = await this.PermissionService.findBy(undefined, username);
        return roles.map(x => x.role);
    }
    

    async getUserAPIs(username: string){
        const roles = await this.getPermissions(username);
        let endpoints = [];

        for (const role of roles) {
            const endpoint = await this.endpointsService.findBy(role, undefined);
            if (endpoint.length > 0) {
                endpoints.push(endpoint[0]);
            }
        }
    
        return endpoints
        
    }
}