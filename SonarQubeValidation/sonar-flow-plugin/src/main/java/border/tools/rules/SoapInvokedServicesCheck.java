package border.tools.rules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.FlowGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;
import border.tools.utils.BorderUtils;

@Rule(key = "S00028", name = "SOAP Services must invoke getStatusFromLastError at least once", priority = Priority.MAJOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class SoapInvokedServicesCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(SoapInvokedServicesCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(FlowGrammar.FLOW);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String serviceName = FlowUtils.getQualifiedName(this.getContext().getFile());
		String filename = this.getContext().getFile().getAbsolutePath();
		if (BorderUtils.checkIfSoapService(serviceName, filename)
				|| BorderUtils.checkIfLegacyRestService(serviceName)) {
			for (AstNode invoke : getContent(astNode).getChildren(FlowGrammar.INVOKE)) {
				AstNode service = invoke.getFirstChild(FlowGrammar.ATTRIBUTES).getFirstChild(FlowAttTypes.SERVICE);
				if (service != null) {
					String name = service.getTokenValue();
					if (name.equalsIgnoreCase("VfToolsExceptionHandling.pub.service.status:getStatusFromLastError")) {
						return;
					}
				}
			}
			getContext().createLineViolation(this,
					"SOAP Services must invoke getStatusFromLastError at least once"
							+ " after the TryCatch Sequence.",
					astNode);
		}
	}

	private AstNode getContent(AstNode sequenceNode) {
		if (sequenceNode != null) {
			return sequenceNode.getFirstChild(FlowGrammar.CONTENT);
		}
		return null;
	}

	@Override
	public boolean isFlowCheck() {
		return true;
	}

	@Override
	public boolean isNodeCheck() {
		return false;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}
}
