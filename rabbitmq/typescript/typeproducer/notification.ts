import mqConnection from './connection';

export type INotification = {
    title: string,
    description: string
};

export const sendNotification = async (notification: INotification) => {
    await mqConnection.sendToQueue('ts_queue_1', notification);

    console.log('Sent the notification to consumer to consumer');
}