package border.tools.rules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.NodeGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;
import border.tools.utils.BorderUtils;

@Rule(key = "S00019", name = "Rest Services must declare responseString and responseCode on their outputs", priority = Priority.MAJOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class RestOutputCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(RestOutputCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(NodeGrammar.SIGNATURE_OUT);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String service = FlowUtils.getQualifiedName(this.getContext().getFile());
		String filename = this.getContext().getFile().getAbsolutePath();
		if (BorderUtils.checkIfRestService(service, filename) || BorderUtils.checkIfLegacyRestService(service)) {
			checkOutputResults(astNode);
		}
	}

	private void checkOutputResults(AstNode astNode) {
		for (AstNode array : astNode.getChildren(NodeGrammar.REC_FIELDS)) {
			for (AstNode record : array.getChildren(NodeGrammar.RECORD)) {
				for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
					for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
						for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
							if (name.getTokenValue().equals("FIELD_NAME")) {
								String results = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue()
										.strip();
								if (results.equals("responseCode")) {
									checkResponseStringOutput(astNode);
									return;
								}
								if (results.equals("responseString")) {
									checkResponseCodeOutput(astNode);
									return;
								}
							}
						}
					}
				}
			}
		}
		getContext().createLineViolation(this, "Rest Service Output must have responseCode and responseString defined in it's variables.", astNode);
	}

	private void checkResponseCodeOutput(AstNode astNode) {
		for (AstNode array : astNode.getChildren(NodeGrammar.REC_FIELDS)) {
			for (AstNode record : array.getChildren(NodeGrammar.RECORD)) {
				for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
					for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
						for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
							if (name.getTokenValue().equals("FIELD_NAME")) {
								String results = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue()
										.strip();
								if (results.equals("responseCode")) {
									return;
								}
							}
						}
					}
				}
			}
		}
		getContext().createLineViolation(this, "Rest Service Output must have responseCode defined in it's variables.", astNode);
	}

	private void checkResponseStringOutput(AstNode astNode) {
		for (AstNode array : astNode.getChildren(NodeGrammar.REC_FIELDS)) {
			for (AstNode record : array.getChildren(NodeGrammar.RECORD)) {
				for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
					for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
						for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
							if (name.getTokenValue().equals("FIELD_NAME")) {
								String results = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue()
										.strip();
								if (results.equals("responseString")) {
									return;
								}
							}
						}
					}
				}
			}
		}
		getContext().createLineViolation(this, "Rest Service Output must have responseString defined in it's variables.", astNode);
	}

	@Override
	public boolean isFlowCheck() {
		return false;
	}

	@Override
	public boolean isNodeCheck() {
		return true;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}
}
