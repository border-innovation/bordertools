export interface PermissionsViewModel {
  username: string;
  role: string;
}
