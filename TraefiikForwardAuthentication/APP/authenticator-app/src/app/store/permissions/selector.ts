import { createSelector } from "@ngrx/store";
import { GlobalState } from "../../app.store";
import { PermissionsState } from "./reducer";

export const selectPermissionsState = (state: GlobalState) => state.permission;

export const selectPermissions = createSelector(
  selectPermissionsState,
  (state: PermissionsState) => {
    return ({
      permissions: state?.permissions ?? [], 
      isLoading: state?.isLoading ?? false
    })}
);
