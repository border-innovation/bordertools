import { IsArray, IsEmail, IsOptional, IsString } from "class-validator";

export class CreateAppDto{
    
    @IsString()
    appName: string;

    @IsString()
    endpoint: string; 

    @IsString()
    role: string;

}