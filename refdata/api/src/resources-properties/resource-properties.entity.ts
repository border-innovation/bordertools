import { Entity, Column, Unique, PrimaryColumn } from 'typeorm';

@Entity('RESOURCEPROPERTIES')
@Unique('RESOURCEPROPERTIES_UK', ['resourceName'])
export class ResourcePropertiesEntity {
  
  @PrimaryColumn({
    name: 'RESOURCEKEY',
    type: 'number',
  })
  resourceKey: number;

  @Column({
    name: 'RESOURCENAME',
    type: 'varchar2',
    length: 100,
    nullable: false,
  })
  resourceName: string;

  @Column({
    name: 'PARENTKEYID',
    type: 'number',
    scale: 0, // No decimal places
    precision: 1, // Number(1, 0)
    nullable: true,
  })
  parentKeyId: number;
} 