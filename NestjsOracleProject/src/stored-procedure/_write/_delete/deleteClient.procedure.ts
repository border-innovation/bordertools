import { QueryRunner } from "typeorm";
import { BIND_OUT, BIND_IN, STRING, NUMBER, CURSOR } from 'oracledb'
import { ExecuteWriteProcedure } from "../executeQuery.procedure";

export class deleteClient extends ExecuteWriteProcedure {
    name = 'TEST.DELETECLIENT';
    queryRunner: QueryRunner;
    callTimeout = 60000;
    batchSize = 10;
    parameters = [
        { dir: BIND_IN, name: 'i_id', type: NUMBER, val: null},
        { dir: BIND_OUT, name: 'o_id', type: NUMBER},
    ];

    constructor(queryRunner: QueryRunner, id: number) {
        super(queryRunner);
        this.queryRunner = queryRunner;
        this.parameters[0].val = id;
    }
}