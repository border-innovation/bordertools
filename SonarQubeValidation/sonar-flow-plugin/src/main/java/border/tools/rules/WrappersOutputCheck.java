package border.tools.rules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.rules.RuleType;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;

import com.sonar.sslr.api.AstNode;

import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.NodeGrammar;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowAttTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.sslr.types.FlowTypes;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.utils.FlowUtils;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.Tags;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheck;
import be.i8c.codequality.sonar.plugins.sag.webmethods.flow.visitor.check.type.FlowCheckRuleType;

@Rule(key = "S00018", name = "Wrappers must have an Output variable that is a Document/Document List called results", priority = Priority.MINOR, tags = {
		Tags.BAD_PRACTICE })
@SqaleConstantRemediation("2min")
@FlowCheckRuleType(ruletype = RuleType.VULNERABILITY)

public class WrappersOutputCheck extends FlowCheck {

	static final Logger logger = LoggerFactory.getLogger(WrappersOutputCheck.class);

	@Override
	public void init() {
		logger.debug("++ Initializing {} ++", this.getClass().getName());
		subscribeTo(NodeGrammar.SIGNATURE_OUT);
	}

	@Override
	public void visitNode(AstNode astNode) {
		String service = FlowUtils.getQualifiedName(this.getContext().getFile());
		if (service.endsWith("_wrp")) {
			checkOutputResults(astNode);
		}
	}

	private void checkOutputResults(AstNode astNode) {
		for (AstNode array : astNode.getChildren(NodeGrammar.REC_FIELDS)) {
			for (AstNode record : array.getChildren(NodeGrammar.RECORD)) {
				for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
					for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
						for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
							if (name.getTokenValue().equals("FIELD_NAME")) {
								String results = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue()
										.strip();
								if (results.equals("results") || results.equals("result")) {
									checkResultsType(record);
									return;
								}
							}
						}
					}
				}
			}
		}

		getContext().createLineViolation(this, "Output must define a variable named results/result as a Document/Document List",
				astNode);
	}

	private void checkResultsType(AstNode record) {
		for (AstNode value : record.getChildren(NodeGrammar.VALUE)) {
			for (AstNode attr : value.getChildren(NodeGrammar.ATTRIBUTES)) {
				for (AstNode name : attr.getChildren(FlowAttTypes.NAME)) {
					if (name.getTokenValue().equals("FIELD_TYPE")) {
						String type = value.getChildren(FlowTypes.ELEMENT_VALUE).get(0).getTokenValue().strip();
						if (!type.equals("record")) {
							getContext().createLineViolation(this,
									"results/result must be of type Document/Document List", value);
						}
						return;
					}
				}
			}
		}
	}

	@Override
	public boolean isFlowCheck() {
		return false;
	}

	@Override
	public boolean isNodeCheck() {
		return true;
	}

	@Override
	public boolean isTopLevelCheck() {
		return false;
	}
}
