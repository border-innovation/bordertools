import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { AdminAppsService } from './admin-apps.service';
import { selectAllApps, selectApps } from '../../store/apps/selector';
import { Observable } from 'rxjs';
import { AddAppRequest, AppsActionTypes, RemoveAppByIdRequest, UpdateAppRequest } from '../../store/apps/actions';
import { MatSort } from '@angular/material/sort';


interface Application {
  id?: string;
  appName: string;
  endpoint: string;
  role: string;
  isEdit?: boolean;
}

@Component({
  selector: 'app-admin-apps',
  templateUrl: './admin-apps.component.html',
  styleUrl: './admin-apps.component.scss'
})
export class AdminAppsComponent implements OnInit {

  createAppForm: FormGroup;
  editAppForm: FormGroup;

  dataSource = new MatTableDataSource<Application>([]);
  appList: Application[];
  displayedColumns: string[] = ['name', 'endpoint', 'role', 'actions'];

  // Mock Data
  // dataSource = [
  //   { name: 'App1', endpoint: '/app1', role: 'Access_App1_role' },
  //   { name: 'App2', endpoint: '/app2', role: 'Access_App2_role' },
  //   { name: 'App3', endpoint: '/app3', role: 'Access_App3_role' }
  // ];
  //dataSource = new MatTableDataSource(this.appList);

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
   private cdr: ChangeDetectorRef // Add ChangeDetectorRef
  ){
    this.createAppForm = this.formBuilder.group({
      appName: new FormControl(""),
      endpoint: new FormControl(""),
      role: new FormControl("")
    });

    this.editAppForm = this.formBuilder.group({
      appName: new FormControl(""),
      endpoint: new FormControl(""),
      role: new FormControl("")
    });
  }

  
  ngOnInit() {
    //TODO
    // Its not updating the table correctly
    // Subscribe to the combined apps and loading state
    this.store.select(selectAllApps)
      .subscribe(({ allApps, isLoading }) => {
        if (allApps && allApps.length > 0) {
          console.log("Init: ", allApps)
          this.dataSource = new MatTableDataSource(allApps);
          this.appList = allApps;
        } else if (!isLoading) {
          console.log("No apps available...");
        }
        this.cdr.detectChanges();
      });
  }


  onSubmit(): void {
    if (this.createAppForm.valid) {
      const appData = this.createAppForm.value;
      console.log('Form Submitted:', appData);
      this.store.dispatch(new AddAppRequest(appData));
      this.createAppForm.reset();

      // TODO
      // Validate Response, if ok alert ui
      // Validate endpoint starts with '/', if not add
      // Its not adding to list properly

      this.createAppForm.reset();
    } else {
      console.log('Form not valid');
    }
  }

  removeApp(app: Application) {
    console.log("Deleting app: ", app)
    this.store.dispatch(new RemoveAppByIdRequest(app.id));

  }

  editApp(app: Application) {
    console.log("IsEditingRow: ", app)

    this.editAppForm.patchValue({
      appName: app.appName,
      endpoint: app.endpoint,
      role: app.role
    });


    if (app.isEdit) {
      this.updateApp(app); // Call save method when "Done" is clicked
    }
    app.isEdit = !app.isEdit; // Toggle edit mode
  }

  updateApp(app: Application) {
      // TODO
      // Validate Response, if ok alert ui
    console.log('Saving app:', app);
    this.store.dispatch(new UpdateAppRequest(app));

  }

  cancelEdit(app: Application){
    app.isEdit = !app.isEdit;
  }
  
}
