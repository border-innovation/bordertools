const mqtt = require('mqtt')
const client = mqtt.connect("mqtt://username:password@localhost:1883")
const topicName = 'test/connection'

client.on('connect', () => {
    client.subscribe(topicName, (err, granted) => {
        if(err) {
            console.log(err, 'err');
        }

        console.log(granted, 'granted')
    })
})


client.on("packetsend", (packet) =>{
    console.log(packet, "packet2");
})