export interface APIModel {
  id?: string;
  name?: string;
  endpoint: string;
  role: string;
  verb: string
}
