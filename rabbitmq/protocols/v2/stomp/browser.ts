import { Stomp } from '@stomp/stompjs';

const url = "ws://localhost:61613/stomp"
/* const client = new Client({
    brokerURL: url,
    connectHeaders: {
        login: 'guest',
        passcode: 'guest'
    },
    debug: (str) => console.log(str),
    reconnectDelay: 5000,
    heartbeatIncoming: 4000,
    heartbeatOutgoing: 4000
}); */

const client = Stomp.client(url);

console.log(client.brokerURL);

client.onConnect = function (frame) {
    console.log('Connected');
};

client.onStompError = function (frame) {
    // Will be invoked in case of error encountered at Broker
    // Bad login/passcode typically will cause an error
    // Complaint brokers will set `message` header with a brief message. Body may contain details.
    // Compliant brokers will terminate the connection after any error
    console.log('Broker reported error: ' + frame.headers['message']);
    console.log('Additional details: ' + frame.body);
};

client.activate();