import { loadRemoteModule } from '@angular-architects/native-federation';
import { CommonModule } from '@angular/common';
import { Component, ElementRef, Input, OnInit, Renderer2, effect, inject, signal } from '@angular/core';
import { initWrapperConfig } from '../wrapper.model';
import { Router } from '@angular/router';
import { User } from '../user.model';

@Component({
  selector: 'app-wrapper-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './wrapper-header.component.html',
  styleUrl: './wrapper-header.component.scss'
})
export class WrapperHeaderComponent implements OnInit {
  elm = inject(ElementRef);

  async ngOnInit() {

    await loadRemoteModule('header', './web-components');
    const root = document.createElement('header-root');
    this.elm.nativeElement.appendChild(root);


    // Listen for the custom event emitted by the header web component
    root.addEventListener('navigate', (event: Event) => {
      const customEvent = event as CustomEvent; // Cast the event to CustomEvent
      // Handle the navigation event here
      const page = customEvent.detail; // Assuming the detail contains the page/route
      this.navigateTo(page);
    });

  }

  //dummyUser: User = { name: "dummy", email: 'dummy@email.com', password: '1111111' };
  activeUser = signal<User>({ name: "dummy", email: 'dummy@email.com', password: '1111111' });

  constructor(private router: Router, private renderer: Renderer2) {
    console.log(this.activeUser());
  }


  navigateTo(page: string) {
    console.log("shell: ir works:", page);
    this.router.navigate([page]);
  }


}
